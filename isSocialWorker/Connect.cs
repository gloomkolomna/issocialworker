﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using isSocialWorker.Properties;

namespace isSocialWorker
{
    public class Connect
    {
        /*public static string MySqlConnectionString()
        {
            if (Settings.Default.CallUpgrade)
            {
                Settings.Default.Upgrade();
                Settings.Default.CallUpgrade = false;
                Settings.Default.Save();
            }
            string connection = "Server=" + Settings.Default.server + ";Database=" + Settings.Default.bdname + ";Uid=" + Settings.Default.user + ";Pwd=" + Settings.Default.password + ";charset=utf8";
            return connection;
        }*/

        public static string MySqlConnectionString
        {
            get
            {
                if (Settings.Default.CallUpgrade)
                {
                    Settings.Default.Upgrade();
                    Settings.Default.CallUpgrade = false;
                    Settings.Default.Save();
                }
                return "Server=" + Settings.Default.server + ";Database=" + Settings.Default.bdname + ";Uid=" + Settings.Default.user + ";Pwd=" + Settings.Default.password + ";charset=utf8";
            }
        }

    }
}
