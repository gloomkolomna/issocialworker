﻿namespace isSocialWorker.View
{
    partial class formSettingsSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fldPath = new System.Windows.Forms.TextBox();
            this.butSelectPath = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(312, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Путь к папке с изображениями";
            // 
            // fldPath
            // 
            this.fldPath.Location = new System.Drawing.Point(16, 33);
            this.fldPath.Name = "fldPath";
            this.fldPath.Size = new System.Drawing.Size(355, 30);
            this.fldPath.TabIndex = 1;
            // 
            // butSelectPath
            // 
            this.butSelectPath.Location = new System.Drawing.Point(377, 33);
            this.butSelectPath.Name = "butSelectPath";
            this.butSelectPath.Size = new System.Drawing.Size(113, 30);
            this.butSelectPath.TabIndex = 2;
            this.butSelectPath.Text = "Выбрать";
            this.butSelectPath.UseVisualStyleBackColor = true;
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(367, 163);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(123, 30);
            this.butSave.TabIndex = 3;
            this.butSave.Text = "Сохранить";
            this.butSave.UseVisualStyleBackColor = true;
            // 
            // formSettingsSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 205);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.butSelectPath);
            this.Controls.Add(this.fldPath);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formSettingsSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Системные настройки";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fldPath;
        private System.Windows.Forms.Button butSelectPath;
        private System.Windows.Forms.Button butSave;
    }
}