﻿using isSocialWorker.ClientList;
using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View
{
    public interface IFormClientList
    {
        event EventHandler LoadForm;
        event EventHandler ExportToExcelClick;
        event EventHandler ClickToClient;
        event EventHandler ButtonRefreshClick;

        List<ClientFields> ClientList { get; set; }
        List<DirectoryDefault> Lists { set; }

        int ClientId { get; }

        void DepartamentList();
        void SocialWorkerList();
        void MedicalWorkerList();
        void StatusList();
        void CategoryList();
    }
    public partial class formClientList : Form, IFormClientList
    {
        private List<ClientFields> clientList = new List<ClientFields>();

        public formClientList()
        {
            InitializeComponent();
            this.Load += formClientList_Load;
            butFilter.Click += butFilter_Click;
            butFilterOk.Click += butFilterOk_Click;
            butExportClientListToExcel.Click += butExportClientListToExcel_Click;
            butRefresh.Click += butRefresh_Click;

            dataClientList.DoubleClick += dataClientList_DoubleClick;

            checkCategory.CheckedChanged += checkCategory_CheckedChanged;
            checkDepartament.CheckedChanged += checkDepartament_CheckedChanged;
            checkFIO.CheckedChanged += checkFIO_CheckedChanged;
            checkMedicalworker.CheckedChanged += checkMedicalworker_CheckedChanged;
            checkSnils.CheckedChanged += checkSnils_CheckedChanged;
            checkSocialworker.CheckedChanged += checkSocialworker_CheckedChanged;
            checkStatus.CheckedChanged += checkStatus_CheckedChanged;

            radioAllPeriod.CheckedChanged += radioAllPeriod_CheckedChanged;
            radioPeriod.CheckedChanged += radioPeriod_CheckedChanged;
        }

        void butRefresh_Click(object sender, EventArgs e)
        {
            if (ButtonRefreshClick != null) ButtonRefreshClick(this, EventArgs.Empty);
        }

        void dataClientList_DoubleClick(object sender, EventArgs e)
        {
            if (ClickToClient != null) ClickToClient(this, EventArgs.Empty);
        }

        void butExportClientListToExcel_Click(object sender, EventArgs e)
        {
            if (ExportToExcelClick != null) ExportToExcelClick(this, EventArgs.Empty);
        }

        #region Checkbox & RadioButton Checkeed Changed

        void radioPeriod_CheckedChanged(object sender, EventArgs e)
        {
            fldPeriodStart.Enabled = true;
            fldPeriodEnd.Enabled = true;
        }

        void radioAllPeriod_CheckedChanged(object sender, EventArgs e)
        {
            fldPeriodStart.Enabled = false;
            fldPeriodEnd.Enabled = false;
        }

        void checkStatus_CheckedChanged(object sender, EventArgs e)
        {
            if (checkStatus.Checked)
                fldStatus.Enabled = true;
            else
                fldStatus.Enabled = false;
        }

        void checkSocialworker_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSocialworker.Checked)
                fldSocialworker.Enabled = true;
            else
                fldSocialworker.Enabled = false;
        }

        void checkSnils_CheckedChanged(object sender, EventArgs e)
        {
            if (checkSnils.Checked)
                fldSnils.Enabled = true;
            else
                fldSnils.Enabled = false;
        }

        void checkMedicalworker_CheckedChanged(object sender, EventArgs e)
        {
            if (checkMedicalworker.Checked)
                fldMedicalworker.Enabled = true;
            else
                fldMedicalworker.Enabled = false;
        }

        void checkFIO_CheckedChanged(object sender, EventArgs e)
        {
            if (checkFIO.Checked)
            {
                fldLastname.Enabled = true;
                fldFirstname.Enabled = true;
                fldMiddlename.Enabled = true;
            }
            else
            {
                fldLastname.Enabled = false;
                fldFirstname.Enabled = false;
                fldMiddlename.Enabled = false;
            }
        }

        void checkDepartament_CheckedChanged(object sender, EventArgs e)
        {
            if (checkDepartament.Checked)
                fldDepartament.Enabled = true;
            else
                fldDepartament.Enabled = false;
        }

        void checkCategory_CheckedChanged(object sender, EventArgs e)
        {
            if (checkCategory.Checked)
                fldCategory.Enabled = true;
            else
                fldCategory.Enabled = false;
        }


        #endregion

        void butFilterOk_Click(object sender, EventArgs e)
        {
            Filter();
            lblCountClients.Text = CountClients().ToString();
        }

        void butFilter_Click(object sender, EventArgs e)
        {
            if (groupBoxFilter.Visible)
            {
                groupBoxFilter.Visible = false;
                butFilter.Image = Properties.Resources.filter;
            }
            else
            {
                groupBoxFilter.Visible = true;
                butFilter.Image = Properties.Resources.filter_advanced;
            }
        }

        void formClientList_Load(object sender, EventArgs e)
        {
            if (LoadForm != null) LoadForm(this, EventArgs.Empty);
        }

        public event EventHandler LoadForm;

        public event EventHandler ExportToExcelClick;

        public List<ClientFields> ClientList
        {
            get
            {
                return _clientList;
            }
            set 
            { 
                clientList = value;

                Filter();
                lblCountClients.Text = CountClients().ToString();
            }
        }

        private int CountClients()
        {
            return dataClientList.RowCount;
        }

        private List<ClientFields> _clientList;
        /// <summary>
        /// Метод фильтрации данных. Первоначально подгружаются все данные.
        /// </summary>
        private void Filter()
        {
            var source = clientList;
            var query = source;

            // Отбор по категории
            if (checkCategory.Checked)
            {
                if (!checkNoCategory.Checked)
                {
                    int id = Convert.ToInt32(fldCategory.SelectedValue);
                    query = query.Where(q => q.Categorys.FindAll(x => x.CategoryId == id).Select(x => x.CategoryId).FirstOrDefault() == id).ToList();
                }
            }

            if (checkNoCategory.Checked)
            {
                query = query.Where(q => !q.Categorys.Any()).ToList();
            }

            // Отбор по отделению
            if (checkDepartament.Checked)
            {
                int id = Convert.ToInt32(fldDepartament.SelectedValue);
                query = query.Where(q => q.DepartamentId == id && !q.IsDead && !q.IsDisposal && !q.IsSuspend).ToList();
            }

            // Отбор по социальному работнику
            if (checkSocialworker.Checked)
            {
                int id = Convert.ToInt32(fldSocialworker.SelectedValue);
                query = query.Where(q => q.SocialworkerId == id && !q.IsDead && !q.IsDisposal && !q.IsSuspend).ToList();
            }

            // Отбор по ФИО
            if (checkFIO.Checked)
            {
                string lastname = fldLastname.Text.Trim().ToLower();
                string firstname = fldFirstname.Text.Trim().ToLower();
                string middlename = fldMiddlename.Text.Trim().ToLower();
                query = query
                    .Where(q => q.Firstname.ToLower().Contains(firstname) &&
                        q.Middlename.ToLower().Contains(middlename) &&
                        q.Lastname.ToLower().Contains(lastname) && 
                        !q.IsDead && !q.IsDisposal && !q.IsSuspend).ToList();
            }

            // Отбор по статусу
            if (checkStatus.Checked)
            {
                int id = Convert.ToInt32(fldStatus.SelectedValue);
                query = query.Where(q => q.StatusId == id && !q.IsDead && !q.IsDisposal && !q.IsSuspend).ToList();
            }

            // Отбор по СНИЛС
            if (checkSnils.Checked)
            {
                string snils = fldSnils.Text;
                query = query.Where(q => q.Snils.Contains(snils) && !q.IsDead && !q.IsDisposal && !q.IsSuspend).ToList();
            }


            // Отбор: добавление выбывших
            if (checkIsDead.Checked)
            {
                query = query.Where(q => q.IsDead).ToList();
            }
            else
            {
                query = query.Where(q => !q.IsDead).ToList();
            }

            // Отбор: добавление выбывших
            if (checkIsDisposal.Checked)
            {
                query = query.Where(q => q.IsDisposal).ToList();
            }
            else
            {
                query = query.Where(q => !q.IsDisposal).ToList();
            }

            // Отбор: добавление выбывших
            if (checkIsSuspend.Checked)
            {
                query = query.Where(q => q.IsSuspend).ToList();
            }
            else
            {
                query = query.Where(q => !q.IsSuspend).ToList();
            }

            // Отбор за период дат рождения
            if (radioPeriod.Checked)
            {
                DateTime dtStart = fldPeriodStart.Value;
                DateTime dtEnd = fldPeriodEnd.Value;

                query = query.Where(q => !q.IsDead && !q.IsDisposal && !q.IsSuspend && (q.Dateborn >= dtStart.Date && q.Dateborn <= dtEnd.Date)).ToList();
            }

            var results = query.ToList();

            var clientdgv = results.Select(l => new
            {
                id = l.Id,
                lastname = l.Lastname,
                firstname = l.Firstname,
                middlename = l.Middlename,
                dateborn = l.Dateborn,
                snils = l.Snils,
                phone = l.Phone
            }).OrderBy(x => x.lastname);

            _clientList = results;

            dataClientList.DataSource = clientdgv.ToList();
            dataClientList.Columns["id"].Visible = false;
            dataClientList.Columns["lastname"].HeaderText = "Фамилия";
            dataClientList.Columns["firstname"].HeaderText = "Имя";
            dataClientList.Columns["middlename"].HeaderText = "Отчество";
            dataClientList.Columns["dateborn"].HeaderText = "Дата рождения";
            dataClientList.Columns["snils"].HeaderText = "СНИЛС";
            dataClientList.Columns["phone"].HeaderText = "Номер телефона";
        }

        #region Filling Combobox
        private List<DirectoryDefault> list = new List<DirectoryDefault>();

        public void DepartamentList()
        {
            FillingComboBox(fldDepartament);
        }

        public void SocialWorkerList()
        {
            FillingComboBox(fldSocialworker);
        }

        public void MedicalWorkerList()
        {
            FillingComboBox(fldMedicalworker);
        }

        public void StatusList()
        {
            FillingComboBox(fldStatus);
        }

        public void CategoryList()
        {
            FillingComboBox(fldCategory);
        }

        public List<DirectoryDefault> Lists
        {
            set { list = value; }
        }

        private void FillingComboBox(ComboBox comboBox)
        {
            var sourse = (from a in list
                          select new { a.Id, Names = a.Name }).ToList();
            comboBox.DataSource = sourse;
            comboBox.DisplayMember = "Names";
            comboBox.ValueMember = "Id";
        }
        #endregion

        public int ClientId
        {
            get
            {
                int clientid = Int32.Parse(dataClientList.CurrentRow.Cells["id"].Value.ToString());
                return clientid;
            }
        }

        public event EventHandler ClickToClient;


        public event EventHandler ButtonRefreshClick;
    }
}
