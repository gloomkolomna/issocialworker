﻿using isSocialWorker.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View
{
    public interface IFormSettingSystem
    {
        string PathToPhoto { get; set; }

        event EventHandler SaveSettingsClick;
        event EventHandler OpenFormLoad;
    }

    public partial class formSettingsSystem : Form, IFormSettingSystem
    {
        public formSettingsSystem()
        {
            InitializeComponent();

            butSelectPath.Click += butSelectPath_Click;
            butSave.Click += butSave_Click;
            this.Load += formSettingsSystem_Load;
        }

        void formSettingsSystem_Load(object sender, EventArgs e)
        {
            if (OpenFormLoad != null) OpenFormLoad(this, EventArgs.Empty);
        }

        void butSave_Click(object sender, EventArgs e)
        {
            if (SaveSettingsClick != null) SaveSettingsClick(this, EventArgs.Empty);
        }

        void butSelectPath_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if(fbd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fldPath.Text = fbd.SelectedPath;
            }
        }

        public string PathToPhoto
        {
            get 
            {
                return fldPath.Text;
            }
            set
            {
                fldPath.Text = Settings.Default.pathToPhoto;
            }
        }

        public event EventHandler SaveSettingsClick;

        public event EventHandler OpenFormLoad;
    }
}
