﻿using isSocialWorker.ClientAnketa;
using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View
{
    public partial class formClientAnketa : Form, IAnketa
    {
        public formClientAnketa()
        {
            InitializeComponent();
            tabControl.DrawItem += tabControl_DrawItem;
            this.Load += formClientAnketa_Load;
            fldRegion.DropDownClosed += fldRegion_DropDownClosed;
            fldCity.DropDownClosed += fldCity_DropDownClosed;
            fldStreet.DropDownClosed += fldStreet_DropDownClosed;
            fldHouse.DropDownClosed += fldHouse_DropDownClosed;
            fldAppartament.DropDownClosed += fldAppartament_DropDownClosed;

            butSaveInfo.Click += butSaveInfo_Click;
            butSaveHead.Click += butSaveHead_Click;
            butAddCategory.Click += butAddCategory_Click;
            butRemoveCategory.Click += butRemoveCategory_Click;
            butSaveAddress.Click += butSaveAddress_Click;
            butSaveHousing.Click += butSaveHousing_Click;
            butSaveDocuments.Click += butSaveDocuments_Click;

            butSuspendStop.Click += butSuspendStop_Click;
            butSuspendStart.Click += butSuspendStart_Click;
            butDead.Click += butDead_Click;
            butDisposalStop.Click += butDisposalStop_Click;
            butDisposalStart.Click += butDisposalStart_Click;

            butReportView.Click += butReportView_Click;
            butSaveReport.Click += butSaveReport_Click;
            butReportPrint.Click += butReportPrint_Click;
            butReportPrintAct.Click += butReportPrintAct_Click;

            dataReport.CellEndEdit += dataReport_CellEndEdit;
        }

        void dataReport_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            if (DataReportCountChanged != null) DataReportCountChanged(this, EventArgs.Empty);
        }

        #region Проброс событий button
        void butReportPrintAct_Click(object sender, EventArgs e)
        {
            if (ButtonReportPrintActClick != null) ButtonReportPrintActClick(this, EventArgs.Empty);
        }

        void butReportPrint_Click(object sender, EventArgs e)
        {
            if (ButtonReportPrintClick != null) ButtonReportPrintClick(this, EventArgs.Empty);
        }

        void butSaveReport_Click(object sender, EventArgs e)
        {
            if (ButtonReportSaveClick != null) ButtonReportSaveClick(this, EventArgs.Empty);
        }

        void butReportView_Click(object sender, EventArgs e)
        {
            if (ButtonReportViewClick != null) ButtonReportViewClick(this, EventArgs.Empty);
        }

        void butDisposalStart_Click(object sender, EventArgs e)
        {
            if (ButtonDisposalStartClick != null) ButtonDisposalStartClick(this, EventArgs.Empty);
        }

        void butDisposalStop_Click(object sender, EventArgs e)
        {
            if (ButtonDisposalStopClick != null) ButtonDisposalStopClick(this, EventArgs.Empty);
        }

        void butDead_Click(object sender, EventArgs e)
        {
            if (ButtonDeadClick != null) ButtonDeadClick(this, EventArgs.Empty);
        }

        void butSuspendStart_Click(object sender, EventArgs e)
        {
            if (ButtonSuspendStartClick != null) ButtonSuspendStartClick(this, EventArgs.Empty);
        }

        void butSuspendStop_Click(object sender, EventArgs e)
        {
            if (ButtonSuspendStopClick != null) ButtonSuspendStopClick(this, EventArgs.Empty);
        }

        void butSaveDocuments_Click(object sender, EventArgs e)
        {
            if (ButtonSaveDocumentsClick != null) ButtonSaveDocumentsClick(this, EventArgs.Empty);
        }

        void butSaveHousing_Click(object sender, EventArgs e)
        {
            if (ButtonSaveHousingClick != null) ButtonSaveHousingClick(this, EventArgs.Empty);
        }

        void butSaveAddress_Click(object sender, EventArgs e)
        {
            if (ButtonSaveAddressClick != null) ButtonSaveAddressClick(this, EventArgs.Empty);
        }

        void butRemoveCategory_Click(object sender, EventArgs e)
        {
            if (ButtonRemoveCategoryToListClick != null) ButtonRemoveCategoryToListClick(this, EventArgs.Empty);
        }

        void butAddCategory_Click(object sender, EventArgs e)
        {
            if (ButtonAddCategoryToListClick != null) ButtonAddCategoryToListClick(this, EventArgs.Empty);
        }

        void butSaveHead_Click(object sender, EventArgs e)
        {
            if (ButtonSaveHeadClick != null) ButtonSaveHeadClick(this, EventArgs.Empty);
        }

        void butSaveInfo_Click(object sender, EventArgs e)
        {
            if (ButtonSaveInfoClick != null) ButtonSaveInfoClick(this, EventArgs.Empty);
        }

        #endregion

        #region Проборс событий на закрывающиеся combobox адресов
        void fldAppartament_DropDownClosed(object sender, EventArgs e)
        {
            if (AppartamentSelect != null) AppartamentSelect(this, EventArgs.Empty);
        }

        void fldHouse_DropDownClosed(object sender, EventArgs e)
        {
            if (HouseSelect != null) HouseSelect(this, EventArgs.Empty);
        }

        void fldStreet_DropDownClosed(object sender, EventArgs e)
        {
            if (StreetSelect != null) StreetSelect(this, EventArgs.Empty);
        }

        void fldCity_DropDownClosed(object sender, EventArgs e)
        {
            if (CitySelect != null) CitySelect(this, EventArgs.Empty);
        }

        void fldRegion_DropDownClosed(object sender, EventArgs e)
        {
            if (RegionSelect != null) RegionSelect(this, EventArgs.Empty);
        }
        #endregion

        void formClientAnketa_Load(object sender, EventArgs e)
        {
            if (LoadForm != null) LoadForm(this, EventArgs.Empty);
        }

        //MSDN :)
        void tabControl_DrawItem(object sender, DrawItemEventArgs e)
        {
            Graphics g = e.Graphics;
            Brush _textBrush;

            // Get the item from the collection.
            TabPage _tabPage = tabControl.TabPages[e.Index];

            // Get the real bounds for the tab rectangle.
            Rectangle _tabBounds = tabControl.GetTabRect(e.Index);

            if (e.State == DrawItemState.Selected)
            {

                // Draw a different background color, and don't paint a focus rectangle.
                _textBrush = new SolidBrush(Color.Black);
                g.FillRectangle(Brushes.LightGray, e.Bounds);
            }
            else
            {
                _textBrush = new System.Drawing.SolidBrush(e.ForeColor);
                e.DrawBackground();
            }

            // Use our own font.
            Font _tabFont = new Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));

            // Draw string. Center the text.
            StringFormat _stringFlags = new StringFormat();
            _stringFlags.Alignment = StringAlignment.Center;
            _stringFlags.LineAlignment = StringAlignment.Center;
            g.DrawString(_tabPage.Text, _tabFont, _textBrush, _tabBounds, new StringFormat(_stringFlags));
        }

        #region Основная информация о клиенте
        public string Lastname
        {
            get
            {
                return fldLastname.Text;
            }
            set
            {
                fldLastname.Text = value;
            }
        }

        public string Firstname
        {
            get
            {
                return fldFirstname.Text;
            }
            set
            {
                fldFirstname.Text = value;
            }
        }

        public string Middlename
        {
            get
            {
                return fldMiddlename.Text;
            }
            set
            {
                fldMiddlename.Text = value;
            }
        }

        public string Dateborn
        {
            get
            {
                return fldDateborn.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldDateborn.Text = value; 
            }
        }

        public string Snils
        {
            get
            {
                return fldSnils.Text;
            }
            set
            {
                fldSnils.Text = value;
            }
        }

        public string Phone
        {
            get
            {
                return fldPhone.Text;
            }
            set
            {
                fldPhone.Text = value;
            }
        }

        #endregion

        #region Информация для заведующих (установка ID)
        public int DepartamentId
        {
            get
            {
                try
                {
                    return (int)fldDepartament.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldDepartament.SelectedValue = value;
            }
        }

        public int SocialWorkerId
        {
            get
            {
                try
                {
                    return (int)fldSocialworker.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldSocialworker.SelectedValue = value;
            }
        }

        public int MedicalWorkerId
        {
            get
            {
                try
                {
                    return (int)fldMedicalworker.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldMedicalworker.SelectedValue = value;
            }
        }

        public int StatusId
        {
            get
            {
                try
                {
                    return (int)fldStatus.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldStatus.SelectedValue = value;
            }
        }

        public int HealthId
        {
            get
            {
                try
                {
                    return (int)fldHealth.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldHealth.SelectedValue = value;
            }
        }

        public int ConditionLivingId
        {
            get
            {
                try
                {
                    return (int)fldConditionLiving.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionLiving.SelectedValue = value;
            }
        }

        public string Job
        {
            get
            {
                return fldJob.Text;
            }
            set
            {
                fldJob.Text = value;
            }
        }

        #endregion

        #region Информация об адресе (установка ID)
        public int RegionId
        {
            get
            {
                return (int)fldRegion.SelectedValue;
            }
            set
            {
                fldRegion.SelectedValue = value;
            }
        }

        public int CityId
        {
            get
            {
                try
                {
                    return (int)fldCity.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                fldCity.SelectedValue = value;
            }
        }

        public int StreetId
        {
            get
            {
                try
                {
                    return (int)fldStreet.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                fldStreet.SelectedValue = value;
            }
        }

        public int HouseId
        {
            get
            {
                try
                {
                    return (int)fldHouse.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                fldHouse.SelectedValue = value;
            }
        }

        public int AppartamentId
        {
            get
            {
                try
                {
                    return (int)fldAppartament.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                fldAppartament.SelectedValue = value;
            }
        }

        public bool IsRefugee
        {
            get
            {
                if (checkRefugee.Checked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                bool isRefugee = value;
                if (isRefugee)
                {
                    checkRefugee.Checked = true;
                }
                else
                {
                    checkRefugee.Checked = false;
                }
            }
        }

        public bool IsHomeless
        {
            get
            {
                if (checkHomeless.Checked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                bool isHomeless = value;
                if (isHomeless)
                {
                    checkHomeless.Checked = true;
                }
                else
                {
                    checkHomeless.Checked = false;
                }
            }
        }

        #endregion

        #region Информация о жилище (установка ID)
        public int TypeHousingId
        {
            get
            {
                try
                {
                    return (int)fldTypeHousing.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldTypeHousing.SelectedValue = value;
            }
        }

        public int ConditionHousingId
        {
            get
            {
                try
                {
                    return (int)fldConditionHousing.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionHousing.SelectedValue = value;
            }
        }

        public int ConditionGasId
        {
            get
            {
                try
                {
                    return (int)fldConditionGas.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionGas.SelectedValue = value;
            }
        }

        public int ConditionWaterId
        {
            get
            {
                try
                {
                    return (int)fldConditionWater.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionWater.SelectedValue = value;
            }
        }

        public int ConditionHeatingId
        {
            get
            {
                try
                {
                    return (int)fldConditionHeating.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionHeating.SelectedValue = value;
            }
        }

        public int ConditionSanitaryId
        {
            get
            {
                try
                {
                    return (int)fldConditionSanitary.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                fldConditionSanitary.SelectedValue = value;
            }
        }

        public double HousingArea
        {
            get
            {
                return double.Parse(fldArea.Text);
            }
            set
            {
                fldArea.Text = value.ToString();
            }
        }

        public int HousingRoom
        {
            get
            {
                return Int32.Parse(fldCountRoom.Text);
            }
            set
            {
                fldCountRoom.Text = value.ToString();
            }
        }

        public bool RepairCosmetic
        {
            get
            {
                if (checkRepairCosmetic.Checked)
                    return true;
                else
                    return false;
            }
            set
            {
                bool isRepairCosmetic = value;
                if (isRepairCosmetic)
                    checkRepairCosmetic.Checked = true;
                else
                    checkRepairCosmetic.Checked = false;
            }
        }

        public bool RepairMajor
        {
            get
            {
                if (checkRepairMajor.Checked)
                    return true;
                else
                    return false;
            }
            set
            {
                bool isRepairMajor = value;
                if (isRepairMajor)
                    checkRepairMajor.Checked = true;
                else
                    checkRepairMajor.Checked = false;
            }
        }

        #endregion

        #region информация о документах
        public string PassportSerial
        {
            get
            {
                return fldPassportSerial.Text;
            }
            set
            {
                fldPassportSerial.Text = value;
            }
        }

        public string PassportNumber
        {
            get
            {
                return fldPassportNumber.Text;
            }
            set
            {
                fldPassportNumber.Text = value;
            }
        }

        public string PassportCode
        {
            get
            {
                return fldPassportCode.Text;
            }
            set
            {
                fldPassportCode.Text = value;
            }
        }

        public string PassportDate
        {
            get
            {
                return fldPassportDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldPassportDate.Text = value;
            }
        }

        public string PassportIssued
        {
            get
            {
                return fldPassportIssued.Text;
            }
            set
            {
                fldPassportIssued.Text = value;
            }
        }

        public string DisabilityGroup
        {
            get
            {
                return fldDisabilityGroup.Text;
            }
            set
            {
                fldDisabilityGroup.Text = value;
            }
        }

        public string DisabilityDegree
        {
            get
            {
                return fldDisabilityDegree.Text;
            }
            set
            {
                fldDisabilityDegree.Text = value;
            }
        }

        public string DisabilitySerial
        {
            get
            {
                return fldDisabilitySerial.Text;
            }
            set
            {
                fldDisabilitySerial.Text = value;
            }
        }

        public string DisabilityNumber
        {
            get
            {
                return fldDisabilityNumber.Text;
            }
            set
            {
                fldDisabilityNumber.Text = value;
            }
        }

        public string DisabilityDate
        {
            get
            {
                return fldDisabilityDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldDisabilityDate.Text = value;
            }
        }

        public string DisabilityAct
        {
            get
            {
                return fldDisabilityAct.Text;
            }
            set
            {
                fldDisabilityAct.Text = value;
            }
        }

        public string CertificatePensionNumber
        {
            get
            {
                return fldCertificatePensionNumber.Text;
            }
            set
            {
                fldCertificatePensionNumber.Text = value;
            }
        }

        public string CertificatePensionDate
        {
            get
            {
                return fldCertificatePensionDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificatePensionDate.Text = value;
            }
        }

        public string CertificateDisabledNumber
        {
            get
            {
                return fldCertificateDisabledNumber.Text; 
            }
            set
            {
                fldCertificateDisabledNumber.Text = value;
            }
        }

        public string CertificateDisabledDate
        {
            get
            {
                return fldCertificateDisabledDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateDisabledDate.Text = value;
            }
        }

        public string CertificateMemberNumber
        {
            get
            {
                return fldCertificateMemberNumber.Text;
            }
            set
            {
                fldCertificateMemberNumber.Text = value;
            }
        }

        public string CertificateMemberDate
        {
            get
            {
                return fldCertificateMemberDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateMemberDate.Text = value;
            }
        }

        public string CertificateVeteranNumber
        {
            get
            {
                return fldCertificateVeteranNumber.Text;
            }
            set
            {
                fldCertificateVeteranNumber.Text = value;
            }
        }

        public string CertificateVeteranDate
        {
            get
            {
                return fldCertificateVeteranDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateVeteranDate.Text = value;
            }
        }

        public string CertificateMemberLaborNumber
        {
            get
            {
                return fldCertificateMemberLaborNumber.Text;
            }
            set
            {
                fldCertificateMemberLaborNumber.Text = value;
            }
        }

        public string CertificateMemberLaborDate
        {
            get
            {
                return fldCertificateMemberLaborDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateMemberLaborDate.Text = value;
            }
        }

        #endregion

        private List<DirectoryDefault> lists = new List<DirectoryDefault>();
        public List<DirectoryDefault> Lists
        {
            set { lists = value; }
        }

        #region Заполнение Combobox
        private void FillingComboBox(ComboBox comboBox)
        {
            var sourse = (from a in lists
                          select new { a.Id, Names = a.Name }).ToList();
            comboBox.DataSource = sourse;
            comboBox.DisplayMember = "Names";
            comboBox.ValueMember = "Id";
        }

        public void DepartamentList()
        {
            FillingComboBox(fldDepartament);
        }

        public void SocialWorkerList()
        {
            FillingComboBox(fldSocialworker);
        }

        public void MedicalWorkerList()
        {
            FillingComboBox(fldMedicalworker);
        }

        public void StatusList()
        {
            FillingComboBox(fldStatus);
        }

        public void HealthList()
        {
            FillingComboBox(fldHealth);
        }

        public void ConditionLivingList()
        {
            FillingComboBox(fldConditionLiving);
        }

        public void CategoryList()
        {
            FillingComboBox(fldCategorys);
        }

        public void FillingCategory()
        {
            var sourse = categorys.ToList();
            dataCategorys.DataSource = sourse;
        }

        public void RegionFilling()
        {
            FillingComboBox(fldRegion);
        }

        public void CityFilling()
        {
            FillingComboBox(fldCity);
        }

        public void StreetFilling()
        {
            FillingComboBox(fldStreet);
        }

        public void HouseFilling()
        {
            FillingComboBox(fldHouse);
        }

        public void AppartamentFilling()
        {
            FillingComboBox(fldAppartament);
        }

        public void TypeHousingFilling()
        {
            FillingComboBox(fldTypeHousing);
        }

        public void ConditionHousingFilling()
        {
            FillingComboBox(fldConditionHousing);
        }

        public void ConditionGasFilling()
        {
            FillingComboBox(fldConditionGas);
        }

        public void ConditionWaterFilling()
        {
            FillingComboBox(fldConditionWater);
        }

        public void ConditionHeatingFilling()
        {
            FillingComboBox(fldConditionHeating);
        }

        public void ConditionSanitaryFilling()
        {
            FillingComboBox(fldConditionSanitary);
        }

        #endregion

        #region Проброс событий
        public event EventHandler ButtonSuspendStopClick;

        public event EventHandler ButtonSuspendStartClick;

        public event EventHandler ButtonDisposalStopClick;

        public event EventHandler ButtonDisposalStartClick;

        public event EventHandler ButtonDeadClick;

        public event EventHandler ButtonPrintAnketaClick;

        public event EventHandler ButtonSaveInfoClick;

        public event EventHandler ButtonSaveHeadClick;

        public event EventHandler ButtonSaveAddressClick;

        public event EventHandler ButtonSaveHousingClick;

        public event EventHandler ButtonSaveDocumentsClick;

        public event EventHandler ButtonAddCategoryToListClick;

        public event EventHandler ButtonRemoveCategoryToListClick;

        public event EventHandler RegionSelect;

        public event EventHandler CitySelect;

        public event EventHandler StreetSelect;

        public event EventHandler HouseSelect;

        public event EventHandler AppartamentSelect;

        public event EventHandler LoadForm;

        #endregion

        private List<DirectoryDefault> categorys = new List<DirectoryDefault>();

        public List<DirectoryDefault> Categorys
        {
            get
            {
                List<DirectoryDefault> list = new List<DirectoryDefault>();
                int count = dataCategorys.RowCount;
                for (int i = 0; i < count; i++)
                {
                    list.Add(new DirectoryDefault()
                        {
                            Id = Int32.Parse(dataCategorys[0,i].Value.ToString()),
                            Name = dataCategorys[1, i].Value.ToString()
                        });
                }

                return list;
            }
            set
            {
                categorys = value;
            }
        }

        public int CategoryId
        {
            get
            {
                return (int)fldCategorys.SelectedValue;
            }
        }

        public string CategoryName
        {
            get
            {
                return fldCategorys.Text;
            }
        }

        public int CategorySelectIndex 
        { 
            get
            {
                try
                {
                    return Convert.ToInt32(dataCategorys[0, dataCategorys.CurrentRow.Index].Value.ToString());
                }
                catch { return 0; }
            }
        }

        public bool IsDead
        {
            set
            {
                bool isDead = value;
                if(isDead)
                {
                    lblClientStatus.Text = "Клиент умер";
                }
            }
        }

        public bool IsDisposal
        {
            set
            {
                bool isDisposal = value;
                if (isDisposal)
                {
                    lblClientStatus.Text = "Клиент выбыл";
                }
            }
        }

        public bool IsSuspend
        {
            set
            {
                bool isSuspend = value;
                if (isSuspend)
                {
                    lblClientStatus.Text = "Обслуживание клиента приостановлено";
                }
            }
        }


        public string SetClientInfo
        {
            set { lblClientInfo.Text = value; }
        }

        public string SetHeadInfo
        {
            set { lblHeadInfo.Text = value; }
        }

        public string SetAddressInfo
        {
            set { lblAddressInfo.Text = value; }
        }

        public string SetHousingInfo
        {
            set { lblHousingInfo.Text = value; }
        }

        public string SetDocumentsInfo
        {
            set { lblDocumentsInfo.Text = value; }
        }

        #region Отчетность

        public string ReportServicePay
        {
            set { lblReportServicePay.Text = value; }
        }

        public double ReportTotal
        {
            set { lblReportTotal.Text = value.ToString(); }
        }

        public double ReportAllPay
        {
            set { lblReportAllPay.Text = value.ToString(); }
        }

        public string ReportDate
        {
            get
            {
                return fldReportDate.Value.ToString("MM;yyyy");
            }
        }

        public event EventHandler ButtonReportPrintClick;

        public event EventHandler ButtonReportPrintActClick;

        public event EventHandler ButtonReportViewClick;


        public bool IsReportGuaranteed
        {
            get 
            {
                if (radioReportGuaranteed.Checked)
                    return true;
                else
                    return false;
            }
        }

        public bool IsReportAdditional
        {
            get
            {
                if (radioReportAdditional.Checked)
                    return true;
                else
                    return false;
            }
        }

        private List<Report> reportList;

        public List<Report> ReportList
        {
            set
            {
                reportList = value;
            }
        }
           
        private List<ReportDetails> reportDetailsList;

        public List<ReportDetails> ReportDetailsList
        {
            get
            {
                return reportDetailsList;
            }
            set
            {
                reportDetailsList = value;
            }
        }

        public void FillingDataReport()
        {
            var sourse = reportDetailsList.ToList();
            dataReport.DataSource = sourse;

            dataReport.Columns["ServiceId"].Visible = false;
            dataReport.Columns["ClientSumm"].Visible = false;
            dataReport.Columns["ServiceName"].HeaderText = "Наименование услуги";
            dataReport.Columns["ServiceName"].ReadOnly = true;
            dataReport.Columns["ServiceSumm"].HeaderText = "Стоимость услуги";
            dataReport.Columns["ServiceSumm"].ReadOnly = true;
            dataReport.Columns["ServiceCount"].HeaderText = "Количество оказанных услуг";
        }

        #endregion


        public event EventHandler ButtonReportSaveClick;


        public event EventHandler DataReportCountChanged;


        public string DataValues
        {
            get
            {
                return dataReport["ServiceId", dataReport.CurrentRow.Index].Value.ToString();
            }
        }
    }
}
