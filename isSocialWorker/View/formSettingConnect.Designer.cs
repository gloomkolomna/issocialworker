﻿namespace isSocialWorker.View
{
    partial class formSettingConnect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formSettingConnect));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fldServer = new System.Windows.Forms.TextBox();
            this.fldNameDb = new System.Windows.Forms.TextBox();
            this.fldNameUser = new System.Windows.Forms.TextBox();
            this.fldPasswordUser = new System.Windows.Forms.TextBox();
            this.butSave = new System.Windows.Forms.Button();
            this.butCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cервер или IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя БД";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(138, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Пользователь";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 129);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Пароль";
            // 
            // fldServer
            // 
            this.fldServer.Location = new System.Drawing.Point(162, 16);
            this.fldServer.Name = "fldServer";
            this.fldServer.Size = new System.Drawing.Size(140, 30);
            this.fldServer.TabIndex = 4;
            // 
            // fldNameDb
            // 
            this.fldNameDb.Location = new System.Drawing.Point(162, 52);
            this.fldNameDb.Name = "fldNameDb";
            this.fldNameDb.Size = new System.Drawing.Size(140, 30);
            this.fldNameDb.TabIndex = 5;
            // 
            // fldNameUser
            // 
            this.fldNameUser.Location = new System.Drawing.Point(162, 88);
            this.fldNameUser.Name = "fldNameUser";
            this.fldNameUser.Size = new System.Drawing.Size(140, 30);
            this.fldNameUser.TabIndex = 6;
            // 
            // fldPasswordUser
            // 
            this.fldPasswordUser.Location = new System.Drawing.Point(162, 124);
            this.fldPasswordUser.Name = "fldPasswordUser";
            this.fldPasswordUser.Size = new System.Drawing.Size(140, 30);
            this.fldPasswordUser.TabIndex = 7;
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(16, 162);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(183, 35);
            this.butSave.TabIndex = 8;
            this.butSave.Text = "Сохранить";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butCancel
            // 
            this.butCancel.Location = new System.Drawing.Point(205, 162);
            this.butCancel.Name = "butCancel";
            this.butCancel.Size = new System.Drawing.Size(97, 35);
            this.butCancel.TabIndex = 9;
            this.butCancel.Text = "Отмена";
            this.butCancel.UseVisualStyleBackColor = true;
            // 
            // formSettingsConnect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 209);
            this.Controls.Add(this.butCancel);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.fldPasswordUser);
            this.Controls.Add(this.fldNameUser);
            this.Controls.Add(this.fldNameDb);
            this.Controls.Add(this.fldServer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formSettingsConnect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки подключения к БД";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fldServer;
        private System.Windows.Forms.TextBox fldNameDb;
        private System.Windows.Forms.TextBox fldNameUser;
        private System.Windows.Forms.TextBox fldPasswordUser;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butCancel;
    }
}