﻿using isSocialWorker.Model;
using isSocialWorker.Presenter;
using isSocialWorker.View;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace isSocialWorker
{
    public partial class formAutorization : Form
    {
        private static List<Users> UserList;

        public formAutorization()
        {
            InitializeComponent();
            butLogin.Click += butLogin_Click;
            butExit.Click += butExit_Click;
            this.Load += formAutorization_Load;
        }

        void butExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        void formAutorization_Load(object sender, EventArgs e)
        {
            UserList = AutorizationModel.GetUserList();
            if (UserList != null)
            {
                var loginList = UserList.Select(o => new { obj = o.Username }).GroupBy(f => f.obj);

                comboUser.DataSource = loginList.ToList();
                comboUser.DisplayMember = "Key";
            }
        }

        void butLogin_Click(object sender, EventArgs e)
        {
            Login();
        }

        private void Login()
        {
            if (AutorizationModel.isCheck(comboUser.Text, fldPassword.Text, UserList))
            {
                formBasic _formBasic = new formBasic();
                _formBasic.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Не верный пароль");
            }
        }

        private void menuSettingsConnect_Click(object sender, EventArgs e)
        {
            formSettingConnect form = new formSettingConnect();
            MessageService service = new MessageService();
            SettingConnectModel model = new SettingConnectModel();
            ConnectPresenter presenter = new ConnectPresenter(form, model, service);

            form.Show();
        }

        private void fldPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Login();
            }
        }
    }
}
