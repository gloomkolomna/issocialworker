﻿namespace isSocialWorker.View
{
    partial class formClientList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.groupBoxFilter = new System.Windows.Forms.GroupBox();
            this.checkIsSuspend = new System.Windows.Forms.CheckBox();
            this.checkIsDisposal = new System.Windows.Forms.CheckBox();
            this.checkMedicalworker = new System.Windows.Forms.CheckBox();
            this.fldMedicalworker = new System.Windows.Forms.ComboBox();
            this.fldSnils = new System.Windows.Forms.MaskedTextBox();
            this.checkSnils = new System.Windows.Forms.CheckBox();
            this.butFilterOk = new System.Windows.Forms.Button();
            this.checkIsDead = new System.Windows.Forms.CheckBox();
            this.checkNoCategory = new System.Windows.Forms.CheckBox();
            this.checkFIO = new System.Windows.Forms.CheckBox();
            this.checkStatus = new System.Windows.Forms.CheckBox();
            this.checkSocialworker = new System.Windows.Forms.CheckBox();
            this.checkDepartament = new System.Windows.Forms.CheckBox();
            this.checkCategory = new System.Windows.Forms.CheckBox();
            this.fldStatus = new System.Windows.Forms.ComboBox();
            this.grb_period = new System.Windows.Forms.GroupBox();
            this.radioAllPeriod = new System.Windows.Forms.RadioButton();
            this.fldPeriodEnd = new System.Windows.Forms.DateTimePicker();
            this.fldPeriodStart = new System.Windows.Forms.DateTimePicker();
            this.radioPeriod = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fldMiddlename = new System.Windows.Forms.TextBox();
            this.fldFirstname = new System.Windows.Forms.TextBox();
            this.fldLastname = new System.Windows.Forms.TextBox();
            this.fldSocialworker = new System.Windows.Forms.ComboBox();
            this.fldDepartament = new System.Windows.Forms.ComboBox();
            this.fldCategory = new System.Windows.Forms.ComboBox();
            this.dataClientList = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblCountClients = new System.Windows.Forms.ToolStripStatusLabel();
            this.butFilter = new System.Windows.Forms.ToolStripButton();
            this.butExportClientListToExcel = new System.Windows.Forms.ToolStripButton();
            this.butRefresh = new System.Windows.Forms.ToolStripButton();
            this.toolStripSplitButton1 = new System.Windows.Forms.ToolStripSplitButton();
            this.butRecalculatePayments = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1.SuspendLayout();
            this.groupBoxFilter.SuspendLayout();
            this.grb_period.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataClientList)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butFilter,
            this.butExportClientListToExcel,
            this.butRefresh,
            this.toolStripSplitButton1});
            this.toolStrip1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(907, 39);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "Панель управления";
            // 
            // groupBoxFilter
            // 
            this.groupBoxFilter.Controls.Add(this.checkIsSuspend);
            this.groupBoxFilter.Controls.Add(this.checkIsDisposal);
            this.groupBoxFilter.Controls.Add(this.checkMedicalworker);
            this.groupBoxFilter.Controls.Add(this.fldMedicalworker);
            this.groupBoxFilter.Controls.Add(this.fldSnils);
            this.groupBoxFilter.Controls.Add(this.checkSnils);
            this.groupBoxFilter.Controls.Add(this.butFilterOk);
            this.groupBoxFilter.Controls.Add(this.checkIsDead);
            this.groupBoxFilter.Controls.Add(this.checkNoCategory);
            this.groupBoxFilter.Controls.Add(this.checkFIO);
            this.groupBoxFilter.Controls.Add(this.checkStatus);
            this.groupBoxFilter.Controls.Add(this.checkSocialworker);
            this.groupBoxFilter.Controls.Add(this.checkDepartament);
            this.groupBoxFilter.Controls.Add(this.checkCategory);
            this.groupBoxFilter.Controls.Add(this.fldStatus);
            this.groupBoxFilter.Controls.Add(this.grb_period);
            this.groupBoxFilter.Controls.Add(this.label3);
            this.groupBoxFilter.Controls.Add(this.label2);
            this.groupBoxFilter.Controls.Add(this.label1);
            this.groupBoxFilter.Controls.Add(this.fldMiddlename);
            this.groupBoxFilter.Controls.Add(this.fldFirstname);
            this.groupBoxFilter.Controls.Add(this.fldLastname);
            this.groupBoxFilter.Controls.Add(this.fldSocialworker);
            this.groupBoxFilter.Controls.Add(this.fldDepartament);
            this.groupBoxFilter.Controls.Add(this.fldCategory);
            this.groupBoxFilter.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBoxFilter.Location = new System.Drawing.Point(0, 39);
            this.groupBoxFilter.Name = "groupBoxFilter";
            this.groupBoxFilter.Size = new System.Drawing.Size(907, 334);
            this.groupBoxFilter.TabIndex = 6;
            this.groupBoxFilter.TabStop = false;
            this.groupBoxFilter.Text = "Фильтр выбора";
            this.groupBoxFilter.Visible = false;
            // 
            // checkIsSuspend
            // 
            this.checkIsSuspend.AutoSize = true;
            this.checkIsSuspend.Location = new System.Drawing.Point(679, 229);
            this.checkIsSuspend.Name = "checkIsSuspend";
            this.checkIsSuspend.Size = new System.Drawing.Size(213, 25);
            this.checkIsSuspend.TabIndex = 31;
            this.checkIsSuspend.Text = "Приостановленные";
            this.checkIsSuspend.UseVisualStyleBackColor = true;
            // 
            // checkIsDisposal
            // 
            this.checkIsDisposal.AutoSize = true;
            this.checkIsDisposal.Location = new System.Drawing.Point(679, 198);
            this.checkIsDisposal.Name = "checkIsDisposal";
            this.checkIsDisposal.Size = new System.Drawing.Size(135, 25);
            this.checkIsDisposal.TabIndex = 30;
            this.checkIsDisposal.Text = "Выбывшие";
            this.checkIsDisposal.UseVisualStyleBackColor = true;
            // 
            // checkMedicalworker
            // 
            this.checkMedicalworker.AutoSize = true;
            this.checkMedicalworker.Location = new System.Drawing.Point(6, 138);
            this.checkMedicalworker.Name = "checkMedicalworker";
            this.checkMedicalworker.Size = new System.Drawing.Size(198, 25);
            this.checkMedicalworker.TabIndex = 29;
            this.checkMedicalworker.Text = "По медработнику";
            this.checkMedicalworker.UseVisualStyleBackColor = true;
            // 
            // fldMedicalworker
            // 
            this.fldMedicalworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldMedicalworker.Enabled = false;
            this.fldMedicalworker.FormattingEnabled = true;
            this.fldMedicalworker.Location = new System.Drawing.Point(207, 136);
            this.fldMedicalworker.Name = "fldMedicalworker";
            this.fldMedicalworker.Size = new System.Drawing.Size(466, 29);
            this.fldMedicalworker.TabIndex = 28;
            // 
            // fldSnils
            // 
            this.fldSnils.Enabled = false;
            this.fldSnils.Location = new System.Drawing.Point(207, 266);
            this.fldSnils.Mask = "000-000-000 00";
            this.fldSnils.Name = "fldSnils";
            this.fldSnils.Size = new System.Drawing.Size(189, 30);
            this.fldSnils.TabIndex = 27;
            // 
            // checkSnils
            // 
            this.checkSnils.AutoSize = true;
            this.checkSnils.Location = new System.Drawing.Point(6, 268);
            this.checkSnils.Name = "checkSnils";
            this.checkSnils.Size = new System.Drawing.Size(132, 25);
            this.checkSnils.TabIndex = 26;
            this.checkSnils.Text = "По СНИЛС";
            this.checkSnils.UseVisualStyleBackColor = true;
            // 
            // butFilterOk
            // 
            this.butFilterOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.butFilterOk.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.butFilterOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butFilterOk.Location = new System.Drawing.Point(730, 284);
            this.butFilterOk.Name = "butFilterOk";
            this.butFilterOk.Size = new System.Drawing.Size(171, 44);
            this.butFilterOk.TabIndex = 24;
            this.butFilterOk.Text = "Применить";
            this.butFilterOk.UseVisualStyleBackColor = true;
            // 
            // checkIsDead
            // 
            this.checkIsDead.AutoSize = true;
            this.checkIsDead.Location = new System.Drawing.Point(679, 167);
            this.checkIsDead.Name = "checkIsDead";
            this.checkIsDead.Size = new System.Drawing.Size(118, 25);
            this.checkIsDead.TabIndex = 23;
            this.checkIsDead.Text = "Умершие";
            this.checkIsDead.UseVisualStyleBackColor = true;
            // 
            // checkNoCategory
            // 
            this.checkNoCategory.AutoSize = true;
            this.checkNoCategory.Location = new System.Drawing.Point(507, 27);
            this.checkNoCategory.Name = "checkNoCategory";
            this.checkNoCategory.Size = new System.Drawing.Size(167, 25);
            this.checkNoCategory.TabIndex = 22;
            this.checkNoCategory.Text = "Без категории";
            this.checkNoCategory.UseVisualStyleBackColor = true;
            // 
            // checkFIO
            // 
            this.checkFIO.AutoSize = true;
            this.checkFIO.Location = new System.Drawing.Point(6, 212);
            this.checkFIO.Name = "checkFIO";
            this.checkFIO.Size = new System.Drawing.Size(106, 25);
            this.checkFIO.TabIndex = 21;
            this.checkFIO.Text = "По ФИО";
            this.checkFIO.UseVisualStyleBackColor = true;
            // 
            // checkStatus
            // 
            this.checkStatus.AutoSize = true;
            this.checkStatus.Location = new System.Drawing.Point(6, 175);
            this.checkStatus.Name = "checkStatus";
            this.checkStatus.Size = new System.Drawing.Size(132, 25);
            this.checkStatus.TabIndex = 20;
            this.checkStatus.Text = "По статусу";
            this.checkStatus.UseVisualStyleBackColor = true;
            // 
            // checkSocialworker
            // 
            this.checkSocialworker.AutoSize = true;
            this.checkSocialworker.Location = new System.Drawing.Point(6, 101);
            this.checkSocialworker.Name = "checkSocialworker";
            this.checkSocialworker.Size = new System.Drawing.Size(196, 25);
            this.checkSocialworker.TabIndex = 19;
            this.checkSocialworker.Text = "По соцработнику";
            this.checkSocialworker.UseVisualStyleBackColor = true;
            // 
            // checkDepartament
            // 
            this.checkDepartament.AutoSize = true;
            this.checkDepartament.Location = new System.Drawing.Point(6, 64);
            this.checkDepartament.Name = "checkDepartament";
            this.checkDepartament.Size = new System.Drawing.Size(164, 25);
            this.checkDepartament.TabIndex = 18;
            this.checkDepartament.Text = "По отделению";
            this.checkDepartament.UseVisualStyleBackColor = true;
            // 
            // checkCategory
            // 
            this.checkCategory.AutoSize = true;
            this.checkCategory.Location = new System.Drawing.Point(6, 27);
            this.checkCategory.Name = "checkCategory";
            this.checkCategory.Size = new System.Drawing.Size(160, 25);
            this.checkCategory.TabIndex = 17;
            this.checkCategory.Text = "По категории";
            this.checkCategory.UseVisualStyleBackColor = true;
            // 
            // fldStatus
            // 
            this.fldStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldStatus.Enabled = false;
            this.fldStatus.FormattingEnabled = true;
            this.fldStatus.Location = new System.Drawing.Point(207, 173);
            this.fldStatus.Name = "fldStatus";
            this.fldStatus.Size = new System.Drawing.Size(466, 29);
            this.fldStatus.TabIndex = 16;
            // 
            // grb_period
            // 
            this.grb_period.Controls.Add(this.radioAllPeriod);
            this.grb_period.Controls.Add(this.fldPeriodEnd);
            this.grb_period.Controls.Add(this.fldPeriodStart);
            this.grb_period.Controls.Add(this.radioPeriod);
            this.grb_period.Location = new System.Drawing.Point(679, 13);
            this.grb_period.Name = "grb_period";
            this.grb_period.Size = new System.Drawing.Size(171, 148);
            this.grb_period.TabIndex = 12;
            this.grb_period.TabStop = false;
            // 
            // radioAllPeriod
            // 
            this.radioAllPeriod.AutoSize = true;
            this.radioAllPeriod.Checked = true;
            this.radioAllPeriod.Location = new System.Drawing.Point(5, 14);
            this.radioAllPeriod.Name = "radioAllPeriod";
            this.radioAllPeriod.Size = new System.Drawing.Size(153, 25);
            this.radioAllPeriod.TabIndex = 14;
            this.radioAllPeriod.TabStop = true;
            this.radioAllPeriod.Text = "За все время";
            this.radioAllPeriod.UseVisualStyleBackColor = true;
            // 
            // fldPeriodEnd
            // 
            this.fldPeriodEnd.Enabled = false;
            this.fldPeriodEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldPeriodEnd.Location = new System.Drawing.Point(5, 110);
            this.fldPeriodEnd.Name = "fldPeriodEnd";
            this.fldPeriodEnd.Size = new System.Drawing.Size(153, 30);
            this.fldPeriodEnd.TabIndex = 3;
            // 
            // fldPeriodStart
            // 
            this.fldPeriodStart.Enabled = false;
            this.fldPeriodStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldPeriodStart.Location = new System.Drawing.Point(5, 72);
            this.fldPeriodStart.Name = "fldPeriodStart";
            this.fldPeriodStart.Size = new System.Drawing.Size(153, 30);
            this.fldPeriodStart.TabIndex = 2;
            // 
            // radioPeriod
            // 
            this.radioPeriod.AutoSize = true;
            this.radioPeriod.Location = new System.Drawing.Point(5, 43);
            this.radioPeriod.Name = "radioPeriod";
            this.radioPeriod.Size = new System.Drawing.Size(159, 25);
            this.radioPeriod.TabIndex = 2;
            this.radioPeriod.Text = "За период ДР";
            this.radioPeriod.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label3.Location = new System.Drawing.Point(524, 242);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Отчество";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label2.Location = new System.Drawing.Point(365, 242);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 15);
            this.label2.TabIndex = 10;
            this.label2.Text = "Имя";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.label1.Location = new System.Drawing.Point(207, 242);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 15);
            this.label1.TabIndex = 9;
            this.label1.Text = "Фамилия";
            // 
            // fldMiddlename
            // 
            this.fldMiddlename.Enabled = false;
            this.fldMiddlename.Location = new System.Drawing.Point(524, 210);
            this.fldMiddlename.Name = "fldMiddlename";
            this.fldMiddlename.Size = new System.Drawing.Size(149, 30);
            this.fldMiddlename.TabIndex = 8;
            // 
            // fldFirstname
            // 
            this.fldFirstname.Enabled = false;
            this.fldFirstname.Location = new System.Drawing.Point(365, 210);
            this.fldFirstname.Name = "fldFirstname";
            this.fldFirstname.Size = new System.Drawing.Size(153, 30);
            this.fldFirstname.TabIndex = 7;
            // 
            // fldLastname
            // 
            this.fldLastname.Enabled = false;
            this.fldLastname.Location = new System.Drawing.Point(207, 210);
            this.fldLastname.Name = "fldLastname";
            this.fldLastname.Size = new System.Drawing.Size(153, 30);
            this.fldLastname.TabIndex = 6;
            // 
            // fldSocialworker
            // 
            this.fldSocialworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldSocialworker.Enabled = false;
            this.fldSocialworker.FormattingEnabled = true;
            this.fldSocialworker.Location = new System.Drawing.Point(207, 99);
            this.fldSocialworker.Name = "fldSocialworker";
            this.fldSocialworker.Size = new System.Drawing.Size(466, 29);
            this.fldSocialworker.TabIndex = 5;
            // 
            // fldDepartament
            // 
            this.fldDepartament.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldDepartament.Enabled = false;
            this.fldDepartament.FormattingEnabled = true;
            this.fldDepartament.Location = new System.Drawing.Point(207, 62);
            this.fldDepartament.Name = "fldDepartament";
            this.fldDepartament.Size = new System.Drawing.Size(466, 29);
            this.fldDepartament.TabIndex = 4;
            // 
            // fldCategory
            // 
            this.fldCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldCategory.Enabled = false;
            this.fldCategory.FormattingEnabled = true;
            this.fldCategory.Location = new System.Drawing.Point(207, 25);
            this.fldCategory.Name = "fldCategory";
            this.fldCategory.Size = new System.Drawing.Size(289, 29);
            this.fldCategory.TabIndex = 2;
            // 
            // dataClientList
            // 
            this.dataClientList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataClientList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataClientList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataClientList.Location = new System.Drawing.Point(0, 373);
            this.dataClientList.MultiSelect = false;
            this.dataClientList.Name = "dataClientList";
            this.dataClientList.ReadOnly = true;
            this.dataClientList.RowTemplate.Height = 24;
            this.dataClientList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataClientList.Size = new System.Drawing.Size(907, 267);
            this.dataClientList.TabIndex = 7;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblCountClients});
            this.statusStrip1.Location = new System.Drawing.Point(0, 640);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(907, 22);
            this.statusStrip1.TabIndex = 8;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(132, 17);
            this.toolStripStatusLabel1.Text = "Количество клиентов: ";
            // 
            // lblCountClients
            // 
            this.lblCountClients.Name = "lblCountClients";
            this.lblCountClients.Size = new System.Drawing.Size(13, 17);
            this.lblCountClients.Text = "0";
            // 
            // butFilter
            // 
            this.butFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butFilter.Image = global::isSocialWorker.Properties.Resources.filter;
            this.butFilter.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butFilter.Name = "butFilter";
            this.butFilter.Size = new System.Drawing.Size(36, 36);
            this.butFilter.Text = "Фильтр клиентов";
            // 
            // butExportClientListToExcel
            // 
            this.butExportClientListToExcel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butExportClientListToExcel.Image = global::isSocialWorker.Properties.Resources.excel_exports;
            this.butExportClientListToExcel.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butExportClientListToExcel.Name = "butExportClientListToExcel";
            this.butExportClientListToExcel.Size = new System.Drawing.Size(36, 36);
            this.butExportClientListToExcel.Text = "Выгрузить список в Excel";
            // 
            // butRefresh
            // 
            this.butRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butRefresh.Image = global::isSocialWorker.Properties.Resources.database_refresh;
            this.butRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butRefresh.Name = "butRefresh";
            this.butRefresh.Size = new System.Drawing.Size(36, 36);
            this.butRefresh.Text = "Обновить список";
            // 
            // toolStripSplitButton1
            // 
            this.toolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSplitButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butRecalculatePayments});
            this.toolStripSplitButton1.Image = global::isSocialWorker.Properties.Resources.service_status;
            this.toolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripSplitButton1.Name = "toolStripSplitButton1";
            this.toolStripSplitButton1.Size = new System.Drawing.Size(48, 36);
            this.toolStripSplitButton1.Text = "Операции над списком";
            // 
            // butRecalculatePayments
            // 
            this.butRecalculatePayments.Name = "butRecalculatePayments";
            this.butRecalculatePayments.Size = new System.Drawing.Size(232, 22);
            this.butRecalculatePayments.Text = "Перерасчет условий оплаты";
            // 
            // formClientList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(907, 662);
            this.Controls.Add(this.dataClientList);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBoxFilter);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formClientList";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Список клиентов";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBoxFilter.ResumeLayout(false);
            this.groupBoxFilter.PerformLayout();
            this.grb_period.ResumeLayout(false);
            this.grb_period.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataClientList)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton butFilter;
        private System.Windows.Forms.ToolStripButton butExportClientListToExcel;
        private System.Windows.Forms.GroupBox groupBoxFilter;
        private System.Windows.Forms.CheckBox checkMedicalworker;
        private System.Windows.Forms.ComboBox fldMedicalworker;
        private System.Windows.Forms.MaskedTextBox fldSnils;
        private System.Windows.Forms.CheckBox checkSnils;
        private System.Windows.Forms.Button butFilterOk;
        private System.Windows.Forms.CheckBox checkIsDead;
        private System.Windows.Forms.CheckBox checkNoCategory;
        private System.Windows.Forms.CheckBox checkFIO;
        private System.Windows.Forms.CheckBox checkStatus;
        private System.Windows.Forms.CheckBox checkSocialworker;
        private System.Windows.Forms.CheckBox checkDepartament;
        private System.Windows.Forms.CheckBox checkCategory;
        private System.Windows.Forms.ComboBox fldStatus;
        private System.Windows.Forms.GroupBox grb_period;
        private System.Windows.Forms.RadioButton radioAllPeriod;
        private System.Windows.Forms.DateTimePicker fldPeriodEnd;
        private System.Windows.Forms.DateTimePicker fldPeriodStart;
        private System.Windows.Forms.RadioButton radioPeriod;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fldMiddlename;
        private System.Windows.Forms.TextBox fldFirstname;
        private System.Windows.Forms.TextBox fldLastname;
        private System.Windows.Forms.ComboBox fldSocialworker;
        private System.Windows.Forms.ComboBox fldDepartament;
        private System.Windows.Forms.ComboBox fldCategory;
        private System.Windows.Forms.DataGridView dataClientList;
        private System.Windows.Forms.CheckBox checkIsSuspend;
        private System.Windows.Forms.CheckBox checkIsDisposal;
        private System.Windows.Forms.ToolStripSplitButton toolStripSplitButton1;
        private System.Windows.Forms.ToolStripMenuItem butRecalculatePayments;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblCountClients;
        private System.Windows.Forms.ToolStripButton butRefresh;
    }
}