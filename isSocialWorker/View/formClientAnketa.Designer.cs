﻿namespace isSocialWorker.View
{
    partial class formClientAnketa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formClientAnketa));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabInfo = new System.Windows.Forms.TabPage();
            this.panelInfo = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.fldSnils = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fldPhone = new System.Windows.Forms.TextBox();
            this.fldLastname = new System.Windows.Forms.TextBox();
            this.fldDateborn = new System.Windows.Forms.DateTimePicker();
            this.fldFirstname = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.fldMiddlename = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.lblClientInfo = new System.Windows.Forms.ToolStripLabel();
            this.tabHead = new System.Windows.Forms.TabPage();
            this.panelHead = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.fldJob = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.fldConditionLiving = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.fldHealth = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.fldStatus = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.fldMedicalworker = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.fldSocialworker = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataCategorys = new System.Windows.Forms.DataGridView();
            this.butRemoveCategory = new System.Windows.Forms.Button();
            this.butAddCategory = new System.Windows.Forms.Button();
            this.fldCategorys = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.fldDepartament = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.toolStrip4 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.lblHeadInfo = new System.Windows.Forms.ToolStripLabel();
            this.tabAddress = new System.Windows.Forms.TabPage();
            this.panelAddress = new System.Windows.Forms.Panel();
            this.groupBoxAddress = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.fldRegion = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.fldCity = new System.Windows.Forms.ComboBox();
            this.fldStreet = new System.Windows.Forms.ComboBox();
            this.fldHouse = new System.Windows.Forms.ComboBox();
            this.fldAppartament = new System.Windows.Forms.ComboBox();
            this.checkRefugee = new System.Windows.Forms.CheckBox();
            this.checkHomeless = new System.Windows.Forms.CheckBox();
            this.toolStrip3 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.lblAddressInfo = new System.Windows.Forms.ToolStripLabel();
            this.tabHousing = new System.Windows.Forms.TabPage();
            this.panelHousing = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBoxCondition = new System.Windows.Forms.GroupBox();
            this.fldConditionSanitary = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.fldConditionHeating = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.fldConditionWater = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.fldConditionGas = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBoxRepair = new System.Windows.Forms.GroupBox();
            this.checkRepairCosmetic = new System.Windows.Forms.CheckBox();
            this.checkRepairMajor = new System.Windows.Forms.CheckBox();
            this.fldTypeHousing = new System.Windows.Forms.ComboBox();
            this.fldConditionHousing = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.fldArea = new System.Windows.Forms.TextBox();
            this.fldCountRoom = new System.Windows.Forms.TextBox();
            this.toolStrip5 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.lblHousingInfo = new System.Windows.Forms.ToolStripLabel();
            this.tabDocuments = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabDocumentsBasic = new System.Windows.Forms.TabPage();
            this.panelDocumentsBasic = new System.Windows.Forms.Panel();
            this.groupDocumentsDisability = new System.Windows.Forms.GroupBox();
            this.fldDisabilityDegree = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilityGroup = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilityAct = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.fldDisabilityDate = new System.Windows.Forms.DateTimePicker();
            this.fldDisabilityNumber = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilitySerial = new System.Windows.Forms.MaskedTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.groupDocumentsPassport = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.fldPassportCode = new System.Windows.Forms.MaskedTextBox();
            this.fldPassportIssued = new System.Windows.Forms.TextBox();
            this.fldPassportDate = new System.Windows.Forms.DateTimePicker();
            this.fldPassportNumber = new System.Windows.Forms.MaskedTextBox();
            this.fldPassportSerial = new System.Windows.Forms.MaskedTextBox();
            this.tabDocumentsSertificate = new System.Windows.Forms.TabPage();
            this.panelDocumentsSertificate = new System.Windows.Forms.Panel();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.fldCertificatePensionNumber = new System.Windows.Forms.TextBox();
            this.fldCertificatePensionDate = new System.Windows.Forms.DateTimePicker();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.fldCertificateMemberLaborNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateMemberLaborDate = new System.Windows.Forms.DateTimePicker();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fldCertificateDisabledNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateDisabledDate = new System.Windows.Forms.DateTimePicker();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.fldCertificateVeteranNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateVeteranDate = new System.Windows.Forms.DateTimePicker();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.fldCertificateMemberNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateMemberDate = new System.Windows.Forms.DateTimePicker();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.toolStrip6 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.lblDocumentsInfo = new System.Windows.Forms.ToolStripLabel();
            this.tabReports = new System.Windows.Forms.TabPage();
            this.dataReport = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblReportServicePay = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblReportTotal = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel4 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel5 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblReportAllPay = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radioReportAdditional = new System.Windows.Forms.RadioButton();
            this.radioReportGuaranteed = new System.Windows.Forms.RadioButton();
            this.fldReportDate = new System.Windows.Forms.DateTimePicker();
            this.toolStrip7 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.lblClientStatus = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tabIncome = new System.Windows.Forms.TabPage();
            this.fldReportServices = new System.Windows.Forms.ComboBox();
            this.butSaveInfo = new System.Windows.Forms.ToolStripButton();
            this.butSaveHead = new System.Windows.Forms.ToolStripButton();
            this.butSaveAddress = new System.Windows.Forms.ToolStripButton();
            this.butSaveHousing = new System.Windows.Forms.ToolStripButton();
            this.butSaveDocuments = new System.Windows.Forms.ToolStripButton();
            this.butReportInsert = new System.Windows.Forms.Button();
            this.butReportView = new System.Windows.Forms.Button();
            this.butSaveReport = new System.Windows.Forms.ToolStripButton();
            this.butReportPrint = new System.Windows.Forms.ToolStripButton();
            this.butReportPrintAct = new System.Windows.Forms.ToolStripButton();
            this.toolStripDropDownButton1 = new System.Windows.Forms.ToolStripDropDownButton();
            this.обслуживаниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.butSuspendStop = new System.Windows.Forms.ToolStripMenuItem();
            this.butSuspendStart = new System.Windows.Forms.ToolStripMenuItem();
            this.выбытиеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.butDisposalStop = new System.Windows.Forms.ToolStripMenuItem();
            this.butDisposalStart = new System.Windows.Forms.ToolStripMenuItem();
            this.butDead = new System.Windows.Forms.ToolStripMenuItem();
            this.butPrintClientCard = new System.Windows.Forms.ToolStripButton();
            this.tabControl.SuspendLayout();
            this.tabInfo.SuspendLayout();
            this.panelInfo.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.tabHead.SuspendLayout();
            this.panelHead.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataCategorys)).BeginInit();
            this.toolStrip4.SuspendLayout();
            this.tabAddress.SuspendLayout();
            this.panelAddress.SuspendLayout();
            this.groupBoxAddress.SuspendLayout();
            this.toolStrip3.SuspendLayout();
            this.tabHousing.SuspendLayout();
            this.panelHousing.SuspendLayout();
            this.groupBoxCondition.SuspendLayout();
            this.groupBoxRepair.SuspendLayout();
            this.toolStrip5.SuspendLayout();
            this.tabDocuments.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabDocumentsBasic.SuspendLayout();
            this.panelDocumentsBasic.SuspendLayout();
            this.groupDocumentsDisability.SuspendLayout();
            this.groupDocumentsPassport.SuspendLayout();
            this.tabDocumentsSertificate.SuspendLayout();
            this.panelDocumentsSertificate.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.toolStrip6.SuspendLayout();
            this.tabReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReport)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip7.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl.Controls.Add(this.tabInfo);
            this.tabControl.Controls.Add(this.tabHead);
            this.tabControl.Controls.Add(this.tabAddress);
            this.tabControl.Controls.Add(this.tabHousing);
            this.tabControl.Controls.Add(this.tabDocuments);
            this.tabControl.Controls.Add(this.tabReports);
            this.tabControl.Controls.Add(this.tabIncome);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl.ItemSize = new System.Drawing.Size(45, 200);
            this.tabControl.Location = new System.Drawing.Point(0, 25);
            this.tabControl.Multiline = true;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(773, 507);
            this.tabControl.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl.TabIndex = 0;
            // 
            // tabInfo
            // 
            this.tabInfo.Controls.Add(this.panelInfo);
            this.tabInfo.Controls.Add(this.toolStrip2);
            this.tabInfo.Location = new System.Drawing.Point(204, 4);
            this.tabInfo.Name = "tabInfo";
            this.tabInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabInfo.Size = new System.Drawing.Size(565, 499);
            this.tabInfo.TabIndex = 0;
            this.tabInfo.Text = "Данные клиента";
            this.tabInfo.UseVisualStyleBackColor = true;
            // 
            // panelInfo
            // 
            this.panelInfo.Controls.Add(this.label3);
            this.panelInfo.Controls.Add(this.label1);
            this.panelInfo.Controls.Add(this.fldSnils);
            this.panelInfo.Controls.Add(this.label2);
            this.panelInfo.Controls.Add(this.fldPhone);
            this.panelInfo.Controls.Add(this.fldLastname);
            this.panelInfo.Controls.Add(this.fldDateborn);
            this.panelInfo.Controls.Add(this.fldFirstname);
            this.panelInfo.Controls.Add(this.label6);
            this.panelInfo.Controls.Add(this.fldMiddlename);
            this.panelInfo.Controls.Add(this.label5);
            this.panelInfo.Controls.Add(this.label4);
            this.panelInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelInfo.Location = new System.Drawing.Point(3, 42);
            this.panelInfo.Name = "panelInfo";
            this.panelInfo.Size = new System.Drawing.Size(559, 454);
            this.panelInfo.TabIndex = 13;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Отчество";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия";
            // 
            // fldSnils
            // 
            this.fldSnils.Location = new System.Drawing.Point(172, 186);
            this.fldSnils.Mask = "999-999-999 99";
            this.fldSnils.Name = "fldSnils";
            this.fldSnils.Size = new System.Drawing.Size(382, 30);
            this.fldSnils.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя";
            // 
            // fldPhone
            // 
            this.fldPhone.Location = new System.Drawing.Point(172, 149);
            this.fldPhone.Name = "fldPhone";
            this.fldPhone.Size = new System.Drawing.Size(382, 30);
            this.fldPhone.TabIndex = 10;
            // 
            // fldLastname
            // 
            this.fldLastname.Location = new System.Drawing.Point(172, 3);
            this.fldLastname.Name = "fldLastname";
            this.fldLastname.Size = new System.Drawing.Size(382, 30);
            this.fldLastname.TabIndex = 3;
            // 
            // fldDateborn
            // 
            this.fldDateborn.Location = new System.Drawing.Point(172, 112);
            this.fldDateborn.Name = "fldDateborn";
            this.fldDateborn.Size = new System.Drawing.Size(382, 30);
            this.fldDateborn.TabIndex = 9;
            // 
            // fldFirstname
            // 
            this.fldFirstname.Location = new System.Drawing.Point(172, 40);
            this.fldFirstname.Name = "fldFirstname";
            this.fldFirstname.Size = new System.Drawing.Size(382, 30);
            this.fldFirstname.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 191);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(81, 21);
            this.label6.TabIndex = 8;
            this.label6.Text = "СНИЛС";
            // 
            // fldMiddlename
            // 
            this.fldMiddlename.Location = new System.Drawing.Point(172, 77);
            this.fldMiddlename.Name = "fldMiddlename";
            this.fldMiddlename.Size = new System.Drawing.Size(382, 30);
            this.fldMiddlename.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 154);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "Телефон";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Дата рождения";
            // 
            // toolStrip2
            // 
            this.toolStrip2.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip2.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveInfo,
            this.toolStripSeparator3,
            this.lblClientInfo});
            this.toolStrip2.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip2.Size = new System.Drawing.Size(559, 39);
            this.toolStrip2.TabIndex = 12;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 39);
            // 
            // lblClientInfo
            // 
            this.lblClientInfo.Name = "lblClientInfo";
            this.lblClientInfo.Size = new System.Drawing.Size(0, 36);
            // 
            // tabHead
            // 
            this.tabHead.Controls.Add(this.panelHead);
            this.tabHead.Controls.Add(this.toolStrip4);
            this.tabHead.Location = new System.Drawing.Point(204, 4);
            this.tabHead.Name = "tabHead";
            this.tabHead.Padding = new System.Windows.Forms.Padding(3);
            this.tabHead.Size = new System.Drawing.Size(565, 499);
            this.tabHead.TabIndex = 1;
            this.tabHead.Text = "Заведующим";
            this.tabHead.UseVisualStyleBackColor = true;
            // 
            // panelHead
            // 
            this.panelHead.Controls.Add(this.label11);
            this.panelHead.Controls.Add(this.fldJob);
            this.panelHead.Controls.Add(this.label10);
            this.panelHead.Controls.Add(this.fldConditionLiving);
            this.panelHead.Controls.Add(this.label9);
            this.panelHead.Controls.Add(this.fldHealth);
            this.panelHead.Controls.Add(this.label8);
            this.panelHead.Controls.Add(this.fldStatus);
            this.panelHead.Controls.Add(this.label7);
            this.panelHead.Controls.Add(this.fldMedicalworker);
            this.panelHead.Controls.Add(this.label12);
            this.panelHead.Controls.Add(this.fldSocialworker);
            this.panelHead.Controls.Add(this.groupBox1);
            this.panelHead.Controls.Add(this.fldDepartament);
            this.panelHead.Controls.Add(this.label14);
            this.panelHead.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHead.Location = new System.Drawing.Point(3, 42);
            this.panelHead.Name = "panelHead";
            this.panelHead.Size = new System.Drawing.Size(559, 454);
            this.panelHead.TabIndex = 29;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 8);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(111, 21);
            this.label11.TabIndex = 14;
            this.label11.Text = "Отделение";
            // 
            // fldJob
            // 
            this.fldJob.Location = new System.Drawing.Point(275, 215);
            this.fldJob.Name = "fldJob";
            this.fldJob.Size = new System.Drawing.Size(279, 30);
            this.fldJob.TabIndex = 28;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 44);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(227, 21);
            this.label10.TabIndex = 15;
            this.label10.Text = "Социальный работник";
            // 
            // fldConditionLiving
            // 
            this.fldConditionLiving.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionLiving.FormattingEnabled = true;
            this.fldConditionLiving.Location = new System.Drawing.Point(275, 180);
            this.fldConditionLiving.Name = "fldConditionLiving";
            this.fldConditionLiving.Size = new System.Drawing.Size(279, 29);
            this.fldConditionLiving.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 21);
            this.label9.TabIndex = 16;
            this.label9.Text = "Статус";
            // 
            // fldHealth
            // 
            this.fldHealth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldHealth.FormattingEnabled = true;
            this.fldHealth.Location = new System.Drawing.Point(275, 145);
            this.fldHealth.Name = "fldHealth";
            this.fldHealth.Size = new System.Drawing.Size(279, 29);
            this.fldHealth.TabIndex = 26;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 149);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(204, 21);
            this.label8.TabIndex = 17;
            this.label8.Text = "Состояние здоровья";
            // 
            // fldStatus
            // 
            this.fldStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldStatus.FormattingEnabled = true;
            this.fldStatus.Location = new System.Drawing.Point(275, 110);
            this.fldStatus.Name = "fldStatus";
            this.fldStatus.Size = new System.Drawing.Size(279, 29);
            this.fldStatus.TabIndex = 25;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 184);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(255, 21);
            this.label7.TabIndex = 18;
            this.label7.Text = "Оценка бытовых условий";
            // 
            // fldMedicalworker
            // 
            this.fldMedicalworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldMedicalworker.FormattingEnabled = true;
            this.fldMedicalworker.Location = new System.Drawing.Point(275, 75);
            this.fldMedicalworker.Name = "fldMedicalworker";
            this.fldMedicalworker.Size = new System.Drawing.Size(279, 29);
            this.fldMedicalworker.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 79);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(241, 21);
            this.label12.TabIndex = 19;
            this.label12.Text = "Медицинский работник";
            // 
            // fldSocialworker
            // 
            this.fldSocialworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldSocialworker.FormattingEnabled = true;
            this.fldSocialworker.Location = new System.Drawing.Point(275, 40);
            this.fldSocialworker.Name = "fldSocialworker";
            this.fldSocialworker.Size = new System.Drawing.Size(279, 29);
            this.fldSocialworker.TabIndex = 23;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataCategorys);
            this.groupBox1.Controls.Add(this.butRemoveCategory);
            this.groupBox1.Controls.Add(this.butAddCategory);
            this.groupBox1.Controls.Add(this.fldCategorys);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Location = new System.Drawing.Point(7, 261);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(547, 162);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Категории";
            // 
            // dataCategorys
            // 
            this.dataCategorys.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataCategorys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataCategorys.Location = new System.Drawing.Point(268, 26);
            this.dataCategorys.MultiSelect = false;
            this.dataCategorys.Name = "dataCategorys";
            this.dataCategorys.ReadOnly = true;
            this.dataCategorys.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataCategorys.Size = new System.Drawing.Size(273, 127);
            this.dataCategorys.TabIndex = 20;
            // 
            // butRemoveCategory
            // 
            this.butRemoveCategory.Location = new System.Drawing.Point(6, 122);
            this.butRemoveCategory.Name = "butRemoveCategory";
            this.butRemoveCategory.Size = new System.Drawing.Size(242, 31);
            this.butRemoveCategory.TabIndex = 19;
            this.butRemoveCategory.Text = "Удалить категорию";
            this.butRemoveCategory.UseVisualStyleBackColor = true;
            // 
            // butAddCategory
            // 
            this.butAddCategory.Location = new System.Drawing.Point(6, 85);
            this.butAddCategory.Name = "butAddCategory";
            this.butAddCategory.Size = new System.Drawing.Size(242, 31);
            this.butAddCategory.TabIndex = 18;
            this.butAddCategory.Text = "Добавить категорию";
            this.butAddCategory.UseVisualStyleBackColor = true;
            // 
            // fldCategorys
            // 
            this.fldCategorys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldCategorys.FormattingEnabled = true;
            this.fldCategorys.Location = new System.Drawing.Point(6, 50);
            this.fldCategorys.Name = "fldCategorys";
            this.fldCategorys.Size = new System.Drawing.Size(242, 29);
            this.fldCategorys.TabIndex = 17;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(213, 21);
            this.label13.TabIndex = 16;
            this.label13.Text = "Выберите категорию";
            // 
            // fldDepartament
            // 
            this.fldDepartament.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldDepartament.FormattingEnabled = true;
            this.fldDepartament.Location = new System.Drawing.Point(275, 4);
            this.fldDepartament.Name = "fldDepartament";
            this.fldDepartament.Size = new System.Drawing.Size(279, 29);
            this.fldDepartament.TabIndex = 22;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 220);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(144, 21);
            this.label14.TabIndex = 21;
            this.label14.Text = "Место работы";
            // 
            // toolStrip4
            // 
            this.toolStrip4.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip4.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip4.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveHead,
            this.toolStripSeparator4,
            this.lblHeadInfo});
            this.toolStrip4.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip4.Location = new System.Drawing.Point(3, 3);
            this.toolStrip4.Name = "toolStrip4";
            this.toolStrip4.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip4.Size = new System.Drawing.Size(559, 39);
            this.toolStrip4.TabIndex = 30;
            this.toolStrip4.Text = "toolStrip4";
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 39);
            // 
            // lblHeadInfo
            // 
            this.lblHeadInfo.Name = "lblHeadInfo";
            this.lblHeadInfo.Size = new System.Drawing.Size(0, 36);
            // 
            // tabAddress
            // 
            this.tabAddress.Controls.Add(this.panelAddress);
            this.tabAddress.Controls.Add(this.toolStrip3);
            this.tabAddress.Location = new System.Drawing.Point(204, 4);
            this.tabAddress.Name = "tabAddress";
            this.tabAddress.Padding = new System.Windows.Forms.Padding(3);
            this.tabAddress.Size = new System.Drawing.Size(565, 499);
            this.tabAddress.TabIndex = 2;
            this.tabAddress.Text = "Адрес клиента";
            this.tabAddress.UseVisualStyleBackColor = true;
            // 
            // panelAddress
            // 
            this.panelAddress.Controls.Add(this.groupBoxAddress);
            this.panelAddress.Controls.Add(this.checkRefugee);
            this.panelAddress.Controls.Add(this.checkHomeless);
            this.panelAddress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAddress.Location = new System.Drawing.Point(3, 42);
            this.panelAddress.Name = "panelAddress";
            this.panelAddress.Size = new System.Drawing.Size(559, 454);
            this.panelAddress.TabIndex = 21;
            // 
            // groupBoxAddress
            // 
            this.groupBoxAddress.Controls.Add(this.label15);
            this.groupBoxAddress.Controls.Add(this.label16);
            this.groupBoxAddress.Controls.Add(this.label17);
            this.groupBoxAddress.Controls.Add(this.label18);
            this.groupBoxAddress.Controls.Add(this.fldRegion);
            this.groupBoxAddress.Controls.Add(this.label19);
            this.groupBoxAddress.Controls.Add(this.fldCity);
            this.groupBoxAddress.Controls.Add(this.fldStreet);
            this.groupBoxAddress.Controls.Add(this.fldHouse);
            this.groupBoxAddress.Controls.Add(this.fldAppartament);
            this.groupBoxAddress.Location = new System.Drawing.Point(3, 34);
            this.groupBoxAddress.Name = "groupBoxAddress";
            this.groupBoxAddress.Size = new System.Drawing.Size(553, 264);
            this.groupBoxAddress.TabIndex = 20;
            this.groupBoxAddress.TabStop = false;
            this.groupBoxAddress.Text = "Житель Московской области";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(22, 36);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(146, 21);
            this.label15.TabIndex = 12;
            this.label15.Text = "Район / город";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 80);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(189, 21);
            this.label16.TabIndex = 1;
            this.label16.Text = "Населенный пункт";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(22, 128);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(69, 21);
            this.label17.TabIndex = 2;
            this.label17.Text = "Улица";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(22, 176);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(49, 21);
            this.label18.TabIndex = 3;
            this.label18.Text = "Дом";
            // 
            // fldRegion
            // 
            this.fldRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldRegion.FormattingEnabled = true;
            this.fldRegion.Location = new System.Drawing.Point(228, 32);
            this.fldRegion.Name = "fldRegion";
            this.fldRegion.Size = new System.Drawing.Size(319, 29);
            this.fldRegion.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(22, 224);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(103, 21);
            this.label19.TabIndex = 4;
            this.label19.Text = "Квартира";
            // 
            // fldCity
            // 
            this.fldCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldCity.FormattingEnabled = true;
            this.fldCity.Location = new System.Drawing.Point(228, 76);
            this.fldCity.Name = "fldCity";
            this.fldCity.Size = new System.Drawing.Size(319, 29);
            this.fldCity.TabIndex = 4;
            // 
            // fldStreet
            // 
            this.fldStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldStreet.FormattingEnabled = true;
            this.fldStreet.Location = new System.Drawing.Point(228, 124);
            this.fldStreet.Name = "fldStreet";
            this.fldStreet.Size = new System.Drawing.Size(319, 29);
            this.fldStreet.TabIndex = 5;
            // 
            // fldHouse
            // 
            this.fldHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldHouse.FormattingEnabled = true;
            this.fldHouse.Location = new System.Drawing.Point(228, 172);
            this.fldHouse.Name = "fldHouse";
            this.fldHouse.Size = new System.Drawing.Size(319, 29);
            this.fldHouse.TabIndex = 6;
            // 
            // fldAppartament
            // 
            this.fldAppartament.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldAppartament.FormattingEnabled = true;
            this.fldAppartament.Location = new System.Drawing.Point(228, 220);
            this.fldAppartament.Name = "fldAppartament";
            this.fldAppartament.Size = new System.Drawing.Size(319, 29);
            this.fldAppartament.TabIndex = 7;
            // 
            // checkRefugee
            // 
            this.checkRefugee.AutoSize = true;
            this.checkRefugee.Location = new System.Drawing.Point(3, 3);
            this.checkRefugee.Name = "checkRefugee";
            this.checkRefugee.Size = new System.Drawing.Size(115, 25);
            this.checkRefugee.TabIndex = 18;
            this.checkRefugee.Text = "Беженец";
            this.checkRefugee.UseVisualStyleBackColor = true;
            // 
            // checkHomeless
            // 
            this.checkHomeless.AutoSize = true;
            this.checkHomeless.Location = new System.Drawing.Point(124, 3);
            this.checkHomeless.Name = "checkHomeless";
            this.checkHomeless.Size = new System.Drawing.Size(388, 25);
            this.checkHomeless.TabIndex = 19;
            this.checkHomeless.Text = "Без определенного места жительства";
            this.checkHomeless.UseVisualStyleBackColor = true;
            // 
            // toolStrip3
            // 
            this.toolStrip3.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip3.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip3.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveAddress,
            this.toolStripSeparator5,
            this.lblAddressInfo});
            this.toolStrip3.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip3.Location = new System.Drawing.Point(3, 3);
            this.toolStrip3.Name = "toolStrip3";
            this.toolStrip3.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip3.Size = new System.Drawing.Size(559, 39);
            this.toolStrip3.TabIndex = 22;
            this.toolStrip3.Text = "toolStrip3";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 39);
            // 
            // lblAddressInfo
            // 
            this.lblAddressInfo.Name = "lblAddressInfo";
            this.lblAddressInfo.Size = new System.Drawing.Size(0, 36);
            // 
            // tabHousing
            // 
            this.tabHousing.Controls.Add(this.panelHousing);
            this.tabHousing.Controls.Add(this.toolStrip5);
            this.tabHousing.Location = new System.Drawing.Point(204, 4);
            this.tabHousing.Name = "tabHousing";
            this.tabHousing.Padding = new System.Windows.Forms.Padding(3);
            this.tabHousing.Size = new System.Drawing.Size(565, 499);
            this.tabHousing.TabIndex = 3;
            this.tabHousing.Text = "Жилье";
            this.tabHousing.UseVisualStyleBackColor = true;
            // 
            // panelHousing
            // 
            this.panelHousing.Controls.Add(this.label25);
            this.panelHousing.Controls.Add(this.groupBoxCondition);
            this.panelHousing.Controls.Add(this.label27);
            this.panelHousing.Controls.Add(this.groupBoxRepair);
            this.panelHousing.Controls.Add(this.fldTypeHousing);
            this.panelHousing.Controls.Add(this.fldConditionHousing);
            this.panelHousing.Controls.Add(this.label26);
            this.panelHousing.Controls.Add(this.label24);
            this.panelHousing.Controls.Add(this.fldArea);
            this.panelHousing.Controls.Add(this.fldCountRoom);
            this.panelHousing.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHousing.Location = new System.Drawing.Point(3, 42);
            this.panelHousing.Name = "panelHousing";
            this.panelHousing.Size = new System.Drawing.Size(559, 454);
            this.panelHousing.TabIndex = 24;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(82, 21);
            this.label25.TabIndex = 18;
            this.label25.Text = "Комнат";
            // 
            // groupBoxCondition
            // 
            this.groupBoxCondition.Controls.Add(this.fldConditionSanitary);
            this.groupBoxCondition.Controls.Add(this.label20);
            this.groupBoxCondition.Controls.Add(this.fldConditionHeating);
            this.groupBoxCondition.Controls.Add(this.label21);
            this.groupBoxCondition.Controls.Add(this.fldConditionWater);
            this.groupBoxCondition.Controls.Add(this.label22);
            this.groupBoxCondition.Controls.Add(this.fldConditionGas);
            this.groupBoxCondition.Controls.Add(this.label23);
            this.groupBoxCondition.Location = new System.Drawing.Point(7, 265);
            this.groupBoxCondition.Name = "groupBoxCondition";
            this.groupBoxCondition.Size = new System.Drawing.Size(543, 141);
            this.groupBoxCondition.TabIndex = 23;
            this.groupBoxCondition.TabStop = false;
            this.groupBoxCondition.Text = "Условия проживания";
            // 
            // fldConditionSanitary
            // 
            this.fldConditionSanitary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionSanitary.FormattingEnabled = true;
            this.fldConditionSanitary.Location = new System.Drawing.Point(270, 103);
            this.fldConditionSanitary.Name = "fldConditionSanitary";
            this.fldConditionSanitary.Size = new System.Drawing.Size(267, 29);
            this.fldConditionSanitary.TabIndex = 19;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(270, 79);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(210, 21);
            this.label20.TabIndex = 18;
            this.label20.Text = "Санитарные условия";
            // 
            // fldConditionHeating
            // 
            this.fldConditionHeating.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionHeating.FormattingEnabled = true;
            this.fldConditionHeating.Location = new System.Drawing.Point(6, 103);
            this.fldConditionHeating.Name = "fldConditionHeating";
            this.fldConditionHeating.Size = new System.Drawing.Size(258, 29);
            this.fldConditionHeating.TabIndex = 17;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 79);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(112, 21);
            this.label21.TabIndex = 16;
            this.label21.Text = "Отопление";
            // 
            // fldConditionWater
            // 
            this.fldConditionWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionWater.FormattingEnabled = true;
            this.fldConditionWater.Location = new System.Drawing.Point(270, 48);
            this.fldConditionWater.Name = "fldConditionWater";
            this.fldConditionWater.Size = new System.Drawing.Size(267, 29);
            this.fldConditionWater.TabIndex = 15;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(270, 24);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(58, 21);
            this.label22.TabIndex = 14;
            this.label22.Text = "Вода";
            // 
            // fldConditionGas
            // 
            this.fldConditionGas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionGas.FormattingEnabled = true;
            this.fldConditionGas.Location = new System.Drawing.Point(6, 48);
            this.fldConditionGas.Name = "fldConditionGas";
            this.fldConditionGas.Size = new System.Drawing.Size(258, 29);
            this.fldConditionGas.TabIndex = 13;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 24);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 21);
            this.label23.TabIndex = 12;
            this.label23.Text = "Газ";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 12);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(114, 21);
            this.label27.TabIndex = 14;
            this.label27.Text = "Вид жилья";
            // 
            // groupBoxRepair
            // 
            this.groupBoxRepair.Controls.Add(this.checkRepairCosmetic);
            this.groupBoxRepair.Controls.Add(this.checkRepairMajor);
            this.groupBoxRepair.Location = new System.Drawing.Point(7, 193);
            this.groupBoxRepair.Name = "groupBoxRepair";
            this.groupBoxRepair.Size = new System.Drawing.Size(543, 67);
            this.groupBoxRepair.TabIndex = 22;
            this.groupBoxRepair.TabStop = false;
            this.groupBoxRepair.Text = "Требуется ремонт";
            // 
            // checkRepairCosmetic
            // 
            this.checkRepairCosmetic.AutoSize = true;
            this.checkRepairCosmetic.Location = new System.Drawing.Point(11, 29);
            this.checkRepairCosmetic.Name = "checkRepairCosmetic";
            this.checkRepairCosmetic.Size = new System.Drawing.Size(177, 25);
            this.checkRepairCosmetic.TabIndex = 8;
            this.checkRepairCosmetic.Text = "Косметический";
            this.checkRepairCosmetic.UseVisualStyleBackColor = true;
            // 
            // checkRepairMajor
            // 
            this.checkRepairMajor.AutoSize = true;
            this.checkRepairMajor.Location = new System.Drawing.Point(194, 29);
            this.checkRepairMajor.Name = "checkRepairMajor";
            this.checkRepairMajor.Size = new System.Drawing.Size(158, 25);
            this.checkRepairMajor.TabIndex = 9;
            this.checkRepairMajor.Text = "Капитальный";
            this.checkRepairMajor.UseVisualStyleBackColor = true;
            // 
            // fldTypeHousing
            // 
            this.fldTypeHousing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldTypeHousing.FormattingEnabled = true;
            this.fldTypeHousing.Location = new System.Drawing.Point(135, 8);
            this.fldTypeHousing.Name = "fldTypeHousing";
            this.fldTypeHousing.Size = new System.Drawing.Size(265, 29);
            this.fldTypeHousing.TabIndex = 15;
            // 
            // fldConditionHousing
            // 
            this.fldConditionHousing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldConditionHousing.FormattingEnabled = true;
            this.fldConditionHousing.Location = new System.Drawing.Point(135, 142);
            this.fldConditionHousing.Name = "fldConditionHousing";
            this.fldConditionHousing.Size = new System.Drawing.Size(265, 29);
            this.fldConditionHousing.TabIndex = 21;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 50);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 21);
            this.label26.TabIndex = 16;
            this.label26.Text = "Площадь";
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(3, 123);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(130, 67);
            this.label24.TabIndex = 20;
            this.label24.Text = "Состояние жилого помещения";
            // 
            // fldArea
            // 
            this.fldArea.Location = new System.Drawing.Point(135, 45);
            this.fldArea.Name = "fldArea";
            this.fldArea.Size = new System.Drawing.Size(100, 30);
            this.fldArea.TabIndex = 17;
            // 
            // fldCountRoom
            // 
            this.fldCountRoom.Location = new System.Drawing.Point(135, 83);
            this.fldCountRoom.Name = "fldCountRoom";
            this.fldCountRoom.Size = new System.Drawing.Size(100, 30);
            this.fldCountRoom.TabIndex = 19;
            // 
            // toolStrip5
            // 
            this.toolStrip5.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip5.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip5.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveHousing,
            this.toolStripSeparator6,
            this.lblHousingInfo});
            this.toolStrip5.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip5.Location = new System.Drawing.Point(3, 3);
            this.toolStrip5.Name = "toolStrip5";
            this.toolStrip5.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip5.Size = new System.Drawing.Size(559, 39);
            this.toolStrip5.TabIndex = 13;
            this.toolStrip5.Text = "toolStrip5";
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 39);
            // 
            // lblHousingInfo
            // 
            this.lblHousingInfo.Name = "lblHousingInfo";
            this.lblHousingInfo.Size = new System.Drawing.Size(0, 36);
            // 
            // tabDocuments
            // 
            this.tabDocuments.Controls.Add(this.tabControl1);
            this.tabDocuments.Controls.Add(this.toolStrip6);
            this.tabDocuments.Location = new System.Drawing.Point(204, 4);
            this.tabDocuments.Name = "tabDocuments";
            this.tabDocuments.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocuments.Size = new System.Drawing.Size(565, 499);
            this.tabDocuments.TabIndex = 4;
            this.tabDocuments.Text = "Документы";
            this.tabDocuments.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabDocumentsBasic);
            this.tabControl1.Controls.Add(this.tabDocumentsSertificate);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 42);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(559, 454);
            this.tabControl1.TabIndex = 14;
            // 
            // tabDocumentsBasic
            // 
            this.tabDocumentsBasic.Controls.Add(this.panelDocumentsBasic);
            this.tabDocumentsBasic.Location = new System.Drawing.Point(4, 30);
            this.tabDocumentsBasic.Name = "tabDocumentsBasic";
            this.tabDocumentsBasic.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocumentsBasic.Size = new System.Drawing.Size(551, 420);
            this.tabDocumentsBasic.TabIndex = 0;
            this.tabDocumentsBasic.Text = "Основные документы";
            this.tabDocumentsBasic.UseVisualStyleBackColor = true;
            // 
            // panelDocumentsBasic
            // 
            this.panelDocumentsBasic.Controls.Add(this.groupDocumentsDisability);
            this.panelDocumentsBasic.Controls.Add(this.groupDocumentsPassport);
            this.panelDocumentsBasic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDocumentsBasic.Location = new System.Drawing.Point(3, 3);
            this.panelDocumentsBasic.Name = "panelDocumentsBasic";
            this.panelDocumentsBasic.Size = new System.Drawing.Size(545, 414);
            this.panelDocumentsBasic.TabIndex = 35;
            // 
            // groupDocumentsDisability
            // 
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilityDegree);
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilityGroup);
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilityAct);
            this.groupDocumentsDisability.Controls.Add(this.label47);
            this.groupDocumentsDisability.Controls.Add(this.label46);
            this.groupDocumentsDisability.Controls.Add(this.label45);
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilityDate);
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilityNumber);
            this.groupDocumentsDisability.Controls.Add(this.fldDisabilitySerial);
            this.groupDocumentsDisability.Controls.Add(this.label44);
            this.groupDocumentsDisability.Controls.Add(this.label43);
            this.groupDocumentsDisability.Location = new System.Drawing.Point(3, 138);
            this.groupDocumentsDisability.Name = "groupDocumentsDisability";
            this.groupDocumentsDisability.Size = new System.Drawing.Size(539, 129);
            this.groupDocumentsDisability.TabIndex = 34;
            this.groupDocumentsDisability.TabStop = false;
            this.groupDocumentsDisability.Text = "Инвалидность";
            // 
            // fldDisabilityDegree
            // 
            this.fldDisabilityDegree.Location = new System.Drawing.Point(57, 40);
            this.fldDisabilityDegree.Mask = "0";
            this.fldDisabilityDegree.Name = "fldDisabilityDegree";
            this.fldDisabilityDegree.Size = new System.Drawing.Size(48, 30);
            this.fldDisabilityDegree.TabIndex = 7;
            this.fldDisabilityDegree.Tag = "txt";
            this.fldDisabilityDegree.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldDisabilityGroup
            // 
            this.fldDisabilityGroup.Location = new System.Drawing.Point(6, 40);
            this.fldDisabilityGroup.Mask = "0";
            this.fldDisabilityGroup.Name = "fldDisabilityGroup";
            this.fldDisabilityGroup.Size = new System.Drawing.Size(48, 30);
            this.fldDisabilityGroup.TabIndex = 6;
            this.fldDisabilityGroup.Tag = "txt";
            this.fldDisabilityGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldDisabilityAct
            // 
            this.fldDisabilityAct.Location = new System.Drawing.Point(158, 89);
            this.fldDisabilityAct.Name = "fldDisabilityAct";
            this.fldDisabilityAct.Size = new System.Drawing.Size(375, 30);
            this.fldDisabilityAct.TabIndex = 11;
            this.fldDisabilityAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label47.Location = new System.Drawing.Point(158, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 15);
            this.label47.TabIndex = 41;
            this.label47.Text = "Акт";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label46.Location = new System.Drawing.Point(6, 71);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(117, 15);
            this.label46.TabIndex = 39;
            this.label46.Text = "Дата освидетельств.";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label45.Location = new System.Drawing.Point(127, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(172, 15);
            this.label45.TabIndex = 40;
            this.label45.Text = "Справка МСЭ: серия и номер";
            // 
            // fldDisabilityDate
            // 
            this.fldDisabilityDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldDisabilityDate.Location = new System.Drawing.Point(6, 89);
            this.fldDisabilityDate.Name = "fldDisabilityDate";
            this.fldDisabilityDate.Size = new System.Drawing.Size(146, 30);
            this.fldDisabilityDate.TabIndex = 10;
            // 
            // fldDisabilityNumber
            // 
            this.fldDisabilityNumber.Location = new System.Drawing.Point(252, 40);
            this.fldDisabilityNumber.Mask = "0000000";
            this.fldDisabilityNumber.Name = "fldDisabilityNumber";
            this.fldDisabilityNumber.Size = new System.Drawing.Size(121, 30);
            this.fldDisabilityNumber.TabIndex = 9;
            this.fldDisabilityNumber.Tag = "txt";
            // 
            // fldDisabilitySerial
            // 
            this.fldDisabilitySerial.Location = new System.Drawing.Point(127, 40);
            this.fldDisabilitySerial.Mask = "МСЭ-0000";
            this.fldDisabilitySerial.Name = "fldDisabilitySerial";
            this.fldDisabilitySerial.Size = new System.Drawing.Size(119, 30);
            this.fldDisabilitySerial.TabIndex = 8;
            this.fldDisabilitySerial.Tag = "txt";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label44.Location = new System.Drawing.Point(57, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 15);
            this.label44.TabIndex = 34;
            this.label44.Text = "Степень";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label43.Location = new System.Drawing.Point(6, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 15);
            this.label43.TabIndex = 32;
            this.label43.Text = "Группа";
            // 
            // groupDocumentsPassport
            // 
            this.groupDocumentsPassport.Controls.Add(this.label41);
            this.groupDocumentsPassport.Controls.Add(this.label40);
            this.groupDocumentsPassport.Controls.Add(this.label39);
            this.groupDocumentsPassport.Controls.Add(this.label38);
            this.groupDocumentsPassport.Controls.Add(this.label37);
            this.groupDocumentsPassport.Controls.Add(this.fldPassportCode);
            this.groupDocumentsPassport.Controls.Add(this.fldPassportIssued);
            this.groupDocumentsPassport.Controls.Add(this.fldPassportDate);
            this.groupDocumentsPassport.Controls.Add(this.fldPassportNumber);
            this.groupDocumentsPassport.Controls.Add(this.fldPassportSerial);
            this.groupDocumentsPassport.Location = new System.Drawing.Point(3, 3);
            this.groupDocumentsPassport.Name = "groupDocumentsPassport";
            this.groupDocumentsPassport.Size = new System.Drawing.Size(539, 129);
            this.groupDocumentsPassport.TabIndex = 33;
            this.groupDocumentsPassport.TabStop = false;
            this.groupDocumentsPassport.Text = "Паспортные данные";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label41.Location = new System.Drawing.Point(6, 70);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 15);
            this.label41.TabIndex = 37;
            this.label41.Text = "Кем выдан";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label40.Location = new System.Drawing.Point(315, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(74, 15);
            this.label40.TabIndex = 36;
            this.label40.Text = "Код подр-ия";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label39.Location = new System.Drawing.Point(164, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(76, 15);
            this.label39.TabIndex = 35;
            this.label39.Text = "Дата выдачи";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label38.Location = new System.Drawing.Point(75, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 15);
            this.label38.TabIndex = 34;
            this.label38.Text = "Номер";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label37.Location = new System.Drawing.Point(6, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 15);
            this.label37.TabIndex = 30;
            this.label37.Text = "Серия";
            // 
            // fldPassportCode
            // 
            this.fldPassportCode.Location = new System.Drawing.Point(315, 40);
            this.fldPassportCode.Mask = "000-000";
            this.fldPassportCode.Name = "fldPassportCode";
            this.fldPassportCode.Size = new System.Drawing.Size(89, 30);
            this.fldPassportCode.TabIndex = 4;
            this.fldPassportCode.Tag = "txt";
            // 
            // fldPassportIssued
            // 
            this.fldPassportIssued.Location = new System.Drawing.Point(6, 89);
            this.fldPassportIssued.Name = "fldPassportIssued";
            this.fldPassportIssued.Size = new System.Drawing.Size(527, 30);
            this.fldPassportIssued.TabIndex = 5;
            this.fldPassportIssued.Tag = "txt";
            // 
            // fldPassportDate
            // 
            this.fldPassportDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldPassportDate.Location = new System.Drawing.Point(164, 40);
            this.fldPassportDate.Name = "fldPassportDate";
            this.fldPassportDate.Size = new System.Drawing.Size(146, 30);
            this.fldPassportDate.TabIndex = 3;
            // 
            // fldPassportNumber
            // 
            this.fldPassportNumber.Location = new System.Drawing.Point(75, 40);
            this.fldPassportNumber.Mask = "000000";
            this.fldPassportNumber.Name = "fldPassportNumber";
            this.fldPassportNumber.Size = new System.Drawing.Size(84, 30);
            this.fldPassportNumber.TabIndex = 2;
            this.fldPassportNumber.Tag = "txt";
            // 
            // fldPassportSerial
            // 
            this.fldPassportSerial.Location = new System.Drawing.Point(6, 40);
            this.fldPassportSerial.Mask = "00 00";
            this.fldPassportSerial.Name = "fldPassportSerial";
            this.fldPassportSerial.Size = new System.Drawing.Size(63, 30);
            this.fldPassportSerial.TabIndex = 1;
            this.fldPassportSerial.Tag = "txt";
            // 
            // tabDocumentsSertificate
            // 
            this.tabDocumentsSertificate.Controls.Add(this.panelDocumentsSertificate);
            this.tabDocumentsSertificate.Location = new System.Drawing.Point(4, 30);
            this.tabDocumentsSertificate.Name = "tabDocumentsSertificate";
            this.tabDocumentsSertificate.Padding = new System.Windows.Forms.Padding(3);
            this.tabDocumentsSertificate.Size = new System.Drawing.Size(551, 420);
            this.tabDocumentsSertificate.TabIndex = 1;
            this.tabDocumentsSertificate.Text = "Удостоверения";
            this.tabDocumentsSertificate.UseVisualStyleBackColor = true;
            // 
            // panelDocumentsSertificate
            // 
            this.panelDocumentsSertificate.Controls.Add(this.groupBox9);
            this.panelDocumentsSertificate.Controls.Add(this.groupBox6);
            this.panelDocumentsSertificate.Controls.Add(this.groupBox3);
            this.panelDocumentsSertificate.Controls.Add(this.groupBox5);
            this.panelDocumentsSertificate.Controls.Add(this.groupBox4);
            this.panelDocumentsSertificate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelDocumentsSertificate.Location = new System.Drawing.Point(3, 3);
            this.panelDocumentsSertificate.Name = "panelDocumentsSertificate";
            this.panelDocumentsSertificate.Size = new System.Drawing.Size(545, 414);
            this.panelDocumentsSertificate.TabIndex = 57;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.fldCertificatePensionNumber);
            this.groupBox9.Controls.Add(this.fldCertificatePensionDate);
            this.groupBox9.Controls.Add(this.label50);
            this.groupBox9.Controls.Add(this.label49);
            this.groupBox9.Location = new System.Drawing.Point(3, 3);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(539, 74);
            this.groupBox9.TabIndex = 52;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Пенсионное";
            // 
            // fldCertificatePensionNumber
            // 
            this.fldCertificatePensionNumber.Location = new System.Drawing.Point(6, 38);
            this.fldCertificatePensionNumber.Name = "fldCertificatePensionNumber";
            this.fldCertificatePensionNumber.Size = new System.Drawing.Size(262, 30);
            this.fldCertificatePensionNumber.TabIndex = 12;
            this.fldCertificatePensionNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificatePensionDate
            // 
            this.fldCertificatePensionDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificatePensionDate.Location = new System.Drawing.Point(274, 38);
            this.fldCertificatePensionDate.Name = "fldCertificatePensionDate";
            this.fldCertificatePensionDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificatePensionDate.TabIndex = 13;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label50.Location = new System.Drawing.Point(274, 20);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(76, 15);
            this.label50.TabIndex = 47;
            this.label50.Text = "Дата выдачи";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label49.Location = new System.Drawing.Point(6, 20);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(45, 15);
            this.label49.TabIndex = 48;
            this.label49.Text = "Номер";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.fldCertificateMemberLaborNumber);
            this.groupBox6.Controls.Add(this.fldCertificateMemberLaborDate);
            this.groupBox6.Controls.Add(this.label34);
            this.groupBox6.Controls.Add(this.label35);
            this.groupBox6.Location = new System.Drawing.Point(3, 311);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(539, 74);
            this.groupBox6.TabIndex = 56;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Участник трудового фронта";
            // 
            // fldCertificateMemberLaborNumber
            // 
            this.fldCertificateMemberLaborNumber.Location = new System.Drawing.Point(6, 38);
            this.fldCertificateMemberLaborNumber.Name = "fldCertificateMemberLaborNumber";
            this.fldCertificateMemberLaborNumber.Size = new System.Drawing.Size(262, 30);
            this.fldCertificateMemberLaborNumber.TabIndex = 20;
            this.fldCertificateMemberLaborNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateMemberLaborDate
            // 
            this.fldCertificateMemberLaborDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateMemberLaborDate.Location = new System.Drawing.Point(274, 38);
            this.fldCertificateMemberLaborDate.Name = "fldCertificateMemberLaborDate";
            this.fldCertificateMemberLaborDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateMemberLaborDate.TabIndex = 21;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label34.Location = new System.Drawing.Point(274, 20);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(76, 15);
            this.label34.TabIndex = 47;
            this.label34.Text = "Дата выдачи";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label35.Location = new System.Drawing.Point(6, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(45, 15);
            this.label35.TabIndex = 48;
            this.label35.Text = "Номер";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.fldCertificateDisabledNumber);
            this.groupBox3.Controls.Add(this.fldCertificateDisabledDate);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Location = new System.Drawing.Point(3, 80);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(539, 74);
            this.groupBox3.TabIndex = 53;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Инвалид ВОВ";
            // 
            // fldCertificateDisabledNumber
            // 
            this.fldCertificateDisabledNumber.Location = new System.Drawing.Point(6, 38);
            this.fldCertificateDisabledNumber.Name = "fldCertificateDisabledNumber";
            this.fldCertificateDisabledNumber.Size = new System.Drawing.Size(262, 30);
            this.fldCertificateDisabledNumber.TabIndex = 14;
            this.fldCertificateDisabledNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateDisabledDate
            // 
            this.fldCertificateDisabledDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateDisabledDate.Location = new System.Drawing.Point(274, 38);
            this.fldCertificateDisabledDate.Name = "fldCertificateDisabledDate";
            this.fldCertificateDisabledDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateDisabledDate.TabIndex = 15;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label28.Location = new System.Drawing.Point(274, 20);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(76, 15);
            this.label28.TabIndex = 47;
            this.label28.Text = "Дата выдачи";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label29.Location = new System.Drawing.Point(6, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(45, 15);
            this.label29.TabIndex = 48;
            this.label29.Text = "Номер";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.fldCertificateVeteranNumber);
            this.groupBox5.Controls.Add(this.fldCertificateVeteranDate);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Location = new System.Drawing.Point(3, 234);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(539, 74);
            this.groupBox5.TabIndex = 55;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ветеран труда";
            // 
            // fldCertificateVeteranNumber
            // 
            this.fldCertificateVeteranNumber.Location = new System.Drawing.Point(6, 38);
            this.fldCertificateVeteranNumber.Name = "fldCertificateVeteranNumber";
            this.fldCertificateVeteranNumber.Size = new System.Drawing.Size(262, 30);
            this.fldCertificateVeteranNumber.TabIndex = 18;
            this.fldCertificateVeteranNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateVeteranDate
            // 
            this.fldCertificateVeteranDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateVeteranDate.Location = new System.Drawing.Point(274, 38);
            this.fldCertificateVeteranDate.Name = "fldCertificateVeteranDate";
            this.fldCertificateVeteranDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateVeteranDate.TabIndex = 19;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label32.Location = new System.Drawing.Point(274, 20);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(76, 15);
            this.label32.TabIndex = 47;
            this.label32.Text = "Дата выдачи";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label33.Location = new System.Drawing.Point(6, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 15);
            this.label33.TabIndex = 48;
            this.label33.Text = "Номер";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.fldCertificateMemberNumber);
            this.groupBox4.Controls.Add(this.fldCertificateMemberDate);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Location = new System.Drawing.Point(3, 157);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(539, 74);
            this.groupBox4.TabIndex = 54;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Участник ВОВ";
            // 
            // fldCertificateMemberNumber
            // 
            this.fldCertificateMemberNumber.Location = new System.Drawing.Point(6, 38);
            this.fldCertificateMemberNumber.Name = "fldCertificateMemberNumber";
            this.fldCertificateMemberNumber.Size = new System.Drawing.Size(262, 30);
            this.fldCertificateMemberNumber.TabIndex = 16;
            this.fldCertificateMemberNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateMemberDate
            // 
            this.fldCertificateMemberDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateMemberDate.Location = new System.Drawing.Point(274, 38);
            this.fldCertificateMemberDate.Name = "fldCertificateMemberDate";
            this.fldCertificateMemberDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateMemberDate.TabIndex = 17;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label30.Location = new System.Drawing.Point(274, 20);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(76, 15);
            this.label30.TabIndex = 47;
            this.label30.Text = "Дата выдачи";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label31.Location = new System.Drawing.Point(6, 20);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(45, 15);
            this.label31.TabIndex = 48;
            this.label31.Text = "Номер";
            // 
            // toolStrip6
            // 
            this.toolStrip6.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip6.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip6.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveDocuments,
            this.toolStripSeparator7,
            this.lblDocumentsInfo});
            this.toolStrip6.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this.toolStrip6.Location = new System.Drawing.Point(3, 3);
            this.toolStrip6.Name = "toolStrip6";
            this.toolStrip6.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip6.Size = new System.Drawing.Size(559, 39);
            this.toolStrip6.TabIndex = 13;
            this.toolStrip6.Text = "toolStrip6";
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 39);
            // 
            // lblDocumentsInfo
            // 
            this.lblDocumentsInfo.Name = "lblDocumentsInfo";
            this.lblDocumentsInfo.Size = new System.Drawing.Size(0, 36);
            // 
            // tabReports
            // 
            this.tabReports.Controls.Add(this.dataReport);
            this.tabReports.Controls.Add(this.statusStrip1);
            this.tabReports.Controls.Add(this.panel1);
            this.tabReports.Controls.Add(this.toolStrip7);
            this.tabReports.Location = new System.Drawing.Point(204, 4);
            this.tabReports.Name = "tabReports";
            this.tabReports.Padding = new System.Windows.Forms.Padding(3);
            this.tabReports.Size = new System.Drawing.Size(565, 499);
            this.tabReports.TabIndex = 5;
            this.tabReports.Text = "Отчетность";
            this.tabReports.UseVisualStyleBackColor = true;
            // 
            // dataReport
            // 
            this.dataReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataReport.Location = new System.Drawing.Point(3, 146);
            this.dataReport.MultiSelect = false;
            this.dataReport.Name = "dataReport";
            this.dataReport.Size = new System.Drawing.Size(559, 328);
            this.dataReport.TabIndex = 3;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblReportServicePay,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.lblReportTotal,
            this.toolStripStatusLabel4,
            this.toolStripStatusLabel5,
            this.lblReportAllPay});
            this.statusStrip1.Location = new System.Drawing.Point(3, 474);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(559, 22);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.Stretch = false;
            this.statusStrip1.TabIndex = 2;
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(103, 17);
            this.toolStripStatusLabel1.Text = "Условия оплаты: ";
            // 
            // lblReportServicePay
            // 
            this.lblReportServicePay.Name = "lblReportServicePay";
            this.lblReportServicePay.Size = new System.Drawing.Size(70, 17);
            this.lblReportServicePay.Text = "Нет данных";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(46, 17);
            this.toolStripStatusLabel3.Text = "Итого: ";
            // 
            // lblReportTotal
            // 
            this.lblReportTotal.Name = "lblReportTotal";
            this.lblReportTotal.Size = new System.Drawing.Size(13, 17);
            this.lblReportTotal.Text = "0";
            // 
            // toolStripStatusLabel4
            // 
            this.toolStripStatusLabel4.Name = "toolStripStatusLabel4";
            this.toolStripStatusLabel4.Size = new System.Drawing.Size(10, 17);
            this.toolStripStatusLabel4.Text = "|";
            // 
            // toolStripStatusLabel5
            // 
            this.toolStripStatusLabel5.Name = "toolStripStatusLabel5";
            this.toolStripStatusLabel5.Size = new System.Drawing.Size(94, 17);
            this.toolStripStatusLabel5.Text = "Всего к оплате: ";
            // 
            // lblReportAllPay
            // 
            this.lblReportAllPay.Name = "lblReportAllPay";
            this.lblReportAllPay.Size = new System.Drawing.Size(13, 17);
            this.lblReportAllPay.Text = "0";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel1.Controls.Add(this.butReportInsert);
            this.panel1.Controls.Add(this.fldReportServices);
            this.panel1.Controls.Add(this.radioReportAdditional);
            this.panel1.Controls.Add(this.radioReportGuaranteed);
            this.panel1.Controls.Add(this.butReportView);
            this.panel1.Controls.Add(this.fldReportDate);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(559, 104);
            this.panel1.TabIndex = 1;
            // 
            // radioReportAdditional
            // 
            this.radioReportAdditional.AutoSize = true;
            this.radioReportAdditional.Location = new System.Drawing.Point(185, 32);
            this.radioReportAdditional.Name = "radioReportAdditional";
            this.radioReportAdditional.Size = new System.Drawing.Size(189, 25);
            this.radioReportAdditional.TabIndex = 4;
            this.radioReportAdditional.Text = "Дополнительный";
            this.radioReportAdditional.UseVisualStyleBackColor = true;
            // 
            // radioReportGuaranteed
            // 
            this.radioReportGuaranteed.AutoSize = true;
            this.radioReportGuaranteed.Checked = true;
            this.radioReportGuaranteed.Location = new System.Drawing.Point(185, 4);
            this.radioReportGuaranteed.Name = "radioReportGuaranteed";
            this.radioReportGuaranteed.Size = new System.Drawing.Size(203, 25);
            this.radioReportGuaranteed.TabIndex = 3;
            this.radioReportGuaranteed.TabStop = true;
            this.radioReportGuaranteed.Text = "Гарантированный";
            this.radioReportGuaranteed.UseVisualStyleBackColor = true;
            // 
            // fldReportDate
            // 
            this.fldReportDate.CustomFormat = "MMMM yyyy";
            this.fldReportDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.fldReportDate.Location = new System.Drawing.Point(3, 15);
            this.fldReportDate.Name = "fldReportDate";
            this.fldReportDate.ShowUpDown = true;
            this.fldReportDate.Size = new System.Drawing.Size(171, 30);
            this.fldReportDate.TabIndex = 2;
            // 
            // toolStrip7
            // 
            this.toolStrip7.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip7.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip7.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSaveReport,
            this.toolStripSeparator8,
            this.butReportPrint,
            this.toolStripSeparator9,
            this.butReportPrintAct});
            this.toolStrip7.Location = new System.Drawing.Point(3, 3);
            this.toolStrip7.Name = "toolStrip7";
            this.toolStrip7.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip7.Size = new System.Drawing.Size(559, 39);
            this.toolStrip7.TabIndex = 0;
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStripSeparator9
            // 
            this.toolStripSeparator9.Name = "toolStripSeparator9";
            this.toolStripSeparator9.Size = new System.Drawing.Size(6, 39);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton1,
            this.toolStripSeparator1,
            this.lblClientStatus,
            this.toolStripSeparator2,
            this.butPrintClientCard});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolStrip1.Size = new System.Drawing.Size(773, 25);
            this.toolStrip1.TabIndex = 1;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // lblClientStatus
            // 
            this.lblClientStatus.Name = "lblClientStatus";
            this.lblClientStatus.Size = new System.Drawing.Size(204, 22);
            this.lblClientStatus.Text = "Клиент находится на обслуживание";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // tabIncome
            // 
            this.tabIncome.Location = new System.Drawing.Point(204, 4);
            this.tabIncome.Name = "tabIncome";
            this.tabIncome.Padding = new System.Windows.Forms.Padding(3);
            this.tabIncome.Size = new System.Drawing.Size(565, 499);
            this.tabIncome.TabIndex = 6;
            this.tabIncome.Text = "Доходы";
            this.tabIncome.UseVisualStyleBackColor = true;
            // 
            // fldReportServices
            // 
            this.fldReportServices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fldReportServices.FormattingEnabled = true;
            this.fldReportServices.Location = new System.Drawing.Point(3, 69);
            this.fldReportServices.Name = "fldReportServices";
            this.fldReportServices.Size = new System.Drawing.Size(405, 29);
            this.fldReportServices.TabIndex = 5;
            // 
            // butSaveInfo
            // 
            this.butSaveInfo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveInfo.Image = ((System.Drawing.Image)(resources.GetObject("butSaveInfo.Image")));
            this.butSaveInfo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveInfo.Name = "butSaveInfo";
            this.butSaveInfo.Size = new System.Drawing.Size(36, 36);
            this.butSaveInfo.Text = "Сохранить";
            // 
            // butSaveHead
            // 
            this.butSaveHead.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveHead.Image = ((System.Drawing.Image)(resources.GetObject("butSaveHead.Image")));
            this.butSaveHead.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveHead.Name = "butSaveHead";
            this.butSaveHead.Size = new System.Drawing.Size(36, 36);
            this.butSaveHead.Text = "Сохранить";
            // 
            // butSaveAddress
            // 
            this.butSaveAddress.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveAddress.Image = ((System.Drawing.Image)(resources.GetObject("butSaveAddress.Image")));
            this.butSaveAddress.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveAddress.Name = "butSaveAddress";
            this.butSaveAddress.Size = new System.Drawing.Size(36, 36);
            this.butSaveAddress.Text = "Сохранить";
            // 
            // butSaveHousing
            // 
            this.butSaveHousing.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveHousing.Image = ((System.Drawing.Image)(resources.GetObject("butSaveHousing.Image")));
            this.butSaveHousing.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveHousing.Name = "butSaveHousing";
            this.butSaveHousing.Size = new System.Drawing.Size(36, 36);
            this.butSaveHousing.Text = "Сохранить";
            // 
            // butSaveDocuments
            // 
            this.butSaveDocuments.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveDocuments.Image = ((System.Drawing.Image)(resources.GetObject("butSaveDocuments.Image")));
            this.butSaveDocuments.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveDocuments.Name = "butSaveDocuments";
            this.butSaveDocuments.Size = new System.Drawing.Size(36, 36);
            this.butSaveDocuments.Text = "Сохранить";
            // 
            // butReportInsert
            // 
            this.butReportInsert.Image = global::isSocialWorker.Properties.Resources.link_add;
            this.butReportInsert.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butReportInsert.Location = new System.Drawing.Point(414, 69);
            this.butReportInsert.Name = "butReportInsert";
            this.butReportInsert.Size = new System.Drawing.Size(140, 29);
            this.butReportInsert.TabIndex = 6;
            this.butReportInsert.Text = "Добавить";
            this.butReportInsert.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butReportInsert.UseVisualStyleBackColor = true;
            // 
            // butReportView
            // 
            this.butReportView.Image = global::isSocialWorker.Properties.Resources.filter;
            this.butReportView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.butReportView.Location = new System.Drawing.Point(414, 4);
            this.butReportView.Name = "butReportView";
            this.butReportView.Size = new System.Drawing.Size(140, 50);
            this.butReportView.TabIndex = 2;
            this.butReportView.Text = "Показать";
            this.butReportView.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.butReportView.UseVisualStyleBackColor = true;
            // 
            // butSaveReport
            // 
            this.butSaveReport.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butSaveReport.Image = global::isSocialWorker.Properties.Resources.save_as;
            this.butSaveReport.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butSaveReport.Name = "butSaveReport";
            this.butSaveReport.Size = new System.Drawing.Size(36, 36);
            this.butSaveReport.Text = "Сохранить изменения";
            // 
            // butReportPrint
            // 
            this.butReportPrint.Image = global::isSocialWorker.Properties.Resources.printer1;
            this.butReportPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butReportPrint.Name = "butReportPrint";
            this.butReportPrint.Size = new System.Drawing.Size(126, 36);
            this.butReportPrint.Text = "Печатать отчет";
            // 
            // butReportPrintAct
            // 
            this.butReportPrintAct.Image = global::isSocialWorker.Properties.Resources.printer1;
            this.butReportPrintAct.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butReportPrintAct.Name = "butReportPrintAct";
            this.butReportPrintAct.Size = new System.Drawing.Size(108, 36);
            this.butReportPrintAct.Text = "Печать акта";
            // 
            // toolStripDropDownButton1
            // 
            this.toolStripDropDownButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обслуживаниеToolStripMenuItem,
            this.выбытиеToolStripMenuItem,
            this.butDead});
            this.toolStripDropDownButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton1.Image")));
            this.toolStripDropDownButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton1.Name = "toolStripDropDownButton1";
            this.toolStripDropDownButton1.Size = new System.Drawing.Size(71, 22);
            this.toolStripDropDownButton1.Text = "Действия";
            // 
            // обслуживаниеToolStripMenuItem
            // 
            this.обслуживаниеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butSuspendStop,
            this.butSuspendStart});
            this.обслуживаниеToolStripMenuItem.Image = global::isSocialWorker.Properties.Resources.service_status1;
            this.обслуживаниеToolStripMenuItem.Name = "обслуживаниеToolStripMenuItem";
            this.обслуживаниеToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.обслуживаниеToolStripMenuItem.Text = "Обслуживание";
            // 
            // butSuspendStop
            // 
            this.butSuspendStop.Image = global::isSocialWorker.Properties.Resources.stop;
            this.butSuspendStop.Name = "butSuspendStop";
            this.butSuspendStop.Size = new System.Drawing.Size(229, 22);
            this.butSuspendStop.Text = "Снять с обслуживания";
            // 
            // butSuspendStart
            // 
            this.butSuspendStart.Image = global::isSocialWorker.Properties.Resources.accept_button;
            this.butSuspendStart.Name = "butSuspendStart";
            this.butSuspendStart.Size = new System.Drawing.Size(229, 22);
            this.butSuspendStart.Text = "Возобновить обслуживание";
            // 
            // выбытиеToolStripMenuItem
            // 
            this.выбытиеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.butDisposalStop,
            this.butDisposalStart});
            this.выбытиеToolStripMenuItem.Image = global::isSocialWorker.Properties.Resources.user_delete;
            this.выбытиеToolStripMenuItem.Name = "выбытиеToolStripMenuItem";
            this.выбытиеToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.выбытиеToolStripMenuItem.Text = "Выбытие";
            // 
            // butDisposalStop
            // 
            this.butDisposalStop.Image = global::isSocialWorker.Properties.Resources.stop;
            this.butDisposalStop.Name = "butDisposalStop";
            this.butDisposalStop.Size = new System.Drawing.Size(229, 22);
            this.butDisposalStop.Text = "Клиент выбыл";
            // 
            // butDisposalStart
            // 
            this.butDisposalStart.Image = global::isSocialWorker.Properties.Resources.accept_button;
            this.butDisposalStart.Name = "butDisposalStart";
            this.butDisposalStart.Size = new System.Drawing.Size(229, 22);
            this.butDisposalStart.Text = "Возобновить обслуживание";
            // 
            // butDead
            // 
            this.butDead.Image = global::isSocialWorker.Properties.Resources.cross_shield;
            this.butDead.Name = "butDead";
            this.butDead.Size = new System.Drawing.Size(157, 22);
            this.butDead.Text = "Клиент умер";
            // 
            // butPrintClientCard
            // 
            this.butPrintClientCard.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.butPrintClientCard.Image = global::isSocialWorker.Properties.Resources.printer;
            this.butPrintClientCard.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.butPrintClientCard.Name = "butPrintClientCard";
            this.butPrintClientCard.Size = new System.Drawing.Size(23, 22);
            this.butPrintClientCard.Text = "Печатать карточку клиента";
            // 
            // formClientAnketa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 532);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.toolStrip1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formClientAnketa";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Анкета клиента";
            this.tabControl.ResumeLayout(false);
            this.tabInfo.ResumeLayout(false);
            this.tabInfo.PerformLayout();
            this.panelInfo.ResumeLayout(false);
            this.panelInfo.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.tabHead.ResumeLayout(false);
            this.tabHead.PerformLayout();
            this.panelHead.ResumeLayout(false);
            this.panelHead.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataCategorys)).EndInit();
            this.toolStrip4.ResumeLayout(false);
            this.toolStrip4.PerformLayout();
            this.tabAddress.ResumeLayout(false);
            this.tabAddress.PerformLayout();
            this.panelAddress.ResumeLayout(false);
            this.panelAddress.PerformLayout();
            this.groupBoxAddress.ResumeLayout(false);
            this.groupBoxAddress.PerformLayout();
            this.toolStrip3.ResumeLayout(false);
            this.toolStrip3.PerformLayout();
            this.tabHousing.ResumeLayout(false);
            this.tabHousing.PerformLayout();
            this.panelHousing.ResumeLayout(false);
            this.panelHousing.PerformLayout();
            this.groupBoxCondition.ResumeLayout(false);
            this.groupBoxCondition.PerformLayout();
            this.groupBoxRepair.ResumeLayout(false);
            this.groupBoxRepair.PerformLayout();
            this.toolStrip5.ResumeLayout(false);
            this.toolStrip5.PerformLayout();
            this.tabDocuments.ResumeLayout(false);
            this.tabDocuments.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabDocumentsBasic.ResumeLayout(false);
            this.panelDocumentsBasic.ResumeLayout(false);
            this.groupDocumentsDisability.ResumeLayout(false);
            this.groupDocumentsDisability.PerformLayout();
            this.groupDocumentsPassport.ResumeLayout(false);
            this.groupDocumentsPassport.PerformLayout();
            this.tabDocumentsSertificate.ResumeLayout(false);
            this.panelDocumentsSertificate.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.toolStrip6.ResumeLayout(false);
            this.toolStrip6.PerformLayout();
            this.tabReports.ResumeLayout(false);
            this.tabReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataReport)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip7.ResumeLayout(false);
            this.toolStrip7.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabInfo;
        private System.Windows.Forms.TextBox fldPhone;
        private System.Windows.Forms.DateTimePicker fldDateborn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox fldMiddlename;
        private System.Windows.Forms.TextBox fldFirstname;
        private System.Windows.Forms.TextBox fldLastname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabHead;
        private System.Windows.Forms.MaskedTextBox fldSnils;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel lblClientStatus;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.ToolStripButton butSaveInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton butPrintClientCard;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataCategorys;
        private System.Windows.Forms.Button butRemoveCategory;
        private System.Windows.Forms.Button butAddCategory;
        private System.Windows.Forms.ComboBox fldCategorys;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox fldJob;
        private System.Windows.Forms.ComboBox fldConditionLiving;
        private System.Windows.Forms.ComboBox fldHealth;
        private System.Windows.Forms.ComboBox fldStatus;
        private System.Windows.Forms.ComboBox fldMedicalworker;
        private System.Windows.Forms.ComboBox fldSocialworker;
        private System.Windows.Forms.ComboBox fldDepartament;
        private System.Windows.Forms.TabPage tabAddress;
        private System.Windows.Forms.GroupBox groupBoxAddress;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox fldRegion;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox fldCity;
        private System.Windows.Forms.ComboBox fldStreet;
        private System.Windows.Forms.ComboBox fldHouse;
        private System.Windows.Forms.ComboBox fldAppartament;
        private System.Windows.Forms.CheckBox checkHomeless;
        private System.Windows.Forms.CheckBox checkRefugee;
        private System.Windows.Forms.TabPage tabHousing;
        private System.Windows.Forms.ToolStrip toolStrip5;
        private System.Windows.Forms.ToolStripButton butSaveHousing;
        private System.Windows.Forms.GroupBox groupBoxCondition;
        private System.Windows.Forms.ComboBox fldConditionSanitary;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox fldConditionHeating;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox fldConditionWater;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox fldConditionGas;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox groupBoxRepair;
        private System.Windows.Forms.CheckBox checkRepairCosmetic;
        private System.Windows.Forms.CheckBox checkRepairMajor;
        private System.Windows.Forms.ComboBox fldConditionHousing;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox fldCountRoom;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox fldArea;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox fldTypeHousing;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TabPage tabDocuments;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabDocumentsBasic;
        private System.Windows.Forms.TabPage tabDocumentsSertificate;
        private System.Windows.Forms.GroupBox groupDocumentsDisability;
        private System.Windows.Forms.MaskedTextBox fldDisabilityDegree;
        private System.Windows.Forms.MaskedTextBox fldDisabilityGroup;
        private System.Windows.Forms.TextBox fldDisabilityAct;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DateTimePicker fldDisabilityDate;
        private System.Windows.Forms.MaskedTextBox fldDisabilityNumber;
        private System.Windows.Forms.MaskedTextBox fldDisabilitySerial;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupDocumentsPassport;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.MaskedTextBox fldPassportCode;
        private System.Windows.Forms.TextBox fldPassportIssued;
        private System.Windows.Forms.DateTimePicker fldPassportDate;
        private System.Windows.Forms.MaskedTextBox fldPassportNumber;
        private System.Windows.Forms.MaskedTextBox fldPassportSerial;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox fldCertificatePensionNumber;
        private System.Windows.Forms.DateTimePicker fldCertificatePensionDate;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox fldCertificateDisabledNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateDisabledDate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox fldCertificateMemberNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateMemberDate;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox fldCertificateVeteranNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateVeteranDate;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox fldCertificateMemberLaborNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateMemberLaborDate;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panelDocumentsSertificate;
        private System.Windows.Forms.Panel panelDocumentsBasic;
        private System.Windows.Forms.Panel panelHousing;
        private System.Windows.Forms.Panel panelInfo;
        private System.Windows.Forms.Panel panelHead;
        private System.Windows.Forms.Panel panelAddress;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton1;
        private System.Windows.Forms.ToolStripMenuItem обслуживаниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem butSuspendStop;
        private System.Windows.Forms.ToolStripMenuItem butSuspendStart;
        private System.Windows.Forms.ToolStripMenuItem выбытиеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem butDisposalStop;
        private System.Windows.Forms.ToolStripMenuItem butDisposalStart;
        private System.Windows.Forms.ToolStripMenuItem butDead;
        private System.Windows.Forms.ToolStrip toolStrip6;
        private System.Windows.Forms.ToolStripButton butSaveDocuments;
        private System.Windows.Forms.ToolStrip toolStrip4;
        private System.Windows.Forms.ToolStripButton butSaveHead;
        private System.Windows.Forms.ToolStrip toolStrip3;
        private System.Windows.Forms.ToolStripButton butSaveAddress;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel lblClientInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel lblHeadInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripLabel lblAddressInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripLabel lblHousingInfo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripLabel lblDocumentsInfo;
        private System.Windows.Forms.TabPage tabReports;
        private System.Windows.Forms.ToolStrip toolStrip7;
        private System.Windows.Forms.ToolStripButton butReportPrint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button butReportView;
        private System.Windows.Forms.DateTimePicker fldReportDate;
        private System.Windows.Forms.ToolStripButton butReportPrintAct;
        private System.Windows.Forms.DataGridView dataReport;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblReportServicePay;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel lblReportTotal;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel4;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel5;
        private System.Windows.Forms.ToolStripStatusLabel lblReportAllPay;
        private System.Windows.Forms.RadioButton radioReportAdditional;
        private System.Windows.Forms.RadioButton radioReportGuaranteed;
        private System.Windows.Forms.ToolStripButton butSaveReport;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator9;
        private System.Windows.Forms.TabPage tabIncome;
        private System.Windows.Forms.Button butReportInsert;
        private System.Windows.Forms.ComboBox fldReportServices;
    }
}