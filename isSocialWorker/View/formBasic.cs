﻿using isSocialWorker.Client;
using isSocialWorker.ClientList;
using isSocialWorker.Model;
using isSocialWorker.Presenter;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View
{
    public partial class formBasic : Form
    {
        public formBasic()
        {
            InitializeComponent();
            this.FormClosing += formBasic_FormClosing;
            this.KeyPreview = true;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            if (e.KeyCode == Keys.F1)
            {
                NewClientClick();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.F2)
            {
                ClientListClick();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.F9)
            {
                //NewClientClick();
                MessageBox.Show("Справочники");
                e.Handled = true;
            }
            if (e.KeyCode == Keys.F10)
            {
                SettingConnectClick();
                e.Handled = true;
            }
            if (e.KeyCode == Keys.F11)
            {
                SettingSystemClick();
                e.Handled = true;
            }
            
        }

        void formBasic_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private void toolNewClient_Click(object sender, EventArgs e)
        {
            NewClientClick();
        }

        private void menuSettingsSystem_Click(object sender, EventArgs e)
        {
            SettingSystemClick();
        }

        private void menuSettingsConnect_Click(object sender, EventArgs e)
        {
            SettingConnectClick();
        }

        private void ClientListClick()
        {
            this.Cursor = Cursors.WaitCursor;
            formClientList form = new formClientList();
            ClientListModel model = new ClientListModel();
            MessageService message = new MessageService();

            ClientListPresenter presenter = new ClientListPresenter(form, model, message);
            form.MdiParent = this;
            form.Show();
            this.Cursor = Cursors.Default;
        }

        private void NewClientClick()
        {
            formNewClient form = new formNewClient();
            NewClientModel model = new NewClientModel();
            MessageService message = new MessageService();

            NewClientPresenter presenter = new NewClientPresenter(form, model, message);
            form.MdiParent = this;
            form.Show();
        }

        private void SettingConnectClick()
        {
            formSettingConnect form = new formSettingConnect();
            MessageService service = new MessageService();
            SettingConnectModel model = new SettingConnectModel();
            ConnectPresenter presenter = new ConnectPresenter(form, model, service);

            form.MdiParent = this;
            form.Show();
        }

        private void SettingSystemClick()
        {
            formSettingsSystem form = new formSettingsSystem();
            SettingSystemModel model = new SettingSystemModel();
            MessageService message = new MessageService();

            SettingSystemPresenter presenter = new SettingSystemPresenter(form, model, message);

            form.MdiParent = this;
            form.Show();
        }

        private void menuFileExit_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void menuFileNewClient_Click(object sender, EventArgs e)
        {
            NewClientClick();
        }

        private void toolClientsList_Click(object sender, EventArgs e)
        {
            ClientListClick();
        }

        private void menuFileClientList_Click(object sender, EventArgs e)
        {
            ClientListClick();
        }
    }
}
