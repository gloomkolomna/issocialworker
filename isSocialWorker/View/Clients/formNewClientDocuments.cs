﻿using isSocialWorker.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View.Clients
{
    public interface IFormNewClientDocuments
    {
        event EventHandler BackButtonClick;
        event EventHandler SaveButtonClick;
        event EventHandler LoadForm;

        formNewClientDocuments Parentform { get; }

        string PassportSerial { get; set; }
        string PassportNumber { get; set; }
        string PassportCode { get; set; }
        string PassportDate { get; set; }
        string PassportIssued { get; set; }

        string DisabilityGroup { get; set; }
        string DisabilityDegree { get; set; }
        string DisabilitySerial { get; set; }
        string DisabilityNumber { get; set; }
        string DisabilityDate { get; set; }
        string DisabilityAct { get; set; }

        string CertificatePensionNumber { get; set; }
        string CertificatePensionDate { get; set; }
        string CertificateDisabledNumber { get; set; }
        string CertificateDisabledDate { get; set; }
        string CertificateMemberNumber { get; set; }
        string CertificateMemberDate { get; set; }
        string CertificateVeteranNumber { get; set; }
        string CertificateVeteranDate { get; set; }
        string CertificateMemberLaborNumber { get; set; }
        string CertificateMemberLaborDate { get; set; }
    }

    public partial class formNewClientDocuments : Form, IFormNewClientDocuments
    {
        public formNewClientDocuments()
        {
            InitializeComponent();
            butBack.Click += butBack_Click;
            butSaveClient.Click += butSaveClient_Click;
            this.FormClosed += formNewClientDocuments_FormClosed;
            this.Load += formNewClientDocuments_Load;
        }

        void formNewClientDocuments_Load(object sender, EventArgs e)
        {
            if (LoadForm != null) LoadForm(this, EventArgs.Empty);
        }

        void formNewClientDocuments_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientInfo.ClearAllFields();
            ClientInfoAddress.ClearAllFields();
            ClientInfoHead.ClearAllFields();
            ClientInfoHousing.ClearAllFields();
            ClientDocuments.ClearAllFields();
        }

        void butSaveClient_Click(object sender, EventArgs e)
        {
            if (SaveButtonClick != null) SaveButtonClick(this, EventArgs.Empty);
        }

        void butBack_Click(object sender, EventArgs e)
        {
            if (BackButtonClick != null) BackButtonClick(this, EventArgs.Empty);
        }


        #region IFormNewClientDocuments
        public event EventHandler BackButtonClick;

        public event EventHandler SaveButtonClick;

        public event EventHandler LoadForm;

        public formNewClientDocuments Parentform { get { return this; } }

        public string PassportSerial
        {
            get
            {
                return fldPassportSerial.Text;
            }
            set
            {
                fldPassportSerial.Text = value;
            }
        }

        public string PassportNumber
        {
            get
            {
                return fldPassportNumber.Text;
            }
            set
            {
                fldPassportNumber.Text = value;
            }
        }

        public string PassportCode
        {
            get
            {
                return fldPassportCode.Text;
            }
            set
            {
                fldPassportCode.Text = value;
            }
        }

        public string PassportDate
        {
            get
            {
                return fldPassportDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldPassportDate.Text = value;
            }
        }

        public string PassportIssued
        {
            get
            {
                return fldPassportIssued.Text;
            }
            set
            {
                fldPassportIssued.Text = value;
            }
        }

        public string DisabilityGroup
        {
            get
            {
                return fldDisabilityGroup.Text;
            }
            set
            {
                fldDisabilityGroup.Text = value;
            }
        }

        public string DisabilityDegree
        {
            get
            {
                return fldDisabilityDegree.Text;
            }
            set
            {
                fldDisabilityDegree.Text = value;
            }
        }

        public string DisabilitySerial
        {
            get
            {
                return fldDisabilitySerial.Text;
            }
            set
            {
                fldDisabilitySerial.Text = value;
            }
        }

        public string DisabilityNumber
        {
            get
            {
                return fldDisabilityNumber.Text;
            }
            set
            {
                fldDisabilityNumber.Text = value;
            }
        }

        public string DisabilityDate
        {
            get
            {
                return fldDisabilityDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldDisabilityDate.Text = value;
            }
        }

        public string DisabilityAct
        {
            get
            {
                return fldDisabilityAct.Text;
            }
            set
            {
                fldDisabilityAct.Text = value;
            }
        }

        public string CertificatePensionNumber
        {
            get
            {
                return fldCertificatePensionNumber.Text;
            }
            set
            {
                fldCertificatePensionNumber.Text = value;
            }
        }

        public string CertificatePensionDate
        {
            get
            {
                return fldCertificatePensionDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificatePensionDate.Text = value;
            }
        }

        public string CertificateDisabledNumber
        {
            get
            {
                return fldCertificateDisabledNumber.Text;
            }
            set
            {
                fldCertificateDisabledNumber.Text = value;
            }
        }

        public string CertificateDisabledDate
        {
            get
            {
                return fldCertificateDisabledDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateDisabledDate.Text = value;
            }
        }

        public string CertificateMemberNumber
        {
            get
            {
                return fldCertificateMemberNumber.Text;
            }
            set
            {
                fldCertificateMemberNumber.Text = value;
            }
        }

        public string CertificateMemberDate
        {
            get
            {
                return fldCertificateMemberDate.Value.ToString("yyyy-MM-dd");
            }
            set
            {
                fldCertificateMemberDate.Text = value;
            }
        }

        public string CertificateVeteranNumber
        {
            get
            {
                return fldCertificateVeteranNumber.Text;
            }
            set
            {
                fldCertificateVeteranNumber.Text = value;
            }
        }

        public string CertificateVeteranDate
        {
            get
            {
                return fldCertificateVeteranDate.Text;
            }
            set
            {
                fldCertificateVeteranDate.Text = value;
            }
        }

        public string CertificateMemberLaborNumber
        {
            get
            {
                return fldCertificateMemberLaborNumber.Text;
            }
            set
            {
                fldCertificateMemberLaborNumber.Text = value;
            }
        }

        public string CertificateMemberLaborDate
        {
            get
            {
                return fldCertificateMemberLaborDate.Text;
            }
            set
            {
                fldCertificateMemberLaborDate.Text = value;
            }
        }

        #endregion
    }
}
