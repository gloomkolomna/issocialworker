﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using isSocialWorker.Model;
using isSocialWorker.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View.Clients
{
    public interface IFormNewClientAddress
    {
        event EventHandler BackButtonClick;
        event EventHandler NextButtonClick;
        event EventHandler LoadForm;

        event EventHandler RegionSelect;
        event EventHandler CitySelect;
        event EventHandler StreetSelect;
        event EventHandler HouseSelect;
        event EventHandler AppartamentSelect;

        int RegionId { get; set; }
        int CityId { get; set; }
        int StreetId { get; set; }
        int HouseId { get; set; }
        int AppartamentId { get; set; }
        bool IsRefugee { get; set; }
        bool IsHomeless { get; set; }

        void RegionFilling();
        void CityFilling();
        void StreetFilling();
        void HouseFilling();
        void AppartamentFilling();

        List<DirectoryDefault> AddressList { set; }

        formNewClientAddress Parentform { get; }
    }

    public partial class formNewClientAddress : Form, IFormNewClientAddress
    {
        public formNewClientAddress()
        {
            InitializeComponent();
            this.Load += formNewClientAddress_Load;
            this.FormClosed += formNewClientAddress_FormClosed;
            comboRegion.DropDownClosed += comboRegion_DropDownClosed;
            comboCity.DropDownClosed += comboCity_DropDownClosed;
            comboStreet.DropDownClosed += comboStreet_DropDownClosed;
            comboHouse.DropDownClosed += comboHouse_DropDownClosed;
            comboAppartament.DropDownClosed += comboAppartament_DropDownClosed;

            butBack.Click += butBack_Click;
            butNext.Click += butNext_Click;

            checkRefugee.Click += checkRefugee_Click;
            checkHomeless.Click += checkHomeless_Click;
        }

        void checkHomeless_Click(object sender, EventArgs e)
        {
            if (checkHomeless.Checked)
            {
                groupBoxAddress.Enabled = false;
            }
            else
            {
                groupBoxAddress.Enabled = true;
            }
        }

        void checkRefugee_Click(object sender, EventArgs e)
        {
            if (checkRefugee.Checked)
            {
                groupBoxAddress.Enabled = false;
            }
            else
            {
                groupBoxAddress.Enabled = true;
            }
        }

        void formNewClientAddress_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientInfo.ClearAllFields();
            ClientInfoAddress.ClearAllFields();
            ClientInfoHead.ClearAllFields();
            ClientInfoHousing.ClearAllFields();
            ClientDocuments.ClearAllFields();
        }

        void butNext_Click(object sender, EventArgs e)
        {
            if (NextButtonClick != null) NextButtonClick(this, EventArgs.Empty);
        }

        void butBack_Click(object sender, EventArgs e)
        {
            if (BackButtonClick != null) BackButtonClick(this, EventArgs.Empty);
        }

        void comboAppartament_DropDownClosed(object sender, EventArgs e)
        {
            if (AppartamentSelect != null) AppartamentSelect(this, EventArgs.Empty);
        }

        void comboHouse_DropDownClosed(object sender, EventArgs e)
        {
            if (HouseSelect != null) HouseSelect(this, EventArgs.Empty);
        }

        void comboStreet_DropDownClosed(object sender, EventArgs e)
        {
            if (StreetSelect != null) StreetSelect(this, EventArgs.Empty);
        }

        void comboCity_DropDownClosed(object sender, EventArgs e)
        {
            if (CitySelect != null) CitySelect(this, EventArgs.Empty);
        }

        void comboRegion_DropDownClosed(object sender, EventArgs e)
        {
            if (RegionSelect != null) RegionSelect(this, EventArgs.Empty);
        }

        void formNewClientAddress_Load(object sender, EventArgs e)
        {
            if (LoadForm != null) LoadForm(this, EventArgs.Empty);
        }

        public event EventHandler BackButtonClick;

        public event EventHandler NextButtonClick;

        public event EventHandler LoadForm;

        public event EventHandler RegionSelect;

        public event EventHandler CitySelect;

        public event EventHandler StreetSelect;

        public event EventHandler HouseSelect;

        public event EventHandler AppartamentSelect;

        public int RegionId
        {
            get
            {
                return (int)comboRegion.SelectedValue;
            }
            set
            {
                comboRegion.SelectedValue = value;
            }
        }

        public int CityId
        {
            get
            {
                try
                {
                    return (int)comboCity.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                comboCity.SelectedValue = value;
            }
        }

        public int StreetId
        {
            get
            {
                try
                {
                    return (int)comboStreet.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                int? select = value;
                comboStreet.SelectedValue = select.HasValue ? value : 0;
            }
        }

        public int HouseId
        {
            get
            {
                try
                {
                    return (int)comboHouse.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                comboHouse.SelectedValue = value;
            }
        }

        public int AppartamentId
        {
            get
            {
                try
                {
                    return (int)comboAppartament.SelectedValue;
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                int? select = value;
                comboAppartament.SelectedValue = select.HasValue ? value : 0;
            }
        }

        public bool IsRefugee
        {
            get
            {
                if (checkRefugee.Checked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                bool isRefugee = value;
                if (isRefugee)
                {
                    checkRefugee.Checked = true;
                }
                else
                {
                    checkRefugee.Checked = false;
                }
            }
        }

        public bool IsHomeless
        {
            get
            {
                if (checkHomeless.Checked)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                bool isHomeless = value;
                if (isHomeless)
                {
                    checkHomeless.Checked = true;
                }
                else
                {
                    checkHomeless.Checked = false;
                }
            }
        }

        public void RegionFilling()
        {
            FillingComboBox(comboRegion);
        }

        public void CityFilling()
        {
            FillingComboBox(comboCity);
        }

        public void StreetFilling()
        {
            FillingComboBox(comboStreet);
        }

        public void HouseFilling()
        {
            FillingComboBox(comboHouse);
        }

        public void AppartamentFilling()
        {
            FillingComboBox(comboAppartament);
        }

        private void FillingComboBox(ComboBox comboBox)
        {
            var sourse = (from a in addressList
                          orderby a.Name ascending
                          select new { a.Id, Names = a.Name }).ToList();
            comboBox.DataSource = sourse;
            comboBox.DisplayMember = "Names";
            comboBox.ValueMember = "Id";
        }

        private List<DirectoryDefault> addressList = new List<DirectoryDefault>();

        public List<DirectoryDefault> AddressList
        {
            set { addressList = value; }
        }

        public formNewClientAddress Parentform
        {
            get { return this; }
        }
    }
}
