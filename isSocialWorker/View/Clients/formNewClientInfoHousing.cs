﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using isSocialWorker.Model;
using isSocialWorker.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View.Clients
{
    public interface IFormNewClientInfoHousing
    {
        event EventHandler BackButtonClick;
        event EventHandler NextButtonClick;
        event EventHandler LoadForm;

        int TypeHousingId { get; set; }
        int ConditionHousingId { get; set; }
        int ConditionGasId { get; set; }
        int ConditionWaterId { get; set; }
        int ConditionHeatingId { get; set; }
        int ConditionSanitaryId { get; set; }
        double HousingArea { get; set; }
        int HousingRoom { get; set; }
        bool RepairCosmetic { get; set; }
        bool RepairMajor { get; set; }

        void TypeHousingFilling();
        void ConditionHousingFilling();
        void ConditionGasFilling();
        void ConditionWaterFilling();
        void ConditionHeatingFilling();
        void ConditionSanitaryFilling();

        List<DirectoryDefault> FillingList { set; }

        formNewClientInfoHousing Parentform { get; }
    }

    public partial class formNewClientInfoHousing : Form, IFormNewClientInfoHousing
    {
        public formNewClientInfoHousing()
        {
            InitializeComponent();
            this.FormClosed += formNewClientInfoHousing_FormClosed;
            this.Load += formNewClientInfoHousing_Load;
            butNext.Click += butNext_Click;
            butBack.Click += butBack_Click;

            fldArea.KeyPress += fldArea_KeyPress;
            fldCountRoom.KeyPress += fldCountRoom_KeyPress;
        }

        void fldCountRoom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
            }
        }

        void fldArea_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(Char.IsDigit(e.KeyChar)))
            {
                if (e.KeyChar == '.' || e.KeyChar == ',' || e.KeyChar == (char)Keys.Back)
                {
                    if (e.KeyChar == (char)Keys.Back)
                    {

                    }
                    else
                        if (fldArea.Text.Contains(",") )
                        {
                            e.Handled = true;
                        }
                }
                else
                {
                    e.Handled = true;
                }
            }
        }

        void butBack_Click(object sender, EventArgs e)
        {
            if (BackButtonClick != null) BackButtonClick(this, EventArgs.Empty);
        }

        void butNext_Click(object sender, EventArgs e)
        {
            if (NextButtonClick != null) NextButtonClick(this, EventArgs.Empty);
        }

        void formNewClientInfoHousing_Load(object sender, EventArgs e)
        {
            if (LoadForm != null) LoadForm(this, EventArgs.Empty);
        }

        void formNewClientInfoHousing_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientInfo.ClearAllFields();
            ClientInfoAddress.ClearAllFields();
            ClientInfoHead.ClearAllFields();
            ClientInfoHousing.ClearAllFields();
            ClientDocuments.ClearAllFields();
        }

        public event EventHandler BackButtonClick;

        public event EventHandler NextButtonClick;

        public event EventHandler LoadForm;

        public int TypeHousingId
        {
            get
            {
                try
                {
                    return (int)comboTypeHousing.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboTypeHousing.SelectedValue = value;
            }
        }

        public int ConditionHousingId
        {
            get
            {
                try
                {
                    return (int)comboConditionHousing.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionHousing.SelectedValue = value;
            }
        }

        public int ConditionGasId
        {
            get
            {
                try
                {
                    return (int)comboConditionGas.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionGas.SelectedValue = value;
            }
        }

        public int ConditionWaterId
        {
            get
            {
                try
                {
                    return (int)comboConditionWater.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionWater.SelectedValue = value;
            }
        }

        public int ConditionHeatingId
        {
            get
            {
                try
                {
                    return (int)comboConditionHeating.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionHeating.SelectedValue = value;
            }
        }

        public int ConditionSanitaryId
        {
            get
            {
                try
                {
                    return (int)comboConditionSanitary.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionSanitary.SelectedValue = value;
            }
        }

        public double HousingArea
        {
            get
            {
                return double.Parse(fldArea.Text);
            }
            set
            {
                fldArea.Text = value.ToString();
            }
        }

        public int HousingRoom
        {
            get
            {
                return Int32.Parse(fldCountRoom.Text);
            }
            set
            {
                fldCountRoom.Text = value.ToString();
            }
        }

        public bool RepairCosmetic
        {
            get
            {
                if (checkRepairCosmetic.Checked)
                    return true;
                else
                    return false;
            }
            set
            {
                bool isRepairCosmetic = value;
                if (isRepairCosmetic)
                    checkRepairCosmetic.Checked = true;
                else
                    checkRepairCosmetic.Checked = false;
            }
        }

        public bool RepairMajor
        {
            get
            {
                if (checkRepairMajor.Checked)
                    return true;
                else
                    return false;
            }
            set
            {
                bool isRepairMajor = value;
                if (isRepairMajor)
                    checkRepairMajor.Checked = true;
                else
                    checkRepairMajor.Checked = false;
            }
        }

        public void TypeHousingFilling()
        {
            FillingComboBox(comboTypeHousing);
        }

        public void ConditionHousingFilling()
        {
            FillingComboBox(comboConditionHousing);
        }

        public void ConditionGasFilling()
        {
            FillingComboBox(comboConditionGas);
        }

        public void ConditionWaterFilling()
        {
            FillingComboBox(comboConditionWater);
        }

        public void ConditionHeatingFilling()
        {
            FillingComboBox(comboConditionHeating);
        }

        public void ConditionSanitaryFilling()
        {
            FillingComboBox(comboConditionSanitary);
        }

        public List<DirectoryDefault> FillingList
        {
            set { list = value; }
        }

        private List<DirectoryDefault> list = new List<DirectoryDefault>();

        private void FillingComboBox(ComboBox comboBox)
        {
            var sourse = (from a in list
                          select new { a.Id, Names = a.Name }).ToList();
            comboBox.DataSource = sourse;
            comboBox.DisplayMember = "Names";
            comboBox.ValueMember = "Id";
        }

        public formNewClientInfoHousing Parentform { get { return this; } }
        
    }
}
