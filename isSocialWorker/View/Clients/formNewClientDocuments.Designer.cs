﻿namespace isSocialWorker.View.Clients
{
    partial class formNewClientDocuments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grb_invalid = new System.Windows.Forms.GroupBox();
            this.fldDisabilityDegree = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilityGroup = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilityAct = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.fldDisabilityDate = new System.Windows.Forms.DateTimePicker();
            this.fldDisabilityNumber = new System.Windows.Forms.MaskedTextBox();
            this.fldDisabilitySerial = new System.Windows.Forms.MaskedTextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.fldPassportCode = new System.Windows.Forms.MaskedTextBox();
            this.fldPassportIssued = new System.Windows.Forms.TextBox();
            this.fldPassportDate = new System.Windows.Forms.DateTimePicker();
            this.fldPassportNumber = new System.Windows.Forms.MaskedTextBox();
            this.fldPassportSerial = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.fldCertificateMemberLaborNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateMemberLaborDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.fldCertificateVeteranNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateVeteranDate = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.fldCertificateMemberNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateMemberDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.fldCertificateDisabledNumber = new System.Windows.Forms.TextBox();
            this.fldCertificateDisabledDate = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.fldCertificatePensionNumber = new System.Windows.Forms.TextBox();
            this.fldCertificatePensionDate = new System.Windows.Forms.DateTimePicker();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.butBack = new System.Windows.Forms.Button();
            this.butSaveClient = new System.Windows.Forms.Button();
            this.grb_invalid.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grb_invalid
            // 
            this.grb_invalid.Controls.Add(this.fldDisabilityDegree);
            this.grb_invalid.Controls.Add(this.fldDisabilityGroup);
            this.grb_invalid.Controls.Add(this.fldDisabilityAct);
            this.grb_invalid.Controls.Add(this.label47);
            this.grb_invalid.Controls.Add(this.label46);
            this.grb_invalid.Controls.Add(this.label45);
            this.grb_invalid.Controls.Add(this.fldDisabilityDate);
            this.grb_invalid.Controls.Add(this.fldDisabilityNumber);
            this.grb_invalid.Controls.Add(this.fldDisabilitySerial);
            this.grb_invalid.Controls.Add(this.label44);
            this.grb_invalid.Controls.Add(this.label43);
            this.grb_invalid.Location = new System.Drawing.Point(386, 12);
            this.grb_invalid.Name = "grb_invalid";
            this.grb_invalid.Size = new System.Drawing.Size(368, 129);
            this.grb_invalid.TabIndex = 32;
            this.grb_invalid.TabStop = false;
            this.grb_invalid.Text = "Инвалидность";
            // 
            // fldDisabilityDegree
            // 
            this.fldDisabilityDegree.Location = new System.Drawing.Point(57, 40);
            this.fldDisabilityDegree.Mask = "0";
            this.fldDisabilityDegree.Name = "fldDisabilityDegree";
            this.fldDisabilityDegree.Size = new System.Drawing.Size(48, 30);
            this.fldDisabilityDegree.TabIndex = 7;
            this.fldDisabilityDegree.Tag = "txt";
            this.fldDisabilityDegree.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldDisabilityGroup
            // 
            this.fldDisabilityGroup.Location = new System.Drawing.Point(6, 40);
            this.fldDisabilityGroup.Mask = "0";
            this.fldDisabilityGroup.Name = "fldDisabilityGroup";
            this.fldDisabilityGroup.Size = new System.Drawing.Size(48, 30);
            this.fldDisabilityGroup.TabIndex = 6;
            this.fldDisabilityGroup.Tag = "txt";
            this.fldDisabilityGroup.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldDisabilityAct
            // 
            this.fldDisabilityAct.Location = new System.Drawing.Point(158, 89);
            this.fldDisabilityAct.Name = "fldDisabilityAct";
            this.fldDisabilityAct.Size = new System.Drawing.Size(201, 30);
            this.fldDisabilityAct.TabIndex = 11;
            this.fldDisabilityAct.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label47.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label47.Location = new System.Drawing.Point(158, 71);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(28, 15);
            this.label47.TabIndex = 41;
            this.label47.Text = "Акт";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label46.Location = new System.Drawing.Point(6, 71);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(117, 15);
            this.label46.TabIndex = 39;
            this.label46.Text = "Дата освидетельств.";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label45.Location = new System.Drawing.Point(127, 22);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(172, 15);
            this.label45.TabIndex = 40;
            this.label45.Text = "Справка МСЭ: серия и номер";
            // 
            // fldDisabilityDate
            // 
            this.fldDisabilityDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldDisabilityDate.Location = new System.Drawing.Point(6, 89);
            this.fldDisabilityDate.Name = "fldDisabilityDate";
            this.fldDisabilityDate.Size = new System.Drawing.Size(146, 30);
            this.fldDisabilityDate.TabIndex = 10;
            // 
            // fldDisabilityNumber
            // 
            this.fldDisabilityNumber.Location = new System.Drawing.Point(238, 40);
            this.fldDisabilityNumber.Mask = "0000000";
            this.fldDisabilityNumber.Name = "fldDisabilityNumber";
            this.fldDisabilityNumber.Size = new System.Drawing.Size(121, 30);
            this.fldDisabilityNumber.TabIndex = 9;
            this.fldDisabilityNumber.Tag = "txt";
            // 
            // fldDisabilitySerial
            // 
            this.fldDisabilitySerial.Location = new System.Drawing.Point(127, 40);
            this.fldDisabilitySerial.Mask = "МСЭ-0000";
            this.fldDisabilitySerial.Name = "fldDisabilitySerial";
            this.fldDisabilitySerial.Size = new System.Drawing.Size(105, 30);
            this.fldDisabilitySerial.TabIndex = 8;
            this.fldDisabilitySerial.Tag = "txt";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label44.Location = new System.Drawing.Point(57, 22);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 15);
            this.label44.TabIndex = 34;
            this.label44.Text = "Степень";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label43.Location = new System.Drawing.Point(6, 22);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(48, 15);
            this.label43.TabIndex = 32;
            this.label43.Text = "Группа";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label41);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.label39);
            this.groupBox9.Controls.Add(this.label38);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.fldPassportCode);
            this.groupBox9.Controls.Add(this.fldPassportIssued);
            this.groupBox9.Controls.Add(this.fldPassportDate);
            this.groupBox9.Controls.Add(this.fldPassportNumber);
            this.groupBox9.Controls.Add(this.fldPassportSerial);
            this.groupBox9.Location = new System.Drawing.Point(12, 12);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(368, 129);
            this.groupBox9.TabIndex = 31;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Паспортные данные";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label41.Location = new System.Drawing.Point(6, 70);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(67, 15);
            this.label41.TabIndex = 37;
            this.label41.Text = "Кем выдан";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label40.Location = new System.Drawing.Point(285, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(74, 15);
            this.label40.TabIndex = 36;
            this.label40.Text = "Код подр-ия";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label39.Location = new System.Drawing.Point(134, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(76, 15);
            this.label39.TabIndex = 35;
            this.label39.Text = "Дата выдачи";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label38.Location = new System.Drawing.Point(66, 22);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(45, 15);
            this.label38.TabIndex = 34;
            this.label38.Text = "Номер";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label37.Location = new System.Drawing.Point(6, 22);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(41, 15);
            this.label37.TabIndex = 30;
            this.label37.Text = "Серия";
            // 
            // fldPassportCode
            // 
            this.fldPassportCode.Location = new System.Drawing.Point(285, 40);
            this.fldPassportCode.Mask = "000-000";
            this.fldPassportCode.Name = "fldPassportCode";
            this.fldPassportCode.Size = new System.Drawing.Size(77, 30);
            this.fldPassportCode.TabIndex = 4;
            this.fldPassportCode.Tag = "txt";
            // 
            // fldPassportIssued
            // 
            this.fldPassportIssued.Location = new System.Drawing.Point(6, 89);
            this.fldPassportIssued.Name = "fldPassportIssued";
            this.fldPassportIssued.Size = new System.Drawing.Size(356, 30);
            this.fldPassportIssued.TabIndex = 5;
            this.fldPassportIssued.Tag = "txt";
            // 
            // fldPassportDate
            // 
            this.fldPassportDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldPassportDate.Location = new System.Drawing.Point(134, 40);
            this.fldPassportDate.Name = "fldPassportDate";
            this.fldPassportDate.Size = new System.Drawing.Size(146, 30);
            this.fldPassportDate.TabIndex = 3;
            // 
            // fldPassportNumber
            // 
            this.fldPassportNumber.Location = new System.Drawing.Point(66, 40);
            this.fldPassportNumber.Mask = "000000";
            this.fldPassportNumber.Name = "fldPassportNumber";
            this.fldPassportNumber.Size = new System.Drawing.Size(62, 30);
            this.fldPassportNumber.TabIndex = 2;
            this.fldPassportNumber.Tag = "txt";
            // 
            // fldPassportSerial
            // 
            this.fldPassportSerial.Location = new System.Drawing.Point(6, 40);
            this.fldPassportSerial.Mask = "00 00";
            this.fldPassportSerial.Name = "fldPassportSerial";
            this.fldPassportSerial.Size = new System.Drawing.Size(54, 30);
            this.fldPassportSerial.TabIndex = 1;
            this.fldPassportSerial.Tag = "txt";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Location = new System.Drawing.Point(12, 147);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(742, 296);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Удостоверения";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.fldCertificateMemberLaborNumber);
            this.groupBox6.Controls.Add(this.fldCertificateMemberLaborDate);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Location = new System.Drawing.Point(374, 118);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(359, 83);
            this.groupBox6.TabIndex = 53;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Участник трудового фронта";
            // 
            // fldCertificateMemberLaborNumber
            // 
            this.fldCertificateMemberLaborNumber.Location = new System.Drawing.Point(6, 45);
            this.fldCertificateMemberLaborNumber.Name = "fldCertificateMemberLaborNumber";
            this.fldCertificateMemberLaborNumber.Size = new System.Drawing.Size(192, 30);
            this.fldCertificateMemberLaborNumber.TabIndex = 20;
            this.fldCertificateMemberLaborNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateMemberLaborDate
            // 
            this.fldCertificateMemberLaborDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateMemberLaborDate.Location = new System.Drawing.Point(204, 45);
            this.fldCertificateMemberLaborDate.Name = "fldCertificateMemberLaborDate";
            this.fldCertificateMemberLaborDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateMemberLaborDate.TabIndex = 21;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label7.Location = new System.Drawing.Point(204, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 47;
            this.label7.Text = "Дата выдачи";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label8.Location = new System.Drawing.Point(6, 27);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 15);
            this.label8.TabIndex = 48;
            this.label8.Text = "Номер";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.fldCertificateVeteranNumber);
            this.groupBox5.Controls.Add(this.fldCertificateVeteranDate);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(374, 29);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(359, 83);
            this.groupBox5.TabIndex = 52;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Ветеран труда";
            // 
            // fldCertificateVeteranNumber
            // 
            this.fldCertificateVeteranNumber.Location = new System.Drawing.Point(6, 45);
            this.fldCertificateVeteranNumber.Name = "fldCertificateVeteranNumber";
            this.fldCertificateVeteranNumber.Size = new System.Drawing.Size(192, 30);
            this.fldCertificateVeteranNumber.TabIndex = 18;
            this.fldCertificateVeteranNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateVeteranDate
            // 
            this.fldCertificateVeteranDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateVeteranDate.Location = new System.Drawing.Point(204, 45);
            this.fldCertificateVeteranDate.Name = "fldCertificateVeteranDate";
            this.fldCertificateVeteranDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateVeteranDate.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(204, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 15);
            this.label5.TabIndex = 47;
            this.label5.Text = "Дата выдачи";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label6.Location = new System.Drawing.Point(6, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(45, 15);
            this.label6.TabIndex = 48;
            this.label6.Text = "Номер";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.fldCertificateMemberNumber);
            this.groupBox4.Controls.Add(this.fldCertificateMemberDate);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Location = new System.Drawing.Point(9, 207);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(359, 83);
            this.groupBox4.TabIndex = 53;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Участник ВОВ";
            // 
            // fldCertificateMemberNumber
            // 
            this.fldCertificateMemberNumber.Location = new System.Drawing.Point(6, 45);
            this.fldCertificateMemberNumber.Name = "fldCertificateMemberNumber";
            this.fldCertificateMemberNumber.Size = new System.Drawing.Size(192, 30);
            this.fldCertificateMemberNumber.TabIndex = 16;
            this.fldCertificateMemberNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateMemberDate
            // 
            this.fldCertificateMemberDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateMemberDate.Location = new System.Drawing.Point(204, 45);
            this.fldCertificateMemberDate.Name = "fldCertificateMemberDate";
            this.fldCertificateMemberDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateMemberDate.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(204, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 15);
            this.label3.TabIndex = 47;
            this.label3.Text = "Дата выдачи";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(6, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 15);
            this.label4.TabIndex = 48;
            this.label4.Text = "Номер";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.fldCertificateDisabledNumber);
            this.groupBox3.Controls.Add(this.fldCertificateDisabledDate);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Location = new System.Drawing.Point(9, 118);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(359, 83);
            this.groupBox3.TabIndex = 52;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Инвалид ВОВ";
            // 
            // fldCertificateDisabledNumber
            // 
            this.fldCertificateDisabledNumber.Location = new System.Drawing.Point(6, 45);
            this.fldCertificateDisabledNumber.Name = "fldCertificateDisabledNumber";
            this.fldCertificateDisabledNumber.Size = new System.Drawing.Size(192, 30);
            this.fldCertificateDisabledNumber.TabIndex = 14;
            this.fldCertificateDisabledNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificateDisabledDate
            // 
            this.fldCertificateDisabledDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificateDisabledDate.Location = new System.Drawing.Point(204, 45);
            this.fldCertificateDisabledDate.Name = "fldCertificateDisabledDate";
            this.fldCertificateDisabledDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificateDisabledDate.TabIndex = 15;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(204, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 15);
            this.label1.TabIndex = 47;
            this.label1.Text = "Дата выдачи";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Location = new System.Drawing.Point(6, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 15);
            this.label2.TabIndex = 48;
            this.label2.Text = "Номер";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.fldCertificatePensionNumber);
            this.groupBox2.Controls.Add(this.fldCertificatePensionDate);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.label49);
            this.groupBox2.Location = new System.Drawing.Point(9, 29);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(359, 83);
            this.groupBox2.TabIndex = 51;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Пенсионное";
            // 
            // fldCertificatePensionNumber
            // 
            this.fldCertificatePensionNumber.Location = new System.Drawing.Point(6, 45);
            this.fldCertificatePensionNumber.Name = "fldCertificatePensionNumber";
            this.fldCertificatePensionNumber.Size = new System.Drawing.Size(192, 30);
            this.fldCertificatePensionNumber.TabIndex = 12;
            this.fldCertificatePensionNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // fldCertificatePensionDate
            // 
            this.fldCertificatePensionDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldCertificatePensionDate.Location = new System.Drawing.Point(204, 45);
            this.fldCertificatePensionDate.Name = "fldCertificatePensionDate";
            this.fldCertificatePensionDate.Size = new System.Drawing.Size(146, 30);
            this.fldCertificatePensionDate.TabIndex = 13;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label50.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label50.Location = new System.Drawing.Point(204, 27);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(76, 15);
            this.label50.TabIndex = 47;
            this.label50.Text = "Дата выдачи";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label49.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label49.Location = new System.Drawing.Point(6, 27);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(45, 15);
            this.label49.TabIndex = 48;
            this.label49.Text = "Номер";
            // 
            // butBack
            // 
            this.butBack.Location = new System.Drawing.Point(12, 449);
            this.butBack.Name = "butBack";
            this.butBack.Size = new System.Drawing.Size(111, 35);
            this.butBack.TabIndex = 22;
            this.butBack.Text = "<- Назад";
            this.butBack.UseVisualStyleBackColor = true;
            // 
            // butSaveClient
            // 
            this.butSaveClient.Location = new System.Drawing.Point(547, 449);
            this.butSaveClient.Name = "butSaveClient";
            this.butSaveClient.Size = new System.Drawing.Size(207, 35);
            this.butSaveClient.TabIndex = 23;
            this.butSaveClient.Text = "Сохранить клиента";
            this.butSaveClient.UseVisualStyleBackColor = true;
            // 
            // formNewClientDocuments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(766, 496);
            this.Controls.Add(this.butSaveClient);
            this.Controls.Add(this.butBack);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.grb_invalid);
            this.Controls.Add(this.groupBox9);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formNewClientDocuments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Документы";
            this.grb_invalid.ResumeLayout(false);
            this.grb_invalid.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grb_invalid;
        private System.Windows.Forms.MaskedTextBox fldDisabilityDegree;
        private System.Windows.Forms.MaskedTextBox fldDisabilityGroup;
        private System.Windows.Forms.TextBox fldDisabilityAct;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.DateTimePicker fldDisabilityDate;
        private System.Windows.Forms.MaskedTextBox fldDisabilityNumber;
        private System.Windows.Forms.MaskedTextBox fldDisabilitySerial;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.MaskedTextBox fldPassportCode;
        private System.Windows.Forms.TextBox fldPassportIssued;
        private System.Windows.Forms.DateTimePicker fldPassportDate;
        private System.Windows.Forms.MaskedTextBox fldPassportNumber;
        private System.Windows.Forms.MaskedTextBox fldPassportSerial;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox fldCertificateVeteranNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateVeteranDate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox fldCertificateMemberNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateMemberDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox fldCertificateDisabledNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateDisabledDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox fldCertificatePensionNumber;
        private System.Windows.Forms.DateTimePicker fldCertificatePensionDate;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox fldCertificateMemberLaborNumber;
        private System.Windows.Forms.DateTimePicker fldCertificateMemberLaborDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.Button butSaveClient;
    }
}