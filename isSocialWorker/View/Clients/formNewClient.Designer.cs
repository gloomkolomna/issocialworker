﻿namespace isSocialWorker.View.Clients
{
    partial class formNewClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.fldLastname = new System.Windows.Forms.TextBox();
            this.fldFirstname = new System.Windows.Forms.TextBox();
            this.fldMiddlename = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.fldDateborn = new System.Windows.Forms.DateTimePicker();
            this.fldSnils = new System.Windows.Forms.MaskedTextBox();
            this.fldPhone = new System.Windows.Forms.TextBox();
            this.butCancel = new System.Windows.Forms.Button();
            this.butNext = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Фамилия";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(178, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Имя";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(344, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Отчество";
            // 
            // fldLastname
            // 
            this.fldLastname.Location = new System.Drawing.Point(12, 33);
            this.fldLastname.Name = "fldLastname";
            this.fldLastname.Size = new System.Drawing.Size(160, 30);
            this.fldLastname.TabIndex = 3;
            // 
            // fldFirstname
            // 
            this.fldFirstname.Location = new System.Drawing.Point(178, 33);
            this.fldFirstname.Name = "fldFirstname";
            this.fldFirstname.Size = new System.Drawing.Size(160, 30);
            this.fldFirstname.TabIndex = 4;
            // 
            // fldMiddlename
            // 
            this.fldMiddlename.Location = new System.Drawing.Point(344, 33);
            this.fldMiddlename.Name = "fldMiddlename";
            this.fldMiddlename.Size = new System.Drawing.Size(160, 30);
            this.fldMiddlename.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(159, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Дата рождения";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(178, 86);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 21);
            this.label5.TabIndex = 7;
            this.label5.Text = "СНИЛС";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(344, 86);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 21);
            this.label6.TabIndex = 8;
            this.label6.Text = "Телефон";
            // 
            // fldDateborn
            // 
            this.fldDateborn.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.fldDateborn.Location = new System.Drawing.Point(11, 110);
            this.fldDateborn.Name = "fldDateborn";
            this.fldDateborn.Size = new System.Drawing.Size(160, 30);
            this.fldDateborn.TabIndex = 9;
            // 
            // fldSnils
            // 
            this.fldSnils.Location = new System.Drawing.Point(178, 110);
            this.fldSnils.Mask = "999-999-999 99";
            this.fldSnils.Name = "fldSnils";
            this.fldSnils.Size = new System.Drawing.Size(160, 30);
            this.fldSnils.TabIndex = 10;
            // 
            // fldPhone
            // 
            this.fldPhone.Location = new System.Drawing.Point(344, 110);
            this.fldPhone.Name = "fldPhone";
            this.fldPhone.Size = new System.Drawing.Size(160, 30);
            this.fldPhone.TabIndex = 11;
            // 
            // butCancel
            // 
            this.butCancel.Location = new System.Drawing.Point(11, 192);
            this.butCancel.Name = "butCancel";
            this.butCancel.Size = new System.Drawing.Size(96, 32);
            this.butCancel.TabIndex = 14;
            this.butCancel.Text = "Отмена";
            this.butCancel.UseVisualStyleBackColor = true;
            // 
            // butNext
            // 
            this.butNext.Location = new System.Drawing.Point(374, 192);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(130, 32);
            this.butNext.TabIndex = 15;
            this.butNext.Text = "Вперед >";
            this.butNext.UseVisualStyleBackColor = true;
            // 
            // formNewClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(516, 236);
            this.Controls.Add(this.butNext);
            this.Controls.Add(this.butCancel);
            this.Controls.Add(this.fldPhone);
            this.Controls.Add(this.fldSnils);
            this.Controls.Add(this.fldDateborn);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fldMiddlename);
            this.Controls.Add(this.fldFirstname);
            this.Controls.Add(this.fldLastname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formNewClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новый клиент";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fldLastname;
        private System.Windows.Forms.TextBox fldFirstname;
        private System.Windows.Forms.TextBox fldMiddlename;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker fldDateborn;
        private System.Windows.Forms.MaskedTextBox fldSnils;
        private System.Windows.Forms.TextBox fldPhone;
        private System.Windows.Forms.Button butCancel;
        private System.Windows.Forms.Button butNext;
    }
}