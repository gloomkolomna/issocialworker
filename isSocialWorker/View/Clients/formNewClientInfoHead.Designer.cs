﻿namespace isSocialWorker.View.Clients
{
    partial class formNewClientInfoHead
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.checkIsMedicalworker = new System.Windows.Forms.CheckBox();
            this.comboDepartament = new System.Windows.Forms.ComboBox();
            this.comboSocworker = new System.Windows.Forms.ComboBox();
            this.comboMedicalworker = new System.Windows.Forms.ComboBox();
            this.comboConditionLiving = new System.Windows.Forms.ComboBox();
            this.comboHealth = new System.Windows.Forms.ComboBox();
            this.comboStatus = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkIsWork = new System.Windows.Forms.CheckBox();
            this.fldJob = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgvCategorys = new System.Windows.Forms.DataGridView();
            this.butRemoveCategory = new System.Windows.Forms.Button();
            this.butAddCategory = new System.Windows.Forms.Button();
            this.comboCategorys = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.butBack = new System.Windows.Forms.Button();
            this.butNext = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategorys)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Отделение";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(270, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(227, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Социальный работник";
            // 
            // checkIsMedicalworker
            // 
            this.checkIsMedicalworker.AutoSize = true;
            this.checkIsMedicalworker.Location = new System.Drawing.Point(533, 8);
            this.checkIsMedicalworker.Name = "checkIsMedicalworker";
            this.checkIsMedicalworker.Size = new System.Drawing.Size(260, 25);
            this.checkIsMedicalworker.TabIndex = 2;
            this.checkIsMedicalworker.Text = "Медицинский работник";
            this.checkIsMedicalworker.UseVisualStyleBackColor = true;
            this.checkIsMedicalworker.CheckedChanged += new System.EventHandler(this.checkIsMedicalworker_CheckedChanged);
            // 
            // comboDepartament
            // 
            this.comboDepartament.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboDepartament.FormattingEnabled = true;
            this.comboDepartament.Location = new System.Drawing.Point(16, 33);
            this.comboDepartament.Name = "comboDepartament";
            this.comboDepartament.Size = new System.Drawing.Size(248, 29);
            this.comboDepartament.TabIndex = 3;
            // 
            // comboSocworker
            // 
            this.comboSocworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSocworker.FormattingEnabled = true;
            this.comboSocworker.Location = new System.Drawing.Point(270, 33);
            this.comboSocworker.Name = "comboSocworker";
            this.comboSocworker.Size = new System.Drawing.Size(257, 29);
            this.comboSocworker.TabIndex = 4;
            // 
            // comboMedicalworker
            // 
            this.comboMedicalworker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMedicalworker.Enabled = false;
            this.comboMedicalworker.FormattingEnabled = true;
            this.comboMedicalworker.Location = new System.Drawing.Point(533, 33);
            this.comboMedicalworker.Name = "comboMedicalworker";
            this.comboMedicalworker.Size = new System.Drawing.Size(257, 29);
            this.comboMedicalworker.TabIndex = 5;
            // 
            // comboConditionLiving
            // 
            this.comboConditionLiving.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionLiving.FormattingEnabled = true;
            this.comboConditionLiving.Location = new System.Drawing.Point(533, 101);
            this.comboConditionLiving.Name = "comboConditionLiving";
            this.comboConditionLiving.Size = new System.Drawing.Size(257, 29);
            this.comboConditionLiving.TabIndex = 11;
            // 
            // comboHealth
            // 
            this.comboHealth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboHealth.FormattingEnabled = true;
            this.comboHealth.Location = new System.Drawing.Point(270, 101);
            this.comboHealth.Name = "comboHealth";
            this.comboHealth.Size = new System.Drawing.Size(257, 29);
            this.comboHealth.TabIndex = 10;
            // 
            // comboStatus
            // 
            this.comboStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStatus.FormattingEnabled = true;
            this.comboStatus.Location = new System.Drawing.Point(16, 101);
            this.comboStatus.Name = "comboStatus";
            this.comboStatus.Size = new System.Drawing.Size(248, 29);
            this.comboStatus.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(270, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(204, 21);
            this.label3.TabIndex = 7;
            this.label3.Text = "Состояние здоровья";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Статус";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(533, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(255, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Оценка бытовых условий";
            // 
            // checkIsWork
            // 
            this.checkIsWork.AutoSize = true;
            this.checkIsWork.Location = new System.Drawing.Point(16, 151);
            this.checkIsWork.Name = "checkIsWork";
            this.checkIsWork.Size = new System.Drawing.Size(271, 25);
            this.checkIsWork.TabIndex = 13;
            this.checkIsWork.Text = "Работает? Место работы:";
            this.checkIsWork.UseVisualStyleBackColor = true;
            this.checkIsWork.CheckedChanged += new System.EventHandler(this.checkIsWork_CheckedChanged);
            // 
            // fldJob
            // 
            this.fldJob.Enabled = false;
            this.fldJob.Location = new System.Drawing.Point(284, 149);
            this.fldJob.Name = "fldJob";
            this.fldJob.Size = new System.Drawing.Size(506, 30);
            this.fldJob.TabIndex = 14;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgvCategorys);
            this.groupBox1.Controls.Add(this.butRemoveCategory);
            this.groupBox1.Controls.Add(this.butAddCategory);
            this.groupBox1.Controls.Add(this.comboCategorys);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(16, 195);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(774, 162);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Категории";
            // 
            // dgvCategorys
            // 
            this.dgvCategorys.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCategorys.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCategorys.Location = new System.Drawing.Point(268, 26);
            this.dgvCategorys.MultiSelect = false;
            this.dgvCategorys.Name = "dgvCategorys";
            this.dgvCategorys.ReadOnly = true;
            this.dgvCategorys.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCategorys.Size = new System.Drawing.Size(500, 127);
            this.dgvCategorys.TabIndex = 20;
            // 
            // butRemoveCategory
            // 
            this.butRemoveCategory.Location = new System.Drawing.Point(6, 122);
            this.butRemoveCategory.Name = "butRemoveCategory";
            this.butRemoveCategory.Size = new System.Drawing.Size(242, 31);
            this.butRemoveCategory.TabIndex = 19;
            this.butRemoveCategory.Text = "Удалить категорию";
            this.butRemoveCategory.UseVisualStyleBackColor = true;
            // 
            // butAddCategory
            // 
            this.butAddCategory.Location = new System.Drawing.Point(6, 85);
            this.butAddCategory.Name = "butAddCategory";
            this.butAddCategory.Size = new System.Drawing.Size(242, 31);
            this.butAddCategory.TabIndex = 18;
            this.butAddCategory.Text = "Добавить категорию";
            this.butAddCategory.UseVisualStyleBackColor = true;
            // 
            // comboCategorys
            // 
            this.comboCategorys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCategorys.FormattingEnabled = true;
            this.comboCategorys.Location = new System.Drawing.Point(6, 50);
            this.comboCategorys.Name = "comboCategorys";
            this.comboCategorys.Size = new System.Drawing.Size(242, 29);
            this.comboCategorys.TabIndex = 17;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(213, 21);
            this.label6.TabIndex = 16;
            this.label6.Text = "Выберите категорию";
            // 
            // butBack
            // 
            this.butBack.Location = new System.Drawing.Point(16, 375);
            this.butBack.Name = "butBack";
            this.butBack.Size = new System.Drawing.Size(94, 36);
            this.butBack.TabIndex = 16;
            this.butBack.Text = "< Назад";
            this.butBack.UseVisualStyleBackColor = true;
            // 
            // butNext
            // 
            this.butNext.Location = new System.Drawing.Point(684, 375);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(106, 36);
            this.butNext.TabIndex = 17;
            this.butNext.Text = "Вперед >";
            this.butNext.UseVisualStyleBackColor = true;
            // 
            // formNewClientInfoHead
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(807, 426);
            this.Controls.Add(this.butNext);
            this.Controls.Add(this.butBack);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.fldJob);
            this.Controls.Add(this.checkIsWork);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboConditionLiving);
            this.Controls.Add(this.comboHealth);
            this.Controls.Add(this.comboStatus);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboMedicalworker);
            this.Controls.Add(this.comboSocworker);
            this.Controls.Add(this.comboDepartament);
            this.Controls.Add(this.checkIsMedicalworker);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formNewClientInfoHead";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Информация для заведующих по клиенту";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCategorys)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox checkIsMedicalworker;
        private System.Windows.Forms.ComboBox comboDepartament;
        private System.Windows.Forms.ComboBox comboSocworker;
        private System.Windows.Forms.ComboBox comboMedicalworker;
        private System.Windows.Forms.ComboBox comboConditionLiving;
        private System.Windows.Forms.ComboBox comboHealth;
        private System.Windows.Forms.ComboBox comboStatus;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkIsWork;
        private System.Windows.Forms.TextBox fldJob;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgvCategorys;
        private System.Windows.Forms.Button butRemoveCategory;
        private System.Windows.Forms.Button butAddCategory;
        private System.Windows.Forms.ComboBox comboCategorys;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.Button butNext;
    }
}