﻿namespace isSocialWorker.View.Clients
{
    partial class formNewClientAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.comboCity = new System.Windows.Forms.ComboBox();
            this.comboStreet = new System.Windows.Forms.ComboBox();
            this.comboHouse = new System.Windows.Forms.ComboBox();
            this.comboAppartament = new System.Windows.Forms.ComboBox();
            this.butBack = new System.Windows.Forms.Button();
            this.butNext = new System.Windows.Forms.Button();
            this.comboRegion = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkRefugee = new System.Windows.Forms.CheckBox();
            this.checkHomeless = new System.Windows.Forms.CheckBox();
            this.groupBoxAddress = new System.Windows.Forms.GroupBox();
            this.groupBoxAddress.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(189, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Населенный пункт";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 128);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Улица";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Дом";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 224);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(103, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "Квартира";
            // 
            // comboCity
            // 
            this.comboCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCity.FormattingEnabled = true;
            this.comboCity.Location = new System.Drawing.Point(228, 76);
            this.comboCity.Name = "comboCity";
            this.comboCity.Size = new System.Drawing.Size(319, 29);
            this.comboCity.TabIndex = 4;
            // 
            // comboStreet
            // 
            this.comboStreet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboStreet.FormattingEnabled = true;
            this.comboStreet.Location = new System.Drawing.Point(228, 124);
            this.comboStreet.Name = "comboStreet";
            this.comboStreet.Size = new System.Drawing.Size(319, 29);
            this.comboStreet.TabIndex = 5;
            // 
            // comboHouse
            // 
            this.comboHouse.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboHouse.FormattingEnabled = true;
            this.comboHouse.Location = new System.Drawing.Point(228, 172);
            this.comboHouse.Name = "comboHouse";
            this.comboHouse.Size = new System.Drawing.Size(319, 29);
            this.comboHouse.TabIndex = 6;
            // 
            // comboAppartament
            // 
            this.comboAppartament.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboAppartament.FormattingEnabled = true;
            this.comboAppartament.Location = new System.Drawing.Point(228, 220);
            this.comboAppartament.Name = "comboAppartament";
            this.comboAppartament.Size = new System.Drawing.Size(319, 29);
            this.comboAppartament.TabIndex = 7;
            // 
            // butBack
            // 
            this.butBack.Location = new System.Drawing.Point(12, 355);
            this.butBack.Name = "butBack";
            this.butBack.Size = new System.Drawing.Size(110, 35);
            this.butBack.TabIndex = 9;
            this.butBack.Text = "<- Назад";
            this.butBack.UseVisualStyleBackColor = true;
            // 
            // butNext
            // 
            this.butNext.Location = new System.Drawing.Point(452, 355);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(113, 35);
            this.butNext.TabIndex = 10;
            this.butNext.Text = "Вперед ->";
            this.butNext.UseVisualStyleBackColor = true;
            // 
            // comboRegion
            // 
            this.comboRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboRegion.FormattingEnabled = true;
            this.comboRegion.Location = new System.Drawing.Point(228, 32);
            this.comboRegion.Name = "comboRegion";
            this.comboRegion.Size = new System.Drawing.Size(319, 29);
            this.comboRegion.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(146, 21);
            this.label1.TabIndex = 12;
            this.label1.Text = "Район / город";
            // 
            // checkRefugee
            // 
            this.checkRefugee.AutoSize = true;
            this.checkRefugee.Location = new System.Drawing.Point(16, 12);
            this.checkRefugee.Name = "checkRefugee";
            this.checkRefugee.Size = new System.Drawing.Size(115, 25);
            this.checkRefugee.TabIndex = 1;
            this.checkRefugee.Text = "Беженец";
            this.checkRefugee.UseVisualStyleBackColor = true;
            // 
            // checkHomeless
            // 
            this.checkHomeless.AutoSize = true;
            this.checkHomeless.Location = new System.Drawing.Point(137, 12);
            this.checkHomeless.Name = "checkHomeless";
            this.checkHomeless.Size = new System.Drawing.Size(388, 25);
            this.checkHomeless.TabIndex = 2;
            this.checkHomeless.Text = "Без определенного места жительства";
            this.checkHomeless.UseVisualStyleBackColor = true;
            // 
            // groupBoxAddress
            // 
            this.groupBoxAddress.Controls.Add(this.label1);
            this.groupBoxAddress.Controls.Add(this.label2);
            this.groupBoxAddress.Controls.Add(this.label3);
            this.groupBoxAddress.Controls.Add(this.label4);
            this.groupBoxAddress.Controls.Add(this.comboRegion);
            this.groupBoxAddress.Controls.Add(this.label5);
            this.groupBoxAddress.Controls.Add(this.comboCity);
            this.groupBoxAddress.Controls.Add(this.comboStreet);
            this.groupBoxAddress.Controls.Add(this.comboHouse);
            this.groupBoxAddress.Controls.Add(this.comboAppartament);
            this.groupBoxAddress.Location = new System.Drawing.Point(12, 43);
            this.groupBoxAddress.Name = "groupBoxAddress";
            this.groupBoxAddress.Size = new System.Drawing.Size(553, 285);
            this.groupBoxAddress.TabIndex = 17;
            this.groupBoxAddress.TabStop = false;
            this.groupBoxAddress.Text = "Житель Московской области";
            // 
            // formNewClientAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(577, 396);
            this.Controls.Add(this.groupBoxAddress);
            this.Controls.Add(this.checkHomeless);
            this.Controls.Add(this.checkRefugee);
            this.Controls.Add(this.butNext);
            this.Controls.Add(this.butBack);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formNewClientAddress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Адрес проживания клиента";
            this.groupBoxAddress.ResumeLayout(false);
            this.groupBoxAddress.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboCity;
        private System.Windows.Forms.ComboBox comboStreet;
        private System.Windows.Forms.ComboBox comboHouse;
        private System.Windows.Forms.ComboBox comboAppartament;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.Button butNext;
        private System.Windows.Forms.ComboBox comboRegion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkRefugee;
        private System.Windows.Forms.CheckBox checkHomeless;
        private System.Windows.Forms.GroupBox groupBoxAddress;
    }
}