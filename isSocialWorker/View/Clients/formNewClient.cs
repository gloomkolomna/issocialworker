﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using isSocialWorker.Properties;
using isSocialWorker.Client;

namespace isSocialWorker.View.Clients
{
    public interface IFormNewClient
    {
        string LastName { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string DateBorn { get; set; }
        string Snils { get; set; }
        string Phone { get; set; }

        formNewClient Parentform { get; }

        event EventHandler SetBufferedClient;
        event EventHandler LoadClientForm;
    }

    public partial class formNewClient : Form, IFormNewClient
    {
        public formNewClient()
        {
            InitializeComponent();
            butNext.Click += butNext_Click;
            this.Load += formNewClient_Load;
            this.FormClosed += formNewClient_FormClosed;
        }

        void formNewClient_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientInfo.ClearAllFields();
            ClientInfoAddress.ClearAllFields();
            ClientInfoHead.ClearAllFields();
            ClientInfoHousing.ClearAllFields();
            ClientDocuments.ClearAllFields();
        }

        void formNewClient_Load(object sender, EventArgs e)
        {
            if (LoadClientForm != null) LoadClientForm(this, EventArgs.Empty);
        }

        void butNext_Click(object sender, EventArgs e)
        {
            bool filled = this.Controls.OfType<TextBox>().All(textBox => textBox.Text != "");

            if (filled)
            {
                if (fldSnils.MaskFull)
                {
                    if (SetBufferedClient != null) SetBufferedClient(this, EventArgs.Empty);
                }
                else
                {
                    MessageBox.Show("Заполните СНИЛС!");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Необходимо заполнить все поля!");
                return;
            }
        }

        #region IFormNewClient
        public string LastName
        {
            get { return fldLastname.Text; }
            set { fldLastname.Text = value; }
        }

        public string FirstName
        {
            get { return fldFirstname.Text; }
            set { fldFirstname.Text = value; }
        }

        public string MiddleName
        {
            get { return fldMiddlename.Text; }
            set { fldMiddlename.Text = value; }
        }

        public string DateBorn
        {
            get { return fldDateborn.Value.ToString("yyyy-MM-dd");}
            set { fldDateborn.Text = value; }
        }

        public string Snils
        {
            get { return fldSnils.Text; }
            set { fldSnils.Text = value; }
        }

        public string Phone
        {
            get { return fldPhone.Text; }
            set { fldPhone.Text = value; }
        }

        public formNewClient Parentform
        {
            get
            {
                return this;
            }
        }

        public event EventHandler SetBufferedClient;
        public event EventHandler LoadClientForm;

        #endregion
    }
}
