﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using isSocialWorker.Model;
using isSocialWorker.Presenter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace isSocialWorker.View.Clients
{
    public interface IFormNewClientInfoHead
    {
        int DepartamentId { get; set; }
        int SocialWorkerId { get; set; }
        int MedicalWorkerId { get; set; }
        int StatusId { get; set; }
        int HealthId { get; set; }
        int ConditionLivingId { get; set; }
        string Job { get; set; }
        string CategoryIdName { get; }
        int CategorySelectedId { get; }
        
        void DepartamentList();
        void SocialWorkerList();
        void MedicalWorkerList();
        void StatusList();
        void HealthList();
        void ConditionLivingList();
        void CategoryList();
        void FillingCategory();
        List<DirectoryDefault> Lists { set; }
        List<DirectoryDefault> Category { get; set; }

        event EventHandler LoadInfoHeadFrom;
        event EventHandler AddCategoryToList;
        event EventHandler RemoveCategoryToList;
        event EventHandler NextButtonClick;
        event EventHandler BackButtonClick;

        formNewClientInfoHead Parentform { get; }
    }

    public partial class formNewClientInfoHead : Form, IFormNewClientInfoHead
    {
        public formNewClientInfoHead()
        {
            InitializeComponent();
            butBack.Click += butBack_Click;
            this.Load += formNewClientInfoHead_Load;
            this.FormClosed += formNewClientInfoHead_FormClosed;
            butAddCategory.Click += butAddCategory_Click;
            butRemoveCategory.Click += butRemoveCategory_Click;
            butNext.Click += butNext_Click;
        }

        void butNext_Click(object sender, EventArgs e)
        {
            if (NextButtonClick != null) NextButtonClick(this, EventArgs.Empty);
        }

        void butRemoveCategory_Click(object sender, EventArgs e)
        {
            if (RemoveCategoryToList != null) RemoveCategoryToList(this, EventArgs.Empty);
        }

        void butAddCategory_Click(object sender, EventArgs e)
        {
            if (AddCategoryToList != null) AddCategoryToList(this, EventArgs.Empty);
        }

        void formNewClientInfoHead_Load(object sender, EventArgs e)
        {
            if (LoadInfoHeadFrom != null) LoadInfoHeadFrom(this, EventArgs.Empty);
        }

        void formNewClientInfoHead_FormClosed(object sender, FormClosedEventArgs e)
        {
            ClientInfo.ClearAllFields();
            ClientInfoAddress.ClearAllFields();
            ClientInfoHead.ClearAllFields();
            ClientInfoHousing.ClearAllFields();
            ClientDocuments.ClearAllFields();
        }

        void butBack_Click(object sender, EventArgs e)
        {
            formNewClient form = new formNewClient();
            NewClientModel model = new NewClientModel();
            MessageService message = new MessageService();

            NewClientPresenter presenter = new NewClientPresenter(form, model, message);

            this.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();

            if (BackButtonClick != null) BackButtonClick(this, EventArgs.Empty);
        }

        private List<DirectoryDefault> list = new List<DirectoryDefault>();
        private List<DirectoryDefault> categorys = new List<DirectoryDefault>();

        #region IFormNewClientInfoHead
        public event EventHandler LoadInfoHeadFrom;
        public event EventHandler AddCategoryToList;
        public event EventHandler RemoveCategoryToList;
        public event EventHandler NextButtonClick;
        public event EventHandler BackButtonClick;

        public List<DirectoryDefault> Lists
        {
            set { list = value; }
        }

        public List<DirectoryDefault> Category
        {
            get
            {
                int count = dgvCategorys.RowCount;
                for (int i = 0; i < count; i++)
                    categorys.Add(new DirectoryDefault()
                    {
                        Id = Convert.ToInt32(dgvCategorys[0, i].ToString()),
                        Name = dgvCategorys[1, i].ToString()
                    });

                return categorys;
            }
            set { categorys = value; }
        }

        public int CategorySelectedId
        {
            get
            {
                return Convert.ToInt32(dgvCategorys[0,dgvCategorys.CurrentRow.Index].Value.ToString());
            }
        }

        public int DepartamentId
        {
            get
            {
                try
                {
                    return (int)comboDepartament.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboDepartament.SelectedValue = value;
            }
        }

        public int SocialWorkerId
        {
            get
            {
                try
                {
                    return (int)comboSocworker.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboSocworker.SelectedValue = value;
            }
        }

        public int MedicalWorkerId
        {
            get
            {
                try
                {
                    return (comboMedicalworker.Items.Count == 0) ? 0 : (int)comboMedicalworker.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboMedicalworker.SelectedValue = value;
            }
        }

        public int StatusId
        {
            get
            {
                try
                {
                    return (int)comboStatus.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboStatus.SelectedValue = value;
            }
        }

        public int HealthId
        {
            get
            {
                try
                {
                    return (int)comboHealth.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboHealth.SelectedValue = value;
            }
        }

        public int ConditionLivingId
        {
            get
            {
                try
                {
                    return (int)comboConditionLiving.SelectedValue;
                }
                catch { return 0; }
            }
            set
            {
                comboConditionLiving.SelectedValue = value;
            }
        }

        public string Job
        {
            get
            {
                return fldJob.Text;
            }
            set
            {
                fldJob.Text = value;
            }
        }

        #endregion

        #region Filling Combobox
        public void DepartamentList()
        {
            FillingComboBox(comboDepartament);
        }

        public void SocialWorkerList()
        {
            FillingComboBox(comboSocworker);
        }

        public void MedicalWorkerList()
        {
            FillingComboBox(comboMedicalworker);
        }

        public void StatusList()
        {
            FillingComboBox(comboStatus);
        }

        public void HealthList()
        {
            FillingComboBox(comboHealth);
        }

        public void ConditionLivingList()
        {
            FillingComboBox(comboConditionLiving);
        }

        public void CategoryList()
        {
            FillingComboBox(comboCategorys);
        }

        private void FillingComboBox(ComboBox comboBox)
        {
            var sourse = (from a in list
                          select new { a.Id, Names = a.Name }).ToList();
            comboBox.DataSource = sourse;
            comboBox.DisplayMember = "Names";
            comboBox.ValueMember = "Id";
        }

        public string CategoryIdName
        {
            get
            {
                string str = comboCategorys.SelectedValue.ToString() + ";" + comboCategorys.Text;
                return str;
            }
        }
        #endregion
        
        public void FillingCategory()
        {
            var sourse = categorys.ToList();
            dgvCategorys.DataSource = sourse;
        }

        private void checkIsMedicalworker_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIsMedicalworker.Checked)
                comboMedicalworker.Enabled = true;
            else
                comboMedicalworker.Enabled = false;
        }

        private void checkIsWork_CheckedChanged(object sender, EventArgs e)
        {
            if (checkIsWork.Checked)
                fldJob.Enabled = true;
            else
                fldJob.Enabled = false;
        }

        public formNewClientInfoHead Parentform
        {
            get { return this; }
        }
    }
}
