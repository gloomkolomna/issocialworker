﻿namespace isSocialWorker.View.Clients
{
    partial class formNewClientInfoHousing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboTypeHousing = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.fldArea = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.fldCountRoom = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboConditionHousing = new System.Windows.Forms.ComboBox();
            this.checkRepairCosmetic = new System.Windows.Forms.CheckBox();
            this.checkRepairMajor = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboConditionSanitary = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboConditionHeating = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboConditionWater = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboConditionGas = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.butBack = new System.Windows.Forms.Button();
            this.butNext = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Вид жилья";
            // 
            // comboTypeHousing
            // 
            this.comboTypeHousing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboTypeHousing.FormattingEnabled = true;
            this.comboTypeHousing.Location = new System.Drawing.Point(16, 33);
            this.comboTypeHousing.Name = "comboTypeHousing";
            this.comboTypeHousing.Size = new System.Drawing.Size(223, 29);
            this.comboTypeHousing.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(245, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Площадь";
            // 
            // fldArea
            // 
            this.fldArea.Location = new System.Drawing.Point(245, 32);
            this.fldArea.Name = "fldArea";
            this.fldArea.Size = new System.Drawing.Size(100, 30);
            this.fldArea.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(351, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Комнат";
            // 
            // fldCountRoom
            // 
            this.fldCountRoom.Location = new System.Drawing.Point(351, 33);
            this.fldCountRoom.Name = "fldCountRoom";
            this.fldCountRoom.Size = new System.Drawing.Size(100, 30);
            this.fldCountRoom.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(193, 47);
            this.label4.TabIndex = 6;
            this.label4.Text = "Состояние жилого помещения";
            // 
            // comboConditionHousing
            // 
            this.comboConditionHousing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionHousing.FormattingEnabled = true;
            this.comboConditionHousing.Location = new System.Drawing.Point(16, 134);
            this.comboConditionHousing.Name = "comboConditionHousing";
            this.comboConditionHousing.Size = new System.Drawing.Size(223, 29);
            this.comboConditionHousing.TabIndex = 7;
            // 
            // checkRepairCosmetic
            // 
            this.checkRepairCosmetic.AutoSize = true;
            this.checkRepairCosmetic.Location = new System.Drawing.Point(11, 29);
            this.checkRepairCosmetic.Name = "checkRepairCosmetic";
            this.checkRepairCosmetic.Size = new System.Drawing.Size(177, 25);
            this.checkRepairCosmetic.TabIndex = 8;
            this.checkRepairCosmetic.Text = "Косметический";
            this.checkRepairCosmetic.UseVisualStyleBackColor = true;
            // 
            // checkRepairMajor
            // 
            this.checkRepairMajor.AutoSize = true;
            this.checkRepairMajor.Location = new System.Drawing.Point(11, 54);
            this.checkRepairMajor.Name = "checkRepairMajor";
            this.checkRepairMajor.Size = new System.Drawing.Size(158, 25);
            this.checkRepairMajor.TabIndex = 9;
            this.checkRepairMajor.Text = "Капитальный";
            this.checkRepairMajor.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkRepairCosmetic);
            this.groupBox1.Controls.Add(this.checkRepairMajor);
            this.groupBox1.Location = new System.Drawing.Point(245, 78);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(206, 88);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Требуется ремонт";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.comboConditionSanitary);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.comboConditionHeating);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comboConditionWater);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.comboConditionGas);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(16, 180);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(435, 141);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Условия проживания";
            // 
            // comboConditionSanitary
            // 
            this.comboConditionSanitary.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionSanitary.FormattingEnabled = true;
            this.comboConditionSanitary.Location = new System.Drawing.Point(218, 103);
            this.comboConditionSanitary.Name = "comboConditionSanitary";
            this.comboConditionSanitary.Size = new System.Drawing.Size(210, 29);
            this.comboConditionSanitary.TabIndex = 19;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(218, 79);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 21);
            this.label7.TabIndex = 18;
            this.label7.Text = "Санитарные условия";
            // 
            // comboConditionHeating
            // 
            this.comboConditionHeating.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionHeating.FormattingEnabled = true;
            this.comboConditionHeating.Location = new System.Drawing.Point(6, 103);
            this.comboConditionHeating.Name = "comboConditionHeating";
            this.comboConditionHeating.Size = new System.Drawing.Size(206, 29);
            this.comboConditionHeating.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 79);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 21);
            this.label8.TabIndex = 16;
            this.label8.Text = "Отопление";
            // 
            // comboConditionWater
            // 
            this.comboConditionWater.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionWater.FormattingEnabled = true;
            this.comboConditionWater.Location = new System.Drawing.Point(218, 48);
            this.comboConditionWater.Name = "comboConditionWater";
            this.comboConditionWater.Size = new System.Drawing.Size(210, 29);
            this.comboConditionWater.TabIndex = 15;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(218, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 21);
            this.label6.TabIndex = 14;
            this.label6.Text = "Вода";
            // 
            // comboConditionGas
            // 
            this.comboConditionGas.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboConditionGas.FormattingEnabled = true;
            this.comboConditionGas.Location = new System.Drawing.Point(6, 48);
            this.comboConditionGas.Name = "comboConditionGas";
            this.comboConditionGas.Size = new System.Drawing.Size(206, 29);
            this.comboConditionGas.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Газ";
            // 
            // butBack
            // 
            this.butBack.Location = new System.Drawing.Point(16, 331);
            this.butBack.Name = "butBack";
            this.butBack.Size = new System.Drawing.Size(102, 33);
            this.butBack.TabIndex = 12;
            this.butBack.Text = "<- Назад";
            this.butBack.UseVisualStyleBackColor = true;
            // 
            // butNext
            // 
            this.butNext.Location = new System.Drawing.Point(336, 331);
            this.butNext.Name = "butNext";
            this.butNext.Size = new System.Drawing.Size(115, 33);
            this.butNext.TabIndex = 13;
            this.butNext.Text = "Вперед ->";
            this.butNext.UseVisualStyleBackColor = true;
            // 
            // formNewClientInfoHousing
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(465, 370);
            this.Controls.Add(this.butNext);
            this.Controls.Add(this.butBack);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.comboConditionHousing);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.fldCountRoom);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.fldArea);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboTypeHousing);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formNewClientInfoHousing";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Жилищные условия";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboTypeHousing;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fldArea;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox fldCountRoom;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboConditionHousing;
        private System.Windows.Forms.CheckBox checkRepairCosmetic;
        private System.Windows.Forms.CheckBox checkRepairMajor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboConditionSanitary;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboConditionHeating;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboConditionWater;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboConditionGas;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button butBack;
        private System.Windows.Forms.Button butNext;
    }
}