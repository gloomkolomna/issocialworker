﻿namespace isSocialWorker.View
{
    partial class formBasic
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formBasic));
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.menuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileNewClient = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileClientList = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFileDirectory = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettingsConnect = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSettingsSystem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuFileExit = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperations = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperationsIncoming = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperationsPayments = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperationsExport = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperationsExportXML = new System.Windows.Forms.ToolStripMenuItem();
            this.menuOperationsExportXLS = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsPanel = new System.Windows.Forms.ToolStrip();
            this.toolNewClient = new System.Windows.Forms.ToolStripButton();
            this.toolClientsList = new System.Windows.Forms.ToolStripButton();
            this.menuMain.SuspendLayout();
            this.toolsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFile,
            this.menuOperations});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(866, 24);
            this.menuMain.TabIndex = 1;
            this.menuMain.Text = "menuStrip1";
            // 
            // menuFile
            // 
            this.menuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuFileNewClient,
            this.menuFileClientList,
            this.menuFileDirectory,
            this.menuSettings,
            this.toolStripMenuItem1,
            this.menuFileExit});
            this.menuFile.Name = "menuFile";
            this.menuFile.Size = new System.Drawing.Size(48, 20);
            this.menuFile.Text = "Файл";
            // 
            // menuFileNewClient
            // 
            this.menuFileNewClient.Name = "menuFileNewClient";
            this.menuFileNewClient.Size = new System.Drawing.Size(196, 22);
            this.menuFileNewClient.Text = "Добавить клиента (F1)";
            this.menuFileNewClient.Click += new System.EventHandler(this.menuFileNewClient_Click);
            // 
            // menuFileClientList
            // 
            this.menuFileClientList.Name = "menuFileClientList";
            this.menuFileClientList.Size = new System.Drawing.Size(196, 22);
            this.menuFileClientList.Text = "Список клиентов (F2)";
            this.menuFileClientList.Click += new System.EventHandler(this.menuFileClientList_Click);
            // 
            // menuFileDirectory
            // 
            this.menuFileDirectory.Name = "menuFileDirectory";
            this.menuFileDirectory.Size = new System.Drawing.Size(196, 22);
            this.menuFileDirectory.Text = "Справочники (F9)";
            // 
            // menuSettings
            // 
            this.menuSettings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuSettingsConnect,
            this.menuSettingsSystem});
            this.menuSettings.Name = "menuSettings";
            this.menuSettings.Size = new System.Drawing.Size(196, 22);
            this.menuSettings.Text = "Настройки";
            // 
            // menuSettingsConnect
            // 
            this.menuSettingsConnect.Name = "menuSettingsConnect";
            this.menuSettingsConnect.Size = new System.Drawing.Size(242, 22);
            this.menuSettingsConnect.Text = "Настройки подключения (F10)";
            this.menuSettingsConnect.Click += new System.EventHandler(this.menuSettingsConnect_Click);
            // 
            // menuSettingsSystem
            // 
            this.menuSettingsSystem.Name = "menuSettingsSystem";
            this.menuSettingsSystem.Size = new System.Drawing.Size(242, 22);
            this.menuSettingsSystem.Text = "Системные настройки (F11)";
            this.menuSettingsSystem.Click += new System.EventHandler(this.menuSettingsSystem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(193, 6);
            // 
            // menuFileExit
            // 
            this.menuFileExit.Name = "menuFileExit";
            this.menuFileExit.Size = new System.Drawing.Size(196, 22);
            this.menuFileExit.Text = "Выход";
            this.menuFileExit.Click += new System.EventHandler(this.menuFileExit_Click);
            // 
            // menuOperations
            // 
            this.menuOperations.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOperationsIncoming,
            this.menuOperationsPayments,
            this.menuOperationsExport});
            this.menuOperations.Name = "menuOperations";
            this.menuOperations.Size = new System.Drawing.Size(75, 20);
            this.menuOperations.Text = "Операции";
            // 
            // menuOperationsIncoming
            // 
            this.menuOperationsIncoming.Name = "menuOperationsIncoming";
            this.menuOperationsIncoming.Size = new System.Drawing.Size(232, 22);
            this.menuOperationsIncoming.Text = "Загрузка доходов";
            // 
            // menuOperationsPayments
            // 
            this.menuOperationsPayments.Name = "menuOperationsPayments";
            this.menuOperationsPayments.Size = new System.Drawing.Size(232, 22);
            this.menuOperationsPayments.Text = "Перерасчет условий оплаты";
            // 
            // menuOperationsExport
            // 
            this.menuOperationsExport.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuOperationsExportXML,
            this.menuOperationsExportXLS});
            this.menuOperationsExport.Name = "menuOperationsExport";
            this.menuOperationsExport.Size = new System.Drawing.Size(232, 22);
            this.menuOperationsExport.Text = "Выгрузить услуги";
            // 
            // menuOperationsExportXML
            // 
            this.menuOperationsExportXML.Name = "menuOperationsExportXML";
            this.menuOperationsExportXML.Size = new System.Drawing.Size(146, 22);
            this.menuOperationsExportXML.Text = "Формат XML";
            // 
            // menuOperationsExportXLS
            // 
            this.menuOperationsExportXLS.Name = "menuOperationsExportXLS";
            this.menuOperationsExportXLS.Size = new System.Drawing.Size(146, 22);
            this.menuOperationsExportXLS.Text = "Формат Excel";
            // 
            // toolsPanel
            // 
            this.toolsPanel.AutoSize = false;
            this.toolsPanel.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolsPanel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolNewClient,
            this.toolClientsList});
            this.toolsPanel.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.toolsPanel.Location = new System.Drawing.Point(0, 24);
            this.toolsPanel.Name = "toolsPanel";
            this.toolsPanel.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.toolsPanel.Size = new System.Drawing.Size(866, 36);
            this.toolsPanel.TabIndex = 2;
            this.toolsPanel.Text = "toolStrip1";
            // 
            // toolNewClient
            // 
            this.toolNewClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolNewClient.Image = global::isSocialWorker.Properties.Resources.add;
            this.toolNewClient.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolNewClient.Name = "toolNewClient";
            this.toolNewClient.Size = new System.Drawing.Size(36, 36);
            this.toolNewClient.Text = "Добавить нового клиента (F1)";
            this.toolNewClient.TextImageRelation = System.Windows.Forms.TextImageRelation.Overlay;
            this.toolNewClient.Click += new System.EventHandler(this.toolNewClient_Click);
            // 
            // toolClientsList
            // 
            this.toolClientsList.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolClientsList.Image = global::isSocialWorker.Properties.Resources.users_3;
            this.toolClientsList.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolClientsList.Name = "toolClientsList";
            this.toolClientsList.Size = new System.Drawing.Size(36, 36);
            this.toolClientsList.Text = "Список клиентов";
            this.toolClientsList.Click += new System.EventHandler(this.toolClientsList_Click);
            // 
            // formBasic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(866, 538);
            this.Controls.Add(this.toolsPanel);
            this.Controls.Add(this.menuMain);
            this.Font = new System.Drawing.Font("Bookman Old Style", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuMain;
            this.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Name = "formBasic";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Информационная система учета пожилых граждан и инвалидов";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.toolsPanel.ResumeLayout(false);
            this.toolsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem menuFile;
        private System.Windows.Forms.ToolStripMenuItem menuFileDirectory;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem menuFileExit;
        private System.Windows.Forms.ToolStripMenuItem menuOperations;
        private System.Windows.Forms.ToolStripMenuItem menuOperationsIncoming;
        private System.Windows.Forms.ToolStripMenuItem menuOperationsPayments;
        private System.Windows.Forms.ToolStripMenuItem menuOperationsExport;
        private System.Windows.Forms.ToolStripMenuItem menuOperationsExportXML;
        private System.Windows.Forms.ToolStripMenuItem menuOperationsExportXLS;
        private System.Windows.Forms.ToolStrip toolsPanel;
        private System.Windows.Forms.ToolStripButton toolNewClient;
        private System.Windows.Forms.ToolStripButton toolClientsList;
        private System.Windows.Forms.ToolStripMenuItem menuSettings;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsConnect;
        private System.Windows.Forms.ToolStripMenuItem menuSettingsSystem;
        private System.Windows.Forms.ToolStripMenuItem menuFileNewClient;
        private System.Windows.Forms.ToolStripMenuItem menuFileClientList;
    }
}