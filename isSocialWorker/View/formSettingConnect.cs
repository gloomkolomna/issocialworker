﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using isSocialWorker.Properties;

namespace isSocialWorker.View
{
    public interface ISettingsForm
    {
        string Server { get; set; }
        string User { get; set; }
        string Password { get; set; }
        string Dbname { get; set; }

        event EventHandler SettingsSaveClick;
        event EventHandler SettingsOpenForm;
    }

    public partial class formSettingConnect : Form, ISettingsForm
    {
        public formSettingConnect()
        {
            InitializeComponent();
            butSave.Click += butSave_Click;
            butCancel.Click += butCancel_Click;
            this.Load += SettingsOpenForm_Load;
        }

        void butCancel_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        
        public string Server
        {
            get { return fldServer.Text; }
            set { fldServer.Text = value; }
        }

        public string User
        {
            get { return fldNameUser.Text; }
            set { fldNameUser.Text = value; }
        }

        public string Password
        {
            get { return fldPasswordUser.Text; }
            set { fldPasswordUser.Text = value; }
        }

        public string Dbname
        {
            get { return fldNameDb.Text; }
            set { fldNameDb.Text = value; }
        }

        public event EventHandler SettingsSaveClick;
        public event EventHandler SettingsOpenForm;

        private void butSave_Click(object sender, EventArgs e)
        {
            if(SettingsSaveClick != null) SettingsSaveClick(this,EventArgs.Empty);
        }

        private void SettingsOpenForm_Load(object sender, EventArgs e)
        {
            if (SettingsOpenForm != null) SettingsOpenForm(this, EventArgs.Empty);
        }

    }
}
