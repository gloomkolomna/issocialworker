﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.Model
{
    public interface INewClientAddressModel
    {
        List<DirectoryDefault> GetDirectory(string commandText);
        List<DirectoryDefault> GetDirectory(string commandText, int id);
        void SetBufferFields(int regionid, int cityid, int streetid, int houseid, int appartamentid, bool isrefugee, bool ishomeless);

        int GetRegionId();
        int GetCityId();
        int GetStreetId();
        int GetHouseId();
        int GetAppartamentId();
        bool GetIsRefugee();
        bool GetIsHomeless();
    }

    public class NewClientAddressModel : INewClientAddressModel
    {
        public List<DirectoryDefault> GetDirectory(string commandText)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public List<DirectoryDefault> GetDirectory(string commandText, int id)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;
            command.Parameters.AddWithValue("$id", id);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public void SetBufferFields(int regionid, int cityid, int streetid, int houseid, int appartamentid, bool isrefugee, bool ishomeless)
        {
            if (isrefugee || ishomeless)
            {
                ClientInfoAddress.AppartamentId = 0;
                ClientInfoAddress.CityId = 0;
                ClientInfoAddress.HouseId = 0;
                ClientInfoAddress.RegionId = 0;
                ClientInfoAddress.StreetId = 0;
                ClientInfoAddress.IsHomeless = ishomeless;
                ClientInfoAddress.IsRefugee = isrefugee;
            }
            else
            {
                ClientInfoAddress.AppartamentId = appartamentid;
                ClientInfoAddress.CityId = cityid;
                ClientInfoAddress.HouseId = houseid;
                ClientInfoAddress.RegionId = regionid;
                ClientInfoAddress.StreetId = streetid;
                ClientInfoAddress.IsHomeless = ishomeless;
                ClientInfoAddress.IsRefugee = isrefugee;
            }
        }

        public int GetRegionId()
        {
            return ClientInfoAddress.RegionId;
        }

        public int GetCityId()
        {
            return ClientInfoAddress.CityId;
        }

        public int GetStreetId()
        {
            return ClientInfoAddress.StreetId;
        }

        public int GetHouseId()
        {
            return ClientInfoAddress.HouseId;
        }

        public int GetAppartamentId()
        {
            return ClientInfoAddress.AppartamentId;
        }

        public bool GetIsRefugee()
        {
            return ClientInfoAddress.IsRefugee;
        }

        public bool GetIsHomeless()
        {
            return ClientInfoAddress.IsHomeless;
        }

    }
}
