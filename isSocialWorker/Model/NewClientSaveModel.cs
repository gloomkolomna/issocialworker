﻿using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.Model
{
    public interface INewClientSaveModel
    {
        void SaveClient(string lastname, string firstname, string middlename, string dateborn, string snils, string phone);
        int GetClientId();
        void SaveClientHead(int clientid, int departamentid, int socworkerid, int medicalworkerid, int statusid, int healthid, int conditionlivingid, string job);

        void SaveClientCategory(int clientid, List<DirectoryDefault> categorylist);

        void SaveClientAddress(int clientid, int regionid, int cityid, int streetid, int houseid, int appartamentid, bool isrefugee, bool ishomeless);
        void SaveClientHousing(int clientid, int typeHousingId, int conditionHousingId, int conditionGasId, int conditionWaterId, int conditionHeatingId, int conditionSanitaryId,
            double housingArea, int housingRoom, bool repairCosmetic, bool repairMajor);

        void SaveClientDocumentsPassport(int clientid, string serial, string number, string code, string date, string issued);
        void SaveClientDocumentsDisability(int clientid, string group, string degree, string serial, string number, string date, string act);
        void SaveClientCertificatePension(int clientid, string number, string date);
        void SaveClientCertificateDisabled(int clientid, string number, string date);
        void SaveClientCertificateMember(int clientid, string number, string date);
        void SaveClientCertificateVeteran(int clientid, string number, string date);
        void SaveClientCertificateMemberLabor(int clientid, string number, string date);

    }
    public class NewClientSaveModel : INewClientSaveModel
    {
        public void SaveClient(string lastname, string firstname, string middlename, string dateborn, string snils, string phone)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClient";
            command.Parameters.AddWithValue("$firstname", firstname);
            command.Parameters.AddWithValue("$lastname", lastname);
            command.Parameters.AddWithValue("$middlename", middlename);
            command.Parameters.Add("$dateborn", MySqlDbType.Date);
            command.Parameters["$dateborn"].Value = dateborn;
            command.Parameters.AddWithValue("$snils", snils);
            command.Parameters.AddWithValue("$phone", phone);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public int GetClientId()
        {
            int clientid = 0;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getClientId";

            try
            {
                connection.Open();
                clientid = (int)command.ExecuteScalar();
                
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
            return clientid;
        }

        public void SaveClientHead(int clientid, int departamentid, int socworkerid, int medicalworkerid, int statusid, int healthid, int conditionlivingid, string job)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientHead";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$departamentid", departamentid);
            command.Parameters.AddWithValue("$socworkerid", socworkerid);
            command.Parameters.AddWithValue("$medicalworkerid", medicalworkerid);
            command.Parameters.AddWithValue("$statusid", statusid);
            command.Parameters.AddWithValue("$healthid", healthid);
            command.Parameters.AddWithValue("$conditionlivingid", conditionlivingid);
            command.Parameters.AddWithValue("$job", job);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCategory(int clientid, List<DirectoryDefault> categorylist)
        {
            if(categorylist.Count > 0)
            {
                foreach (var categoryid in categorylist)
                {
                    MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
                    MySqlCommand command = new MySqlCommand();
                    command.Connection = connection;
                    command.CommandType = CommandType.StoredProcedure;
                    command.CommandText = "p_saveClientCategory";
                    command.Parameters.AddWithValue("$clientid", clientid);
                    command.Parameters.AddWithValue("$categoryid", categoryid.Id);
                    try
                    {
                        connection.Open();
                        command.ExecuteNonQuery();
                    }
                    catch (MySqlException ex)
                    {
                        logs.logger.Warn(" -> " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        public void SaveClientAddress(int clientid, int regionid, int cityid, int streetid, int houseid, int appartamentid, bool isrefugee, bool ishomeless)
        {
            string Address = String.Format("{0};{1};{2};{3};{4}", regionid, cityid, streetid, houseid, appartamentid);
            int IsRefugee = isrefugee ? 1 : 0;
            int IsHomeless = ishomeless ? 1 : 0;

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientAddress";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$address", Address);
            command.Parameters.AddWithValue("$isrefugee", IsRefugee);
            command.Parameters.AddWithValue("$ishomeless", IsHomeless);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientHousing(int clientid, int typeHousingId, int conditionHousingId, int conditionGasId, int conditionWaterId, int conditionHeatingId, 
            int conditionSanitaryId, double housingArea, int housingRoom, bool repairCosmetic, bool repairMajor)
        {
            int RepairCosmetic = repairCosmetic ? 1 : 0;
            int RepairMajor = repairCosmetic ? 1 : 0;

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientHousing";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$typehousingId", typeHousingId);
            command.Parameters.AddWithValue("$conditionhousingId", conditionHousingId);
            command.Parameters.AddWithValue("$conditiongasId", conditionGasId);
            command.Parameters.AddWithValue("$conditionwaterId", conditionWaterId);
            command.Parameters.AddWithValue("$conditionheatingId", conditionHeatingId);
            command.Parameters.AddWithValue("$conditionsanitaryId", conditionSanitaryId);
            command.Parameters.AddWithValue("$housingarea", housingArea);
            command.Parameters.AddWithValue("$housingroom", housingRoom);
            command.Parameters.AddWithValue("$repaircosmetic", RepairCosmetic);
            command.Parameters.AddWithValue("$repairmajor", RepairMajor);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientDocumentsPassport(int clientid, string serial, string number, string code, string date, string issued)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_passport";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$serial", serial);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.AddWithValue("$code", code);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;
            command.Parameters.AddWithValue("$issued", issued);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientDocumentsDisability(int clientid, string group, string degree, string serial, string number, string date, string act)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_disability";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$groupinv", group);
            command.Parameters.AddWithValue("$degree", degree);
            command.Parameters.AddWithValue("$serial", serial);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;
            command.Parameters.AddWithValue("$act", act);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCertificatePension(int clientid, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_certificatepension";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCertificateDisabled(int clientid, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_certificatedisabled";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCertificateMember(int clientid, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_certificatemember";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCertificateVeteran(int clientid, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_certificateveteran";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }

        public void SaveClientCertificateMemberLabor(int clientid, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_saveClientDocuments_certificatememberlabor";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
