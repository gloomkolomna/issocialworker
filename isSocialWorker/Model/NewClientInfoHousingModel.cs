﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.Model
{
    public interface INewClientInfoHousingModel
    {
        List<DirectoryDefault> GetDirectory(string commandText);

        void SetInfoHousingBuffered(int typeHousingId, int conditionHousingId, int conditionGasId, int conditionWaterId, int conditionHeatingId, int conditionSanitaryId,
            double housingArea, int housingRoom, bool repairCosmetic, bool repairMajor);

        int GetTypeHousingId();
        int GetConditionHousingId();
        int GetConditionGasId();
        int GetConditionWaterId();
        int GetConditionHeatingId();
        int GetConditionSanitaryId();

        double GetHousingArea();
        int GetHousingRoom();
        bool GetRepairCosmetic();
        bool GetRepairMajor();
    }
    public class NewClientInfoHousingModel : INewClientInfoHousingModel
    {
        public List<DirectoryDefault> GetDirectory(string commandText)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public void SetInfoHousingBuffered(int typeHousingId, int conditionHousingId, int conditionGasId, int conditionWaterId, int conditionHeatingId, int conditionSanitaryId,
            double housingArea, int housingRoom, bool repairCosmetic, bool repairMajor)
        {
            ClientInfoHousing.TypeHousingId = typeHousingId;
            ClientInfoHousing.ConditionHousingId = conditionHousingId;
            ClientInfoHousing.ConditionGasId = conditionGasId;
            ClientInfoHousing.ConditionWaterId = conditionWaterId;
            ClientInfoHousing.ConditionHeatingId = conditionHeatingId;
            ClientInfoHousing.ConditionSanitaryId = conditionSanitaryId;
            ClientInfoHousing.HousingArea = housingArea;
            ClientInfoHousing.HousingRoom = housingRoom;
            ClientInfoHousing.RepairCosmetic = repairCosmetic;
            ClientInfoHousing.RepairMajor = repairMajor;
        }

        public int GetTypeHousingId()
        {
            return ClientInfoHousing.TypeHousingId;
        }

        public int GetConditionHousingId()
        {
            return ClientInfoHousing.ConditionHousingId;
        }

        public int GetConditionGasId()
        {
            return ClientInfoHousing.ConditionGasId;
        }

        public int GetConditionWaterId()
        {
            return ClientInfoHousing.ConditionWaterId;
        }

        public int GetConditionHeatingId()
        {
            return ClientInfoHousing.ConditionHeatingId;
        }

        public int GetConditionSanitaryId()
        {
            return ClientInfoHousing.ConditionSanitaryId;
        }

        public double GetHousingArea()
        {
            return ClientInfoHousing.HousingArea;
        }

        public int GetHousingRoom()
        {
            return ClientInfoHousing.HousingRoom;
        }

        public bool GetRepairCosmetic()
        {
            return ClientInfoHousing.RepairCosmetic;
        }

        public bool GetRepairMajor()
        {
            return ClientInfoHousing.RepairMajor;
        }
    }
}
