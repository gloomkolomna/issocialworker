﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.Model
{
    public interface INewClientInfoHeadModel
    {
        List<DirectoryDefault> GetDirectory(string commandText);
        List<DirectoryDefault> GetCategoryList();
        void SetClientHeadInfoBuffer(int departamentid, int socworkerid, int medicalworkerid, int statusid, int healthid, int conditionlivingid, string job);
        void AddCategoryToList(int Id, string Name);
        bool IsCategory(int catId);
        void RemoveCategory(int id);

        int GetDepartamentId();
        int GetSocialWorkerId();
        int GetMedicalWorkerId();
        int GetStatusId();
        int GetHealthId();
        int GetConditionLivingId();
        string GetJob();
    }

    public class NewClientInfoHeadModel : INewClientInfoHeadModel
    {
        public List<DirectoryDefault> GetDirectory(string commandText)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();
            
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while(dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public void SetClientHeadInfoBuffer(int departamentid, int socworkerid, int medicalworkerid, int statusid, int healthid, int conditionlivingid, string job)
        {
            ClientInfoHead.DepartamentId = departamentid;
            ClientInfoHead.SocialWorkerId = socworkerid;
            ClientInfoHead.MedicalWorkerId = medicalworkerid;
            ClientInfoHead.StatusId = statusid;
            ClientInfoHead.HealthId = healthid;
            ClientInfoHead.ConditionLivingId = conditionlivingid;
            ClientInfoHead.Job = job;
        }

        public void AddCategoryToList(int id, string name)
        {
            ClientInfoHead.CategoryList.Add(new DirectoryDefault()
                {
                    Id = id,
                    Name = name
                });
        }

        public void RemoveCategory(int id)
        {
            ClientInfoHead.CategoryList.RemoveAll(r => r.Id == id);
        }

        public List<DirectoryDefault> GetCategoryList()
        {
            return ClientInfoHead.CategoryList;
        }

        public bool IsCategory(int catId)
        {
            bool isCategory = ClientInfoHead.CategoryList.Any(a => a.Id == catId);
            return isCategory;
        }

        #region Getters Ids
        public int GetDepartamentId()
        {
            return ClientInfoHead.DepartamentId;
        }

        public int GetSocialWorkerId()
        {
            return ClientInfoHead.SocialWorkerId;
        }

        public int GetMedicalWorkerId()
        {
            return ClientInfoHead.MedicalWorkerId;
        }

        public int GetStatusId()
        {
            return ClientInfoHead.StatusId;
        }

        public int GetHealthId()
        {
            return ClientInfoHead.HealthId;
        }

        public int GetConditionLivingId()
        {
            return ClientInfoHead.ConditionLivingId;
        }

        public string GetJob()
        {
            return ClientInfoHead.Job;
        }
        #endregion
    }
}
