﻿using isSocialWorker.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Model
{
    public interface INewClientDocumentsModel
    {
        void SetBufferPassport(string serial, string number, string code, string date, string issued);
        void SetBufferDisability(string group, string degree, string serial, string number, string date, string act);
        void SetBufferCertificatePension(string number, string date);
        void SetBufferCertificateDisabled(string number, string date);
        void SetBufferCertificateMember(string number, string date);
        void SetBufferCertificateVeteran(string number, string date);
        void SetBufferCertificateMemberLabor(string number, string date);

        string GetPassportSerial();
        string GetPassportNumber();
        string GetPassportCode();
        string GetPassportDate();
        string GetPassportIssued();

        string GetDisabilityGroup();
        string GetDisabilityDegree();
        string GetDisabilitySerial();
        string GetDisabilityNumber();
        string GetDisabilityDate();
        string GetDisabilityAct();

        string GetCertificatePensionNumber();
        string GetCertificatePensionDate();
        string GetCertificateDisabledNumber();
        string GetCertificateDisabledDate();
        string GetCertificateMemberNumber();
        string GetCertificateMemberDate();
        string GetCertificateVeteranNumber();
        string GetCertificateVeteranDate();
        string GetCertificateMemberLaborNumber();
        string GetCertificateMemberLaborDate();
    }
    public class NewClientDocumentsModel : INewClientDocumentsModel
    {
        public void SetBufferPassport(string serial, string number, string code, string date, string issued)
        {
            ClientDocuments.PassportSerial = serial;
            ClientDocuments.PassportNumber = number;
            ClientDocuments.PassportCode = code;
            ClientDocuments.PassportDate = date;
            ClientDocuments.PassportIssued = issued;
        }

        public void SetBufferDisability(string group, string degree, string serial, string number, string date, string act)
        {
            ClientDocuments.DisabilityAct = act;
            ClientDocuments.DisabilityDate = date;
            ClientDocuments.DisabilityDegree = degree;
            ClientDocuments.DisabilityGroup = group;
            ClientDocuments.DisabilityNumber = number;
            ClientDocuments.DisabilitySerial = serial;
        }

        public void SetBufferCertificatePension(string number, string date)
        {
            ClientDocuments.CertificatePensionDate = date;
            ClientDocuments.CertificatePensionNumber = number;
        }

        public void SetBufferCertificateDisabled(string number, string date)
        {
            ClientDocuments.CertificateDisabledDate = date;
            ClientDocuments.CertificateDisabledNumber = number;
        }

        public void SetBufferCertificateMember(string number, string date)
        {
            ClientDocuments.CertificateMemberDate = date;
            ClientDocuments.CertificateMemberNumber = number;
        }

        public void SetBufferCertificateVeteran(string number, string date)
        {
            ClientDocuments.CertificateVeteranDate = date;
            ClientDocuments.CertificateVeteranNumber = number;
        }

        public void SetBufferCertificateMemberLabor(string number, string date)
        {
            ClientDocuments.CertificateMemberLaborDate = date;
            ClientDocuments.CertificateMemberLaborNumber = number;
        }

        public string GetPassportSerial()
        {
            return ClientDocuments.PassportSerial;
        }

        public string GetPassportNumber()
        {
            return ClientDocuments.PassportNumber;
        }

        public string GetPassportCode()
        {
            return ClientDocuments.PassportCode;
        }

        public string GetPassportDate()
        {
            return ClientDocuments.PassportDate;
        }

        public string GetPassportIssued()
        {
            return ClientDocuments.PassportIssued;
        }

        public string GetDisabilityGroup()
        {
            return ClientDocuments.DisabilityGroup;
        }

        public string GetDisabilityDegree()
        {
            return ClientDocuments.DisabilityDegree;
        }

        public string GetDisabilitySerial()
        {
            return ClientDocuments.DisabilitySerial;
        }

        public string GetDisabilityNumber()
        {
            return ClientDocuments.DisabilityNumber;
        }

        public string GetDisabilityDate()
        {
            return ClientDocuments.DisabilityDate;
        }

        public string GetDisabilityAct()
        {
            return ClientDocuments.DisabilityAct;
        }

        public string GetCertificatePensionNumber()
        {
            return ClientDocuments.CertificatePensionNumber;
        }

        public string GetCertificatePensionDate()
        {
            return ClientDocuments.CertificatePensionDate;
        }

        public string GetCertificateDisabledNumber()
        {
            return ClientDocuments.CertificateDisabledNumber;
        }

        public string GetCertificateDisabledDate()
        {
            return ClientDocuments.CertificateDisabledDate;
        }

        public string GetCertificateMemberNumber()
        {
            return ClientDocuments.CertificateMemberNumber;
        }

        public string GetCertificateMemberDate()
        {
            return ClientDocuments.CertificateMemberDate;
        }

        public string GetCertificateVeteranNumber()
        {
            return ClientDocuments.CertificateVeteranNumber;
        }

        public string GetCertificateVeteranDate()
        {
            return ClientDocuments.CertificateVeteranDate;
        }

        public string GetCertificateMemberLaborNumber()
        {
            return ClientDocuments.CertificateMemberLaborNumber;
        }

        public string GetCertificateMemberLaborDate()
        {
            return ClientDocuments.CertificateMemberLaborDate;
        }
    }
}
