﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using isSocialWorker.Properties;

namespace isSocialWorker.Model
{
    public interface ISettingSystemModel
    {
        void SavePath(string pathToPhoto);
        string GetPath();
    }

    public class SettingSystemModel : ISettingSystemModel
    {
        public void SavePath(string pathToPhoto)
        {
            Settings.Default.pathToPhoto = pathToPhoto;
            Settings.Default.Save();
        }


        public string GetPath()
        {
            return Settings.Default.pathToPhoto;
        }
    }
}
