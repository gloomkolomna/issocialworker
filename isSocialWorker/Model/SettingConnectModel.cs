﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using isSocialWorker.Properties;

namespace isSocialWorker.Model
{
    public interface ISettingConnectModel
    {
        void SaveConnect(string server, string user, string password, string dbname);
        string GetServer();
        string GetUser();
        string GetPassword();
        string GetDbname();
    }

    public class SettingConnectModel : ISettingConnectModel
    {
        public void SaveConnect(string server, string user, string password, string dbname)
        {
            Settings.Default.server = server;
            Settings.Default.user = user;
            Settings.Default.password = password;
            Settings.Default.bdname = dbname;
            Settings.Default.Save();
        }

        public string GetServer()
        {
            string server = Settings.Default.server;
            return server;
        }

        public string GetUser()
        {
            string user = Settings.Default.user;
            return user;
        }

        public string GetPassword()
        {
            string password = Settings.Default.password;
            return password;
        }

        public string GetDbname()
        {
            string bdname = Settings.Default.bdname;
            return bdname;
        }
    }
}
