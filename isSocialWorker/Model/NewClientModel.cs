﻿using isSocialWorker.Client;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace isSocialWorker.Model
{
    public interface INewClientModel
    {
        bool IsExistClient(string firstname, string lastname, string middlename, string dateborn);
        bool IsEmptyFields(string firstname, string lastname, string middlename, string snils);
        bool IsExistSnils(string snils);
        void SetClientBuffer(string firstname, string lastname, string middlename, string dateborn, string snils, string phone);
        string GetFirstName();
        string GetLastName();
        string GetMiddleName();
        string GetDateBorn();
        string GetSnils();
        string GetPhone();
    }

    public class NewClientModel : INewClientModel
    {

        /// <summary>
        /// Метод проверяет наличие клиента в базе данных по ФИО и дате рождения
        /// </summary>
        /// <param name="firstname">Имя</param>
        /// <param name="lastname">Фамилия</param>
        /// <param name="middlename">Отчество</param>
        /// <param name="dateborn">Дата рождения</param>
        /// <returns></returns>
        public bool IsExistClient(string firstname, string lastname, string middlename, string dateborn)
        {
            bool isExistClient = false;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_existClient";
            command.Parameters.AddWithValue("$firstname", firstname);
            command.Parameters.AddWithValue("$lastname", lastname);
            command.Parameters.AddWithValue("$middlename", middlename);
            command.Parameters.Add("$dateborn", MySqlDbType.Date);
            command.Parameters["$dateborn"].Value = dateborn;

            try
            {
                connection.Open();
                int query = Convert.ToInt32(command.ExecuteScalar());
                isExistClient = (query > 0) ? true : false;
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return isExistClient;
        }

        /// <summary>
        /// Метод проверяет наличие СНИЛС в базе данных
        /// </summary>
        /// <param name="snils">СНИЛС</param>
        /// <returns></returns>
        public bool IsExistSnils(string snils)
        {
            bool isExistSnils = false;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_existSnils";
            command.Parameters.AddWithValue("$snils", snils);

            try
            {
                connection.Open();
                int query = Convert.ToInt32(command.ExecuteScalar());
                isExistSnils = (query > 0) ? true : false;
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return isExistSnils;
        }

        /// <summary>
        /// Метод проверяет наличие незаполненных обязательных полей
        /// </summary>
        /// <param name="firstname">Имя</param>
        /// <param name="lastname">Фамилия</param>
        /// <param name="middlename">Отчество</param>
        /// <param name="snils">Снилс</param>
        /// <returns>true - если все заполнено, false - если хотя бы одно поле пустое</returns>
        public bool IsEmptyFields(string firstname, string lastname, string middlename, string snils)
        {
            bool isEmptyFields = false;
            if (String.IsNullOrEmpty(firstname) || String.IsNullOrEmpty(lastname) || String.IsNullOrEmpty(middlename) || snils.Equals("   -   -    "))
                isEmptyFields = true;
            else
                isEmptyFields = false;

            return isEmptyFields;
        }
        
        public void SetClientBuffer(string firstname, string lastname, string middlename, string dateborn, string snils, string phone)
        {
            ClientInfo.FirstName = firstname;
            ClientInfo.LastName = lastname;
            ClientInfo.MiddleName = middlename;
            ClientInfo.DateBorn = dateborn;
            ClientInfo.Snils = snils;
            ClientInfo.Phone = phone;
        }

        public string GetFirstName()
        {
            return ClientInfo.FirstName;
        }

        public string GetLastName()
        {
            return ClientInfo.LastName;
        }

        public string GetMiddleName()
        {
            return ClientInfo.MiddleName;
        }

        public string GetDateBorn()
        {
            return ClientInfo.DateBorn;
        }

        public string GetSnils()
        {
            return ClientInfo.Snils;
        }

        public string GetPhone()
        {
            return ClientInfo.Phone;
        }
    }
}
