﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using isSocialWorker.Properties;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace isSocialWorker.Model
{
    public class AutorizationModel
    {
        public static List<Users> GetUserList()
        {
            List<Users> userList = new List<Users>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getUsers";

            try
            {
                connection.Open();
                MySqlDataReader dataReader = command.ExecuteReader();
                while(dataReader.Read())
                {
                    userList.Add(new Users()
                        {
                            Username = dataReader["username"].ToString(),
                            Password = dataReader["password"].ToString(),
                            Access = Int32.Parse(dataReader["access"].ToString())
                        });
                }

                return userList;
            }
            catch(Exception ex)
            {
                MessageService msg = new MessageService();
                msg.ShowError(ex.Message);
                return null;
            }
            finally
            {
                connection.Close();
            }
        }

        public static bool isCheck(string username, string password, List<Users> userList)
        {
            bool check = userList.Where(x => x.Username == username).Where(x => x.Password == password).Any();
            return check;
        }
    }

    
}
