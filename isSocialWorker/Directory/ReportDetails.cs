﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Directory
{
    public class ReportDetails
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public int ServiceCount { get; set; }
        public double ServiceSumm { get; set; }
        public double ClientSumm { get; set; }
    }
}
