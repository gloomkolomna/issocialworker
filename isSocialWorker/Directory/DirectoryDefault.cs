﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Directory
{
    public class DirectoryDefault
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
