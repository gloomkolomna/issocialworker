﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Directory
{
    public class Report
    {
        public double SummTotal { get; set; }
        public double SummPay { get; set; }
        public string PaymentTerms { get; set; }
    }
}
