﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker
{
    public class Users
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int Access { get; set; }
    }
}
