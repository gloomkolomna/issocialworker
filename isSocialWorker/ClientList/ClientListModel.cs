﻿using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientList
{
    public interface IClientListModel
    {
        List<DirectoryDefault> GetDirectory(string commandText);
        List<ClientFields> GetClientList();
        void ExportToExcel(List<ClientFields> clientList);
    }
    public class ClientListModel : IClientListModel
    {
        public List<DirectoryDefault> GetDirectory(string commandText)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public List<ClientFields> GetClientList()
        {
            List<ClientFields> list = new List<ClientFields>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getClientList";

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while(dataReader.Read())
                {
                    list.Add(
                        new ClientFields()
                        {
                            Id = Int32.Parse(dataReader["id"].ToString()),
                            Lastname = dataReader["lastname"].ToString(),
                            Firstname = dataReader["firstname"].ToString(),
                            Middlename = dataReader["middlename"].ToString(),
                            Dateborn = Convert.ToDateTime(dataReader["dateborn"].ToString()),
                            Snils = dataReader["snils"].ToString(),
                            Phone = dataReader["phone"].ToString(),

                            IsDead = (Int32.Parse(dataReader["isDead"].ToString()) == 0) ? false : true,
                            IsDisposal = (Int32.Parse(dataReader["isDisposal"].ToString()) == 0) ? false : true,
                            IsSuspend = (Int32.Parse(dataReader["isSuspend"].ToString()) == 0) ? false : true,

                            DepartamentId = Int32.Parse(dataReader["departamentId"].ToString()),
                            MedicalworkerID = Int32.Parse(dataReader["medicalId"].ToString()),
                            SocialworkerId = Int32.Parse(dataReader["socworkerId"].ToString()),
                            StatusId = Int32.Parse(dataReader["statusId"].ToString())
                        }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            /*command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;*/
            command.CommandText = "p_getClientListCategory";
            dataReader = null;

            try
            {
                connection.Open();
                int countlist = list.Count;
                for (int i = 0; i < countlist; i++)
                {
                    command.Parameters.AddWithValue("$clientid", list[i].Id);
                    dataReader = command.ExecuteReader();
                    command.Parameters.Clear();

                    while(dataReader.Read())
                    {
                        list[i].Categorys.Add(
                            new ClientCategory()
                            {
                                CategoryId = Int32.Parse(dataReader["category"].ToString())
                            }
                            );
                    }

                    dataReader.Close();
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public void ExportToExcel(List<ClientFields> clientList)
        {
            Microsoft.Office.Interop.Excel.Application ExcelApp = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook ExcelWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet ExcelWorkSheet;
            //Книга.
            ExcelWorkBook = ExcelApp.Workbooks.Add(System.Reflection.Missing.Value);
            //Таблица.
            ExcelWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)ExcelWorkBook.Worksheets.get_Item(1);

            ExcelApp.Cells[1, 1] = "№ п/п";
            ExcelApp.Cells[1, 2] = "Фамилия";
            ExcelApp.Cells[1, 3] = "Имя";
            ExcelApp.Cells[1, 4] = "Отчество";
            ExcelApp.Cells[1, 5] = "Дата рождения";
            ExcelApp.Cells[1, 6] = "СНИЛС";
            ExcelApp.Cells[1, 7] = "Телефон";

            int clientCount = clientList.Count;
            for (int i = 0; i < clientCount; i++)
            {
                ExcelApp.Cells[i + 2, 1] = i + 1;
                ExcelApp.Cells[i + 2, 2] = clientList[i].Lastname;
                ExcelApp.Cells[i + 2, 3] = clientList[i].Firstname;
                ExcelApp.Cells[i + 2, 4] = clientList[i].Middlename;
                ExcelApp.Cells[i + 2, 5] = clientList[i].Dateborn;
                ExcelApp.Cells[i + 2, 6] = clientList[i].Snils;
                ExcelApp.Cells[i + 2, 7] = clientList[i].Phone;
            }

            ExcelApp.Columns.AutoFit();
            //Вызываем нашу созданную эксельку.
            ExcelApp.Visible = true;
            ExcelApp.UserControl = true;
        }
    }
}
