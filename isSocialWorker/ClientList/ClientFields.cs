﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientList
{
    public class ClientFields
    {
        public int Id { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Snils { get; set; }
        public DateTime Dateborn { get; set; }
        public string Phone { get; set; }

        public bool IsDead { get; set; }
        public bool IsDisposal { get; set; }
        public bool IsSuspend { get; set; }

        public int DepartamentId { get; set; }
        public int SocialworkerId { get; set; }
        public int MedicalworkerID { get; set; }
        public int StatusId { get; set; }

        public List<ClientCategory> Categorys = new List<ClientCategory>();
    }
}
