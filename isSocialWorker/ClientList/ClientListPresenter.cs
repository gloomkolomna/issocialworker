﻿using isSocialWorker.ClientAnketa;
using isSocialWorker.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace isSocialWorker.ClientList
{
    public class ClientListPresenter
    {
        private readonly IFormClientList _view;
        private readonly IClientListModel _model;
        private readonly IMessageService _message;

        public ClientListPresenter(IFormClientList view, IClientListModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.LoadForm += _view_LoadForm;
            _view.ExportToExcelClick += _view_ExportToExcelClick;
            _view.ClickToClient += _view_ClickToClient;
            _view.ButtonRefreshClick += _view_ButtonRefreshClick;
        }

        void _view_ButtonRefreshClick(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            LoadListInfo();
            Cursor.Current = Cursors.Default;
        }

        void _view_ClickToClient(object sender, EventArgs e)
        {
            formClientAnketa form = new formClientAnketa();
            AnketaModel model = new AnketaModel();
            MessageService message = new MessageService();
            AnketaUpdater updater = new AnketaUpdater();
            ReportModel report = new ReportModel();
            AnketaPresenter presenter = new AnketaPresenter(_view.ClientId, form, model, message, updater, report);

            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_ExportToExcelClick(object sender, EventArgs e)
        {
            Thread exportExcel = new Thread(ExportExcel);
            exportExcel.Start();
        }

        private void ExportExcel()
        {
            _model.ExportToExcel(_view.ClientList);
        }

        void _view_LoadForm(object sender, EventArgs e)
        {
            LoadListInfo();
        }

        private void LoadListInfo()
        {
            _view.ClientList = _model.GetClientList();

            _view.Lists = _model.GetDirectory("p_getDirectory_category");
            _view.CategoryList();
            _view.Lists = _model.GetDirectory("p_getDirectory_departaments");
            _view.DepartamentList();
            _view.Lists = _model.GetDirectory("p_getDirectory_socialworker");
            _view.SocialWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_medicalworker");
            _view.MedicalWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_status");
            _view.StatusList();
        }
    }
}
