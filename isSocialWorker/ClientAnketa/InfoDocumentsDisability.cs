﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoDocumentsDisability
    {
        public string Group { get; set; }
        public string Degree { get; set; }
        public string Serial { get; set; }
        public string Number { get; set; }
        public string Date { get; set; }
        public string Act { get; set; }
    }
}
