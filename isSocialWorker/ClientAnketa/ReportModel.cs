﻿using isSocialWorker.Directory;
using Microsoft.Office.Interop.Excel;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class ReportModel : IReportModel
    {
        public List<ReportDetails> GetReportClient(int clientid, int report_month, int report_year, bool report_type)
        {
            List<ReportDetails> list = new List<ReportDetails>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            if(report_type)
                command.CommandText = "p_getReportClient_guaranteed";
            else
                command.CommandText = "p_getReportClient_additional";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new ReportDetails()
                    {
                        ServiceSumm = Double.Parse(dataReader["summ_service"].ToString()),
                        ServiceCount = Int32.Parse(dataReader["count"].ToString()),
                        ServiceId = Int32.Parse(dataReader["serviceid"].ToString()),
                        ServiceName = dataReader["name"].ToString(),
                        ClientSumm = Double.Parse(dataReader["summ_client"].ToString())
                    }
                    );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public string GetPaymentTerms(int clientid, int report_month, int report_year)
        {
            string paymentTerms = "";

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getReportClient_paymentsterm";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                paymentTerms = (string)command.ExecuteScalar();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return paymentTerms;
        }

        public double GetSummTotal(int clientid, int report_month, int report_year)
        {
            double summ = 0;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getReportClient_summtotal";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                summ = Double.Parse(command.ExecuteScalar().ToString());
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return summ;
        }

        public double GetSummPay(int clientid, int report_month, int report_year, bool type)
        {
            double summ = 0;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            if (type)
                command.CommandText = "p_getReportClient_summpay";
            else
                command.CommandText = "p_getReportClient_summ_additional";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                summ = Double.Parse(command.ExecuteScalar().ToString());
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return summ;
        }

        public string GetWorkerName(int clientid, int report_month, int report_year, bool type)
        {
            string result = "";
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            if(type)
                command.CommandText = "p_getReportClient_workername";
            else
                command.CommandText = "p_getReportClient_workername_additional";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                result = (string)command.ExecuteScalar();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return result;
        }

        public string GetDepartamentName(int clientid, int report_month, int report_year, bool type)
        {
            string result = "";
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            if (type)
                command.CommandText = "p_getReportClient_departamentname";
            else
                command.CommandText = "p_getReportClient_departamentname_additional";

            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$report_month", report_month);
            command.Parameters.AddWithValue("$report_year", report_year);

            try
            {
                connection.Open();
                result = (string)command.ExecuteScalar();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return result;
        }

        #region Report Print
        public double CheckVersionExcel()
        {
            double _versionExcel;
            Microsoft.Office.Interop.Excel.Application excelapp = new Microsoft.Office.Interop.Excel.Application();

            System.Globalization.NumberFormatInfo provider = new System.Globalization.NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            _versionExcel = Convert.ToDouble(excelapp.Version, provider);

            return _versionExcel;
        }

        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch
            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        public void ReportExcel(double _versionExcel, List<ReportDetails> list, string fio, string workername, string departamentname,
            string paymentTerm, double summTotal, double summPay, string date, bool type)
        {
            Microsoft.Office.Interop.Excel.Application xl = null;
            Microsoft.Office.Interop.Excel._Workbook wBook = null;
            Microsoft.Office.Interop.Excel._Worksheet wSheet = null;

            object m_objOpt = System.Reflection.Missing.Value;

            string patchto = System.Windows.Forms.Application.StartupPath + "\\templates\\";
            string excelTemplatePath = "";
            if (type)
                excelTemplatePath = patchto + "report_client_guaranteed.xls";
            else
                excelTemplatePath = patchto + "report_client_additional.xls";

            try
            {
                xl = new Microsoft.Office.Interop.Excel.Application();
                wBook = (Microsoft.Office.Interop.Excel._Workbook)xl.Workbooks.Open(excelTemplatePath, false, false, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt);
                wSheet = (Microsoft.Office.Interop.Excel._Worksheet)wBook.ActiveSheet;

                int iRowCount = 0;
                if (type)
                    iRowCount = 17;
                else
                    iRowCount = 15;

                int rowCountToExcel = 1;

                int count = list.Count;
                for (int i = 0; i < count; i++ )
                {
                    wSheet.Cells[iRowCount, 1] = rowCountToExcel.ToString();
                    wSheet.Cells[iRowCount, 2] = list[i].ServiceName;
                    wSheet.Cells[iRowCount, 3] = list[i].ServiceCount;
                    if (type)
                        wSheet.Cells[iRowCount, 4] = list[i].ClientSumm;
                    else
                        wSheet.Cells[iRowCount, 4] = list[i].ServiceSumm;

                    var styleRange = wSheet.get_Range("A" + iRowCount, "D" + iRowCount);
                    styleRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    styleRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    if (_versionExcel > 11.0)
                    {
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    }
                    var wrapText = wSheet.get_Range("B" + iRowCount);
                    wrapText.WrapText = true;
                    rowCountToExcel++;
                    iRowCount++;
                    var cellsDRnr = wSheet.get_Range("A" + iRowCount, "A" + iRowCount);
                    cellsDRnr.EntireRow.Insert(-4121, m_objOpt);
                }

                wSheet.Cells.Replace("$CLIENT_NAME$", fio, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                if (type)
                {
                    wSheet.Cells.Replace("$SERV_PAY$", paymentTerm, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                    wSheet.Cells.Replace("$SUMM_ITOGO$", summPay, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                }
                wSheet.Cells.Replace("$DATE$", date, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                
                wSheet.Cells.Replace("$DEP_NAME$; $SOC_NAME$", departamentname + "; " + workername, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                if (type)
                    wSheet.Cells.Replace("$SUMM_VSEGO$", summTotal, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                else
                    wSheet.Cells.Replace("$SUMM_VSEGO$", summPay, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                
                //wBook.Close(true, excelTemplatePath, m_objOpt);
                Object oSaveAsFileExcel1 = patchto + "created\\" + fio + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".xls";
                wBook.SaveAs(oSaveAsFileExcel1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, true, Type.Missing, Type.Missing, Type.Missing);
                wSheet = (Microsoft.Office.Interop.Excel._Worksheet)wBook.Worksheets.get_Item(1);
                xl.Visible = true;
                xl.UserControl = true;
                wBook = null;
            }
            catch (Exception ex)
            {
                logs.logger.Warn(" -> Report error: " + ex.Message);
                releaseObject(wSheet);
                releaseObject(wBook);
                releaseObject(xl);
                xl.Quit();
                wBook = null;
                wSheet = null;
                xl = null;
            }
            finally
            {
                releaseObject(wSheet);
                releaseObject(wBook);
                releaseObject(xl);
                wBook = null;
                wSheet = null;
                xl = null;
            }
        }

        public void ActExcel(double _versionExcel, List<ReportDetails> list, string fio, string workername, double summTotal, double summPay, string date)
        {
            Microsoft.Office.Interop.Excel.Application xl = null;
            Microsoft.Office.Interop.Excel._Workbook wBook = null;
            Microsoft.Office.Interop.Excel._Worksheet wSheet = null;

            object m_objOpt = System.Reflection.Missing.Value;

            string patchto = System.Windows.Forms.Application.StartupPath + "\\templates\\";
            string excelTemplatePath = patchto + "act_client.xls";

            try
            {
                xl = new Microsoft.Office.Interop.Excel.Application();
                wBook = (Microsoft.Office.Interop.Excel._Workbook)xl.Workbooks.Open(excelTemplatePath, false, false, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt, m_objOpt);
                wSheet = (Microsoft.Office.Interop.Excel._Worksheet)wBook.ActiveSheet;

                int iRowCount = 13;

                int rowCountToExcel = 1;

                int count = list.Count;
                for (int i = 0; i < count; i++)
                {
                    wSheet.Cells[iRowCount, 1] = rowCountToExcel.ToString();
                    wSheet.Cells[iRowCount, 2] = list[i].ServiceName;
                    wSheet.Cells[iRowCount, 3] = list[i].ClientSumm;

                    var styleRange = wSheet.get_Range("A" + iRowCount, "C" + iRowCount);
                    styleRange.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    styleRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    if (_versionExcel > 11.0)
                    {
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        styleRange.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    }
                    var wrapText = wSheet.get_Range("B" + iRowCount);
                    wrapText.WrapText = true;
                    rowCountToExcel++;
                    iRowCount++;
                    var cellsDRnr = wSheet.get_Range("A" + iRowCount, "A" + iRowCount);
                    cellsDRnr.EntireRow.Insert(-4121, m_objOpt);
                }

                wSheet.Cells.Replace("$CLIENT_NAME$", fio, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                wSheet.Cells.Replace("$SUMM_ITOGO$", summPay, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                wSheet.Cells.Replace("$DATE$", date, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                wSheet.Cells.Replace("$SOC_NAME$", workername, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);
                wSheet.Cells.Replace("$SUMM_VSEGO$", summTotal, Microsoft.Office.Interop.Excel.XlLookAt.xlWhole, Microsoft.Office.Interop.Excel.XlSearchOrder.xlByColumns, false, Type.Missing, false, false);

                //wBook.Close(true, excelTemplatePath, m_objOpt);
                Object oSaveAsFileExcel1 = patchto + "created\\" + fio + "_" + DateTime.Now.ToString("yyyy_MM_dd_HH_mm_ss") + ".xls";
                wBook.SaveAs(oSaveAsFileExcel1, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, XlSaveAsAccessMode.xlNoChange, XlSaveConflictResolution.xlLocalSessionChanges, true, Type.Missing, Type.Missing, Type.Missing);
                wSheet = (Microsoft.Office.Interop.Excel._Worksheet)wBook.Worksheets.get_Item(1);
                xl.Visible = true;
                xl.UserControl = true;
                wBook = null;
            }
            catch (Exception ex)
            {
                logs.logger.Warn(" -> Report error: " + ex.Message);
                releaseObject(wSheet);
                releaseObject(wBook);
                releaseObject(xl);
                xl.Quit();
                wBook = null;
                wSheet = null;
                xl = null;
            }
            finally
            {
                releaseObject(wSheet);
                releaseObject(wBook);
                releaseObject(xl);
                wBook = null;
                wSheet = null;
                xl = null;
            }
        }

        #endregion

    }
}
