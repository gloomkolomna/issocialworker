﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public interface IAnketaModel
    {
        List<DirectoryDefault> GetDirectory(string commandText);
        List<DirectoryDefault> GetDirectory(string commandText, int id);
        List<DirectoryDefault> GetCategoryList(int clientid);
        List<InfoClient> GetClientInfo(int clientid);
        List<InfoHead> GetHeadInfo(int clientid);
        List<InfoAddress> GetAddressInfo(int clientid);
        List<InfoHousing> GetHousingInfo(int clientid);
        List<InfoDocumentsPassport> GetDocumentsPassport(int clientid);
        List<InfoDocumentsDisability> GetDocumentsDisability(int clientid);
        List<InfoDocumentsBasic> GetDocumentsBasic(string commandtext, int clientid);

        string GetFirstName(List<InfoClient> list);
        string GetLastName(List<InfoClient> list);
        string GetMiddleName(List<InfoClient> list);
        string GetDateBorn(List<InfoClient> list);
        string GetSnils(List<InfoClient> list);
        string GetPhone(List<InfoClient> list);

        bool GetIsDead(List<InfoClient> list);
        bool GetIsDisposal(List<InfoClient> list);
        bool GetIsSuspend(List<InfoClient> list);

        int GetDepartamentId(List<InfoHead> list);
        int GetSocialworkerId(List<InfoHead> list);
        int GetMedicalworkerId(List<InfoHead> list);
        int GetHealthId(List<InfoHead> list);
        int GetLivingId(List<InfoHead> list);
        int GetStatusId(List<InfoHead> list);
        string GetJob(List<InfoHead> list);

        int GetTypeHousingId(List<InfoHousing> list);
        int GetConditionHousingId(List<InfoHousing> list);
        int GetConditionGasId(List<InfoHousing> list);
        int GetConditionWaterId(List<InfoHousing> list);
        int GetConditionHeatingId(List<InfoHousing> list);
        int GetConditionSanitaryId(List<InfoHousing> list);
        double GetHousingArea(List<InfoHousing> list);
        int GetHousingRoom(List<InfoHousing> list);

        string GetDocNumer(List<InfoDocumentsBasic> list);
        string GetDocDate(List<InfoDocumentsBasic> list);

        string GetPassportSerial(List<InfoDocumentsPassport> list);
        string GetPassportNumber(List<InfoDocumentsPassport> list);
        string GetPassportCode(List<InfoDocumentsPassport> list);
        string GetPassportDate(List<InfoDocumentsPassport> list);
        string GetPassportIssued(List<InfoDocumentsPassport> list);

        string GetDisabilityGroup(List<InfoDocumentsDisability> list);
        string GetDisabilityDegree(List<InfoDocumentsDisability> list);
        string GetDisabilitySerial(List<InfoDocumentsDisability> list);
        string GetDisabilityNumber(List<InfoDocumentsDisability> list);
        string GetDisabilityDate(List<InfoDocumentsDisability> list);
        string GetDisabilityAct(List<InfoDocumentsDisability> list);

        bool IsNeedUpdate(string source, string update);
        bool IsExistCategory(List<DirectoryDefault> list, int id);
        List<DirectoryDefault> AddCategory(List<DirectoryDefault> list, int id, string name);
        List<DirectoryDefault> RemoveCategory(List<DirectoryDefault> list, int id);

        void ActionsClient(int clientid, string commandtext, bool isOk);
    }
}
