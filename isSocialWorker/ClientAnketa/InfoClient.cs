﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoClient
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string DateBorn { get; set; }
        public string Snils { get; set; }
        public string Phone { get; set; }

        public int IsDead { get; set; }
        public int IsDisposal { get; set; }
        public int IsSuspend { get; set; }

    }
}
