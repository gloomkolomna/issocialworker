﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public interface IReportModel
    {
        List<ReportDetails> GetReportClient(int clientid, int report_month, int report_year, bool report_type);
        string GetPaymentTerms(int clientid, int report_month, int report_year);
        double GetSummTotal(int clientid, int report_month, int report_year);
        double GetSummPay(int clientid, int report_month, int report_year, bool type);
        string GetWorkerName(int clientid, int report_month, int report_year, bool type);
        string GetDepartamentName(int clientid, int report_month, int report_year, bool type);

        double CheckVersionExcel();
        void ReportExcel(double _versionExcel, List<ReportDetails> list, string fio, string workername, string departamentname,
            string paymentTerm, double summTotal, double summPay, string date, bool type);
        void ActExcel(double _versionExcel, List<ReportDetails> list, string fio, string workername, double summTotal, double summPay, string date);

    }
}
