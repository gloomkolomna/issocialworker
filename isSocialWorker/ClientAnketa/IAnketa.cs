﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public interface IAnketa
    {
        #region Fields

        bool IsDead { set; }
        bool IsDisposal {set; }
        bool IsSuspend { set; }

        // Client info
        string Lastname { get; set; }
        string Firstname { get; set; }
        string Middlename { get; set; }
        string Dateborn { get; set; }
        string Snils { get; set; }
        string Phone { get; set; }

        // Head info
        int DepartamentId { get; set; }
        int SocialWorkerId { get; set; }
        int MedicalWorkerId { get; set; }
        int StatusId { get; set; }
        int HealthId { get; set; }
        int ConditionLivingId { get; set; }
        string Job { get; set; }
        int CategoryId { get; }
        string CategoryName { get; }
        int CategorySelectIndex { get; }
        List<DirectoryDefault> Categorys { get; set; }

        // Address info
        int RegionId { get; set; }
        int CityId { get; set; }
        int StreetId { get; set; }
        int HouseId { get; set; }
        int AppartamentId { get; set; }
        bool IsRefugee { get; set; }
        bool IsHomeless { get; set; }

        // Housing info
        int TypeHousingId { get; set; }
        int ConditionHousingId { get; set; }
        int ConditionGasId { get; set; }
        int ConditionWaterId { get; set; }
        int ConditionHeatingId { get; set; }
        int ConditionSanitaryId { get; set; }
        double HousingArea { get; set; }
        int HousingRoom { get; set; }
        bool RepairCosmetic { get; set; }
        bool RepairMajor { get; set; }

        // Documents info
        string PassportSerial { get; set; }
        string PassportNumber { get; set; }
        string PassportCode { get; set; }
        string PassportDate { get; set; }
        string PassportIssued { get; set; }

        string DisabilityGroup { get; set; }
        string DisabilityDegree { get; set; }
        string DisabilitySerial { get; set; }
        string DisabilityNumber { get; set; }
        string DisabilityDate { get; set; }
        string DisabilityAct { get; set; }

        string CertificatePensionNumber { get; set; }
        string CertificatePensionDate { get; set; }
        string CertificateDisabledNumber { get; set; }
        string CertificateDisabledDate { get; set; }
        string CertificateMemberNumber { get; set; }
        string CertificateMemberDate { get; set; }
        string CertificateVeteranNumber { get; set; }
        string CertificateVeteranDate { get; set; }
        string CertificateMemberLaborNumber { get; set; }
        string CertificateMemberLaborDate { get; set; }

        // Other
        List<DirectoryDefault> Lists { set; }

        // Reports
        string ReportServicePay { set; }
        double ReportTotal { set; }
        double ReportAllPay { set; }

        string ReportDate { get; }
        bool IsReportGuaranteed { get; }
        bool IsReportAdditional { get; }

        List<Report> ReportList { set; }
        List<ReportDetails> ReportDetailsList { get; set; }

        void FillingDataReport();

        #endregion

        #region Methods

        // Head
        void DepartamentList();
        void SocialWorkerList();
        void MedicalWorkerList();
        void StatusList();
        void HealthList();
        void ConditionLivingList();
        void CategoryList();
        void FillingCategory();

        // Address
        void RegionFilling();
        void CityFilling();
        void StreetFilling();
        void HouseFilling();
        void AppartamentFilling();

        // Housing
        void TypeHousingFilling();
        void ConditionHousingFilling();
        void ConditionGasFilling();
        void ConditionWaterFilling();
        void ConditionHeatingFilling();
        void ConditionSanitaryFilling();

        #endregion

        #region Events

        event EventHandler LoadForm;

        event EventHandler ButtonSuspendStopClick;
        event EventHandler ButtonSuspendStartClick;
        event EventHandler ButtonDisposalStopClick;
        event EventHandler ButtonDisposalStartClick;
        event EventHandler ButtonDeadClick;

        event EventHandler ButtonPrintAnketaClick;

        event EventHandler ButtonSaveInfoClick;
        event EventHandler ButtonSaveHeadClick;
        event EventHandler ButtonSaveAddressClick;
        event EventHandler ButtonSaveHousingClick;
        event EventHandler ButtonSaveDocumentsClick;

        event EventHandler ButtonAddCategoryToListClick;
        event EventHandler ButtonRemoveCategoryToListClick;

        event EventHandler RegionSelect;
        event EventHandler CitySelect;
        event EventHandler StreetSelect;
        event EventHandler HouseSelect;
        event EventHandler AppartamentSelect;

        event EventHandler ButtonReportPrintClick;
        event EventHandler ButtonReportPrintActClick;
        event EventHandler ButtonReportViewClick;
        event EventHandler ButtonReportSaveClick;

        event EventHandler DataReportCountChanged;

        #endregion

        string DataValues { get; }

        string SetClientInfo { set; }
        string SetHeadInfo { set; }
        string SetAddressInfo { set; }
        string SetHousingInfo { set; }
        string SetDocumentsInfo { set; }
    }
}
