﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace isSocialWorker.ClientAnketa
{
    public class AnketaPresenter
    {
        private readonly IAnketa _view;
        private readonly IAnketaModel _model;
        private readonly IMessageService _message;
        private readonly IAnketaUpdater _updater;
        private readonly IReportModel _report;
        private readonly int _clientId;

        private List<InfoClient> infoClient;
        private List<InfoHead> infoHead;
        private List<InfoAddress> infoAddress;
        private List<InfoHousing> infoHousing;
        private List<InfoDocumentsPassport> infoDocPassport;
        private List<InfoDocumentsDisability> infoDocDisability;
        private List<InfoDocumentsBasic> infoDocBasic;
        private List<ReportDetails> listReport;

        public AnketaPresenter(int clientId, IAnketa view, IAnketaModel model, IMessageService message, IAnketaUpdater updater, IReportModel report)
        {
            _view = view;
            _model = model;
            _message = message;
            _updater = updater;
            _report = report;
            _clientId = clientId;

            _view.LoadForm += _view_LoadForm;
            _view.RegionSelect += _view_RegionSelect;
            _view.CitySelect += _view_CitySelect;
            _view.StreetSelect += _view_StreetSelect;
            _view.HouseSelect += _view_HouseSelect;

            _view.ButtonSaveInfoClick += _view_ButtonSaveInfoClick;
            _view.ButtonSaveHeadClick += _view_ButtonSaveHeadClick;
            _view.ButtonSaveAddressClick += _view_ButtonSaveAddressClick;
            _view.ButtonSaveHousingClick += _view_ButtonSaveHousingClick;
            _view.ButtonSaveDocumentsClick += _view_ButtonSaveDocumentsClick;

            _view.ButtonAddCategoryToListClick += _view_ButtonAddCategoryToListClick;
            _view.ButtonRemoveCategoryToListClick += _view_ButtonRemoveCategoryToListClick;

            _view.ButtonSuspendStopClick += _view_ButtonSuspendStopClick;
            _view.ButtonSuspendStartClick += _view_ButtonSuspendStartClick;
            _view.ButtonDeadClick += _view_ButtonDeadClick;
            _view.ButtonDisposalStartClick += _view_ButtonDisposalStartClick;
            _view.ButtonDisposalStopClick += _view_ButtonDisposalStopClick;
            
            _view.ButtonReportViewClick += _view_ButtonReportViewClick;
            _view.ButtonReportPrintClick += _view_ButtonReportPrintClick;
            _view.ButtonReportPrintActClick += _view_ButtonReportPrintActClick;

            _view.DataReportCountChanged += _view_DataReportCountChanged;
        }

        void _view_DataReportCountChanged(object sender, EventArgs e)
        {
            int serviceid = Int32.Parse(_view.DataValues);
            int valuedefault = _view.ReportDetailsList.Where(x => x.ServiceId == serviceid).Select(x => x.ServiceCount).SingleOrDefault();

        }

        void _view_ButtonReportPrintActClick(object sender, EventArgs e)
        {
            string[] report_date = _view.ReportDate.Split(';');
            int report_month = Int32.Parse(report_date[0]);
            int report_year = Int32.Parse(report_date[1]);
            DateTime dt = new DateTime(report_year,report_month,1);
            string date = String.Format("{0} {1}", dt.ToString("MMMM"), dt.ToString("yyyy"));

            bool isType = _view.IsReportGuaranteed;

            double versionExcel = _report.CheckVersionExcel();
            string fio = _view.Lastname + " " + _view.Firstname + " " + _view.Middlename;

            string workername = _report.GetWorkerName(_clientId, report_month, report_year, isType);
            double summPay = _report.GetSummPay(_clientId, report_month, report_year, isType);
            double summTotal = _report.GetSummTotal(_clientId, report_month, report_year);

            _report.ActExcel(versionExcel, _view.ReportDetailsList, fio, workername, summTotal, summPay, date);
        }

        void _view_ButtonReportPrintClick(object sender, EventArgs e)
        {
            string[] report_date = _view.ReportDate.Split(';');
            int report_month = Int32.Parse(report_date[0]);
            int report_year = Int32.Parse(report_date[1]);
            DateTime dt = new DateTime(report_year, report_month, 1);
            string date = String.Format("{0} {1}", dt.ToString("MMMM"), dt.ToString("yyyy"));

            bool isType = _view.IsReportGuaranteed;

            double versionExcel = _report.CheckVersionExcel();
            string fio = _view.Lastname + " " + _view.Firstname + " " + _view.Middlename;

            string workername = _report.GetWorkerName(_clientId, report_month, report_year, isType);
            string departamentname = _report.GetDepartamentName(_clientId, report_month, report_year, isType);
            string paymentTerm = _report.GetPaymentTerms(_clientId, report_month, report_year);
            double summPay = _report.GetSummPay(_clientId, report_month, report_year, isType);
            double summTotal = _report.GetSummTotal(_clientId, report_month, report_year);

            _report.ReportExcel(versionExcel, _view.ReportDetailsList, fio, workername, departamentname, paymentTerm, summTotal, summPay, date, isType);
        }

        void _view_ButtonReportViewClick(object sender, EventArgs e)
        {
            string[] report_date = _view.ReportDate.Split(';');
            int report_month = Int32.Parse(report_date[0]);
            int report_year = Int32.Parse(report_date[1]);
            bool isType = _view.IsReportGuaranteed;

            _view.ReportServicePay = _report.GetPaymentTerms(_clientId, report_month, report_year);
            _view.ReportAllPay = _report.GetSummPay(_clientId, report_month, report_year, isType);
            if (isType)
                _view.ReportTotal = _report.GetSummTotal(_clientId, report_month, report_year);
            else
                _view.ReportTotal = 0;

            listReport = _report.GetReportClient(_clientId, report_month, report_year, isType);
            _view.ReportDetailsList = listReport;
            _view.FillingDataReport();
        }

        void _view_ButtonDisposalStopClick(object sender, EventArgs e)
        {
            _model.ActionsClient(_clientId, "p_updateClient_Disposal", true);
        }

        void _view_ButtonDisposalStartClick(object sender, EventArgs e)
        {
            _model.ActionsClient(_clientId, "p_updateClient_Disposal", false);
        }

        void _view_ButtonDeadClick(object sender, EventArgs e)
        {
            _model.ActionsClient(_clientId, "p_updateClient_Dead", true);
        }

        void _view_ButtonSuspendStartClick(object sender, EventArgs e)
        {
            _model.ActionsClient(_clientId, "p_updateClient_Suspend", false);
        }

        void _view_ButtonSuspendStopClick(object sender, EventArgs e)
        {
            _model.ActionsClient(_clientId, "p_updateClient_Suspend", true);
        }

        void _view_ButtonSaveDocumentsClick(object sender, EventArgs e)
        {
            infoDocPassport = _model.GetDocumentsPassport(_clientId);
            infoDocDisability = _model.GetDocumentsDisability(_clientId);

            // Passport
            _updater.UpdateClientDocumentsPassport(_clientId, _view.PassportSerial, _view.PassportNumber, _view.PassportCode, 
                _view.PassportDate, _view.PassportIssued);

            // Disability
            _updater.UpdateClientDocumentsDisability(_clientId, _view.DisabilityGroup, _view.DisabilityDegree, _view.DisabilitySerial,
                _view.DisabilityNumber, _view.DisabilityDate, _view.DisabilityAct);

            if(_view.CertificateDisabledNumber != "")
            {
                _updater.UpdateClientCertificate(_clientId, "p_updateClient_DocDisabled", _view.CertificateDisabledNumber, _view.CertificateDisabledDate);
            }

            if (_view.CertificateMemberNumber != "")
            {
                _updater.UpdateClientCertificate(_clientId, "p_updateClient_DocMember", _view.CertificateMemberNumber, _view.CertificateMemberDate);
            }

            if (_view.CertificateMemberLaborNumber != "")
            {
                _updater.UpdateClientCertificate(_clientId, "p_updateClient_DocMemberLabor", _view.CertificateMemberLaborNumber, _view.CertificateMemberLaborDate);
            }

            if (_view.CertificatePensionNumber != "")
            {
                _updater.UpdateClientCertificate(_clientId, "p_updateClient_DocPension", _view.CertificatePensionNumber, _view.CertificatePensionDate);
            }

            if (_view.CertificateVeteranNumber != "")
            {
                _updater.UpdateClientCertificate(_clientId, "p_updateClient_DocVeteran", _view.CertificateVeteranNumber, _view.CertificateVeteranDate);
            }

        }

        void _view_ButtonSaveHousingClick(object sender, EventArgs e)
        {
            infoHousing = _model.GetHousingInfo(_clientId);

            // Type Housing
            Updater("p_updateClient_HousingType", _model.GetTypeHousingId(infoHousing), _view.TypeHousingId);

            // Area
            Updater("p_updateClient_HousingArea", _model.GetHousingArea(infoHousing), _view.HousingArea);

            // Room
            Updater("p_updateClient_HousingRoom", _model.GetHousingRoom(infoHousing), _view.HousingRoom);

            // Condition housing
            Updater("p_updateClient_HousingCondition", _model.GetConditionHousingId(infoHousing), _view.ConditionHousingId);

            // Repair cosmetic
            Updater("p_updateClient_HousingRepairCosmetic", infoHousing[0].RepairCosmetic, _view.RepairCosmetic ? 1 : 0);

            // Repair cosmetic
            Updater("p_updateClient_HousingRepairMajor", infoHousing[0].RepairMajor, _view.RepairMajor ? 1 : 0);

            // Condition gas
            Updater("p_updateClient_HousingGas", _model.GetConditionGasId(infoHousing), _view.ConditionGasId);

            // Condition water
            Updater("p_updateClient_HousingWater", _model.GetConditionWaterId(infoHousing), _view.ConditionWaterId);

            // Condition heating
            Updater("p_updateClient_HousingHeating", _model.GetConditionHeatingId(infoHousing), _view.ConditionHeatingId);

            // Condition sanitary
            Updater("p_updateClient_HousingSanitary", _model.GetConditionSanitaryId(infoHousing), _view.ConditionSanitaryId);

            _view.SetHousingInfo = "Информация о клиенте обновлена " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        void _view_ButtonSaveAddressClick(object sender, EventArgs e)
        {
            string address = String.Format("{0};{1};{2};{3};{4}", _view.RegionId, _view.CityId, _view.StreetId, _view.HouseId, _view.AppartamentId);
            int isRefugee = _view.IsRefugee ? 1 : 0;
            int isHomeless = _view.IsHomeless ? 1 : 0;

            _updater.UpdateClientAddress(_clientId, address, isRefugee, isHomeless);
            _view.SetAddressInfo = "Информация о клиенте обновлена " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        void _view_ButtonRemoveCategoryToListClick(object sender, EventArgs e)
        {
            List<DirectoryDefault> list = _view.Categorys;
            int id = _view.CategorySelectIndex;

            if (id != 0)
            {
                _view.Categorys = _model.RemoveCategory(list, id);
                _view.FillingCategory();
            }
        }

        void _view_ButtonAddCategoryToListClick(object sender, EventArgs e)
        {
            List<DirectoryDefault> list = _view.Categorys;
            int id = _view.CategoryId;

            bool isExistCategory = _model.IsExistCategory(list, id);
            if (isExistCategory)
                _message.ShowExclamation("Такая категория уже добавлена!");
            else
            {
                string name = _view.CategoryName;
                _view.Categorys = _model.AddCategory(list, id, name);
                _view.FillingCategory();
            }
        }

        void _view_ButtonSaveHeadClick(object sender, EventArgs e)
        {
            infoHead = _model.GetHeadInfo(_clientId);

            // Departament
            Updater("p_updateClient_HeadDepartament", _model.GetDepartamentId(infoHead), _view.DepartamentId);

            // Socworker
            Updater("p_updateClient_HeadSocworker", _model.GetSocialworkerId(infoHead), _view.SocialWorkerId);

            // Medicalworker
            Updater("p_updateClient_HeadMedicalworker", _model.GetMedicalworkerId(infoHead), _view.MedicalWorkerId);

            // Status
            Updater("p_updateClient_HeadStatus", _model.GetStatusId(infoHead), _view.StatusId);

            // Health
            Updater("p_updateClient_HeadHealting", _model.GetHealthId(infoHead), _view.HealthId);

            // Living
            Updater("p_updateClient_HeadConditionLiving", _model.GetLivingId(infoHead), _view.ConditionLivingId);

            // Job
            Updater("p_updateClient_HeadJob", _model.GetJob(infoHead), _view.Job);

            // Categorys
            int count = _view.Categorys.Count;
            for(int i = 0; i < count; i++)
            {
                _updater.UpdateClientBasic("p_updateClient_HeadCategory", _clientId, _view.Categorys[i].Id);
            }

            _view.SetHeadInfo = "Информация о клиенте обновлена " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();

        }

        private void Updater(string commandtext, int source, int upd_id)
        {
            bool isNeedUpdate = _model.IsNeedUpdate(source.ToString(), upd_id.ToString());
            if (isNeedUpdate)
                _updater.UpdateClientBasic(commandtext, _clientId, upd_id);
        }

        private void Updater(string commandtext, string source, string upd_id)
        {
            bool isNeedUpdate = _model.IsNeedUpdate(source, upd_id);
            if (isNeedUpdate)
                _updater.UpdateClientBasic(commandtext, _clientId, upd_id);
        }

        private void Updater(string commandtext, double source, double upd_id)
        {
            bool isNeedUpdate = _model.IsNeedUpdate(source.ToString(), upd_id.ToString());
            if (isNeedUpdate)
                _updater.UpdateClientBasic(commandtext, _clientId, upd_id);
        }

        void _view_ButtonSaveInfoClick(object sender, EventArgs e)
        {
            _updater.UpdateClient(_clientId, _view.Lastname, _view.Firstname, _view.Middlename, _view.Dateborn, _view.Snils, _view.Phone);
            _view.SetClientInfo = "Информация о клиенте обновлена " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        #region События на select у адресов
        void _view_HouseSelect(object sender, EventArgs e)
        {
            _view.Lists = _model.GetDirectory("p_getAddress_Appartament", _view.HouseId);
            _view.AppartamentFilling();
        }

        void _view_StreetSelect(object sender, EventArgs e)
        {
            if (_view.StreetId == 0)
                _view.Lists = _model.GetDirectory("p_getAddress_House_city", _view.CityId);
            else
                _view.Lists = _model.GetDirectory("p_getAddress_House_street", _view.StreetId);

            _view.HouseFilling();
        }

        void _view_CitySelect(object sender, EventArgs e)
        {
            _view.Lists = _model.GetDirectory("p_getAddress_Street", _view.CityId);
            _view.StreetFilling();
        }

        void _view_RegionSelect(object sender, EventArgs e)
        {
            _view.Lists = _model.GetDirectory("p_getAddress_City", _view.RegionId);
            _view.CityFilling();
        }
        #endregion

        void _view_LoadForm(object sender, EventArgs e)
        {
            infoClient = _model.GetClientInfo(_clientId);
            infoHead = _model.GetHeadInfo(_clientId);
            infoAddress = _model.GetAddressInfo(_clientId);
            infoHousing = _model.GetHousingInfo(_clientId);
            infoDocPassport = _model.GetDocumentsPassport(_clientId);
            infoDocDisability = _model.GetDocumentsDisability(_clientId);

            #region Данные о клиенте

            _view.Lastname = _model.GetLastName(infoClient);
            _view.Firstname = _model.GetFirstName(infoClient);
            _view.Middlename = _model.GetMiddleName(infoClient);
            _view.Dateborn = _model.GetDateBorn(infoClient);
            _view.Snils = _model.GetSnils(infoClient);
            _view.Phone = _model.GetPhone(infoClient);

            _view.IsDead = infoClient[0].IsDead == 0 ? false : true;
            _view.IsDisposal = infoClient[0].IsDisposal == 0 ? false : true;
            _view.IsSuspend = infoClient[0].IsSuspend == 0 ? false : true;

            #endregion

            #region Данные для заведующих

            _view.Lists = _model.GetDirectory("p_getDirectory_departaments");
            _view.DepartamentList();
            _view.Lists = _model.GetDirectory("p_getDirectory_socialworker");
            _view.SocialWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_medicalworker");
            _view.MedicalWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_status");
            _view.StatusList();
            _view.Lists = _model.GetDirectory("p_getDirectory_health");
            _view.HealthList();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionliving");
            _view.ConditionLivingList();
            _view.Lists = _model.GetDirectory("p_getDirectory_category");
            _view.CategoryList();
            _view.Categorys = _model.GetCategoryList(_clientId);
            _view.FillingCategory();

            _view.DepartamentId = _model.GetDepartamentId(infoHead);
            _view.SocialWorkerId = _model.GetSocialworkerId(infoHead);
            _view.MedicalWorkerId = _model.GetMedicalworkerId(infoHead);
            _view.StatusId = _model.GetStatusId(infoHead);
            _view.HealthId = _model.GetHealthId(infoHead);
            _view.ConditionLivingId = _model.GetLivingId(infoHead);
            _view.Job = _model.GetJob(infoHead);

            #endregion

            #region Загрузка данных адреса

            string[] address = infoAddress[0].Address.Split(';');
            int regionId = Int32.Parse(address[0]);
            int cityId = Int32.Parse(address[1]);
            int streetId = Int32.Parse(address[2]);
            int houseId = Int32.Parse(address[3]);
            int appartamentId = Int32.Parse(address[4]);

            _view.Lists = _model.GetDirectory("p_getAddress_Region");
            _view.RegionFilling();
            _view.RegionId = regionId;

            if (cityId != 0)
            {
                _view.Lists = _model.GetDirectory("p_getAddress_City", regionId);
                _view.CityFilling();
                _view.CityId = cityId;
            }

            if (streetId != 0)
            {
                _view.Lists = _model.GetDirectory("p_getAddress_Street", cityId);
                _view.StreetFilling();
                _view.StreetId = streetId;
            }

            if(houseId != 0)
            {
                if (streetId != 0)
                {
                    _view.Lists = _model.GetDirectory("p_getAddress_House_street", streetId);
                    _view.HouseFilling();
                    _view.HouseId = houseId;
                }
                else
                {
                    _view.Lists = _model.GetDirectory("p_getAddress_House_city", cityId);
                    _view.HouseFilling();
                    _view.HouseId = houseId;
                }
            }

            if(appartamentId != 0)
            {
                _view.Lists = _model.GetDirectory("p_getAddress_Appartament", houseId);
                _view.AppartamentFilling();
                _view.AppartamentId = appartamentId;
            }

            bool isRefugee = infoAddress[0].IsRefugee == 0 ? false : true;
            bool isHomeless = infoAddress[0].IsHomeless == 0 ? false : true;
            _view.IsRefugee = isRefugee;
            _view.IsHomeless = isHomeless;

            #endregion

            #region Загрузка данных жилья

            _view.Lists = _model.GetDirectory("p_getDirectory_typeHousing");
            _view.TypeHousingFilling();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionHousing");
            _view.ConditionHousingFilling();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionGas");
            _view.ConditionGasFilling();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionWater");
            _view.ConditionWaterFilling();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionHeating");
            _view.ConditionHeatingFilling();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionSanitary");
            _view.ConditionSanitaryFilling();

            _view.TypeHousingId = _model.GetTypeHousingId(infoHousing);
            _view.ConditionHeatingId = _model.GetConditionHeatingId(infoHousing);
            _view.ConditionGasId = _model.GetConditionGasId(infoHousing);
            _view.ConditionWaterId = _model.GetConditionWaterId(infoHousing);
            _view.ConditionHeatingId = _model.GetConditionHeatingId(infoHousing);
            _view.ConditionSanitaryId = _model.GetConditionSanitaryId(infoHousing);
            _view.HousingArea = _model.GetHousingArea(infoHousing);
            _view.HousingRoom = _model.GetHousingRoom(infoHousing);
            _view.RepairCosmetic = infoHousing[0].RepairCosmetic == 0 ? false : true;
            _view.RepairMajor = infoHousing[0].RepairMajor == 0 ? false : true;

            #endregion

            #region Загрузка данных о документах

            #region Паспорт

            _view.PassportSerial = _model.GetPassportSerial(infoDocPassport);
            _view.PassportNumber = _model.GetPassportNumber(infoDocPassport);
            _view.PassportCode = _model.GetPassportCode(infoDocPassport);
            _view.PassportDate = _model.GetPassportDate(infoDocPassport);
            _view.PassportIssued = _model.GetPassportIssued(infoDocPassport);

            #endregion

            #region Инвалидность

            _view.DisabilityGroup = _model.GetDisabilityGroup(infoDocDisability);
            _view.DisabilityDegree = _model.GetDisabilityDegree(infoDocDisability);
            _view.DisabilitySerial = _model.GetDisabilitySerial(infoDocDisability);
            _view.DisabilityNumber = _model.GetDisabilityNumber(infoDocDisability);
            _view.DisabilityDate = _model.GetDisabilityDate(infoDocDisability);
            _view.DisabilityAct = _model.GetDisabilityAct(infoDocDisability);

            #endregion

            #region Остальные документы

            infoDocBasic = _model.GetDocumentsBasic("p_getClientCurrent_DocDisabled", _clientId);
            _view.CertificateDisabledNumber = _model.GetDocNumer(infoDocBasic);
            _view.CertificateDisabledDate = _model.GetDocDate(infoDocBasic);

            infoDocBasic = _model.GetDocumentsBasic("p_getClientCurrent_DocMember", _clientId);
            _view.CertificateMemberNumber = _model.GetDocNumer(infoDocBasic);
            _view.CertificateMemberDate = _model.GetDocDate(infoDocBasic);

            infoDocBasic = _model.GetDocumentsBasic("p_getClientCurrent_DocMemberLabor", _clientId);
            _view.CertificateMemberLaborNumber = _model.GetDocNumer(infoDocBasic);
            _view.CertificateMemberLaborDate = _model.GetDocDate(infoDocBasic);

            infoDocBasic = _model.GetDocumentsBasic("p_getClientCurrent_DocPension", _clientId);
            _view.CertificatePensionNumber = _model.GetDocNumer(infoDocBasic);
            _view.CertificatePensionDate = _model.GetDocDate(infoDocBasic);

            infoDocBasic = _model.GetDocumentsBasic("p_getClientCurrent_DocVeteran", _clientId);
            _view.CertificateVeteranNumber = _model.GetDocNumer(infoDocBasic);
            _view.CertificateVeteranDate = _model.GetDocDate(infoDocBasic);

            #endregion

            #endregion
        }

    }
}
