﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoHousing
    {
        public int TypeHousingId { get; set; }
        public int ConditionHousingId { get; set; }
        public int ConditionGasId { get; set; }
        public int ConditionWaterId { get; set; }
        public int ConditionHeatingId { get; set; }
        public int ConditionSanitaryId { get; set; }

        public double HousingArea { get; set; }
        public int HousingRoom { get; set; }
        public int RepairCosmetic { get; set; }
        public int RepairMajor { get; set; }
    }
}
