﻿using isSocialWorker.ClientAnketa;
using isSocialWorker.Directory;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class AnketaModel : IAnketaModel
    {
        public List<DirectoryDefault> GetDirectory(string commandText)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public List<DirectoryDefault> GetDirectory(string commandText, int id)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();

            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();

            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandText;
            command.Parameters.AddWithValue("$id", id);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new DirectoryDefault()
                    {
                        Id = Int32.Parse(dataReader["id"].ToString()),
                        Name = dataReader["name"].ToString()
                    }
                    );
                }
            }
            catch (MySqlException ex)
            {
                logs.logger.Warn(" -> " + ex.Message);
            }
            finally
            {
                connection.Close();
            }

            return list;
        }

        public List<DirectoryDefault> GetCategoryList(int clientid)
        {
            List<DirectoryDefault> list = new List<DirectoryDefault>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getClientCurrentCategory";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(
                        new DirectoryDefault()
                        {
                            Id = Int32.Parse(dataReader["id"].ToString()),
                            Name = dataReader["name"].ToString()
                        }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoClient> GetClientInfo(int clientid)
        {
            List<InfoClient> list = new List<InfoClient>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getAnketa_info";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while(dataReader.Read())
                {
                    list.Add(new InfoClient()
                    {
                        LastName = dataReader["lastname"].ToString(),
                        FirstName = dataReader["firstname"].ToString(),
                        MiddleName = dataReader["middlename"].ToString(),
                        DateBorn = dataReader["dateborn"].ToString(),
                        Snils = dataReader["snils"].ToString(),
                        Phone = dataReader["phone"].ToString(),
                        IsDead = Int32.Parse(dataReader["isDead"].ToString()),
                        IsDisposal = Int32.Parse(dataReader["isDisposal"].ToString()),
                        IsSuspend = Int32.Parse(dataReader["isSuspend"].ToString())
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoHead> GetHeadInfo(int clientid)
        {
            List<InfoHead> list = new List<InfoHead>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getAnketa_head";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();
                while (dataReader.Read())
                {
                    list.Add(new InfoHead()
                    {
                        ConditionLivingId = Int32.Parse(dataReader["livingId"].ToString()),
                        DepartamentId = Int32.Parse(dataReader["departamentId"].ToString()),
                        HealthId = Int32.Parse(dataReader["healthId"].ToString()),
                        Job = dataReader["job"].ToString(),
                        MedicalWorkerId = Int32.Parse(dataReader["medicalId"].ToString()),
                        SocialWorkerId = Int32.Parse(dataReader["socworkerId"].ToString()),
                        StatusId = Int32.Parse(dataReader["statusId"].ToString())
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoAddress> GetAddressInfo(int clientid)
        {
            List<InfoAddress> list = new List<InfoAddress>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getAnketa_address";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while(dataReader.Read())
                {
                    list.Add(new InfoAddress()
                    {
                        Address = dataReader["address"].ToString(),
                        IsRefugee = Int32.Parse(dataReader["isRefugee"].ToString()),
                        IsHomeless = Int32.Parse(dataReader["isHomeless"].ToString())
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoHousing> GetHousingInfo(int clientid)
        {
            List<InfoHousing> list = new List<InfoHousing>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getAnketa_housing";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(new InfoHousing()
                    {
                        ConditionGasId = Int32.Parse(dataReader["conditiongasid"].ToString()),
                        ConditionHeatingId = Int32.Parse(dataReader["conditionheatingid"].ToString()),
                        ConditionHousingId = Int32.Parse(dataReader["conditionhousingid"].ToString()),
                        ConditionSanitaryId = Int32.Parse(dataReader["conditionsanitaryid"].ToString()),
                        ConditionWaterId = Int32.Parse(dataReader["conditionwaterid"].ToString()),
                        HousingArea = Double.Parse(dataReader["housingarea"].ToString()),
                        HousingRoom = Int32.Parse(dataReader["housingroom"].ToString()),
                        RepairCosmetic = Int32.Parse(dataReader["isrepaircosmetic"].ToString()),
                        RepairMajor = Int32.Parse(dataReader["isrepairmajor"].ToString()),
                        TypeHousingId = Int32.Parse(dataReader["typehousingid"].ToString())
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoDocumentsPassport> GetDocumentsPassport(int clientid)
        {
            List<InfoDocumentsPassport> list = new List<InfoDocumentsPassport>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getClientCurrent_DocPassport";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(new InfoDocumentsPassport()
                    {
                        Serial = dataReader["serial"].ToString(),
                        Number = dataReader["number"].ToString(),
                        Code = dataReader["code"].ToString(),
                        Date = dataReader["date"].ToString(),
                        Issued = dataReader["issued"].ToString()
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoDocumentsDisability> GetDocumentsDisability(int clientid)
        {
            List<InfoDocumentsDisability> list = new List<InfoDocumentsDisability>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_getClientCurrent_DocDisability";
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(new InfoDocumentsDisability()
                    {
                        Group = dataReader["groupinv"].ToString(),
                        Degree = dataReader["degree"].ToString(),
                        Serial = dataReader["serial"].ToString(),
                        Number = dataReader["number"].ToString(),
                        Date = dataReader["date"].ToString(),
                        Act = dataReader["act"].ToString()
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        public List<InfoDocumentsBasic> GetDocumentsBasic(string commandtext, int clientid)
        {
            List<InfoDocumentsBasic> list = new List<InfoDocumentsBasic>();
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            MySqlDataReader dataReader;

            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);

            try
            {
                connection.Open();
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    list.Add(new InfoDocumentsBasic()
                    {
                        Number = dataReader["number"].ToString(),
                        Date = dataReader["date"].ToString()
                    }
                        );
                }
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }

            return list;
        }

        #region Получение данных о клиенте (ФИО, ДР, СНИЛС...)
        public string GetFirstName(List<InfoClient> list)
        {
            var str = list.Select(x => x.FirstName).ToList();
            return String.Join("",str);
        }

        public string GetLastName(List<InfoClient> list)
        {
            var str = list.Select(x => x.LastName).ToList();
            return String.Join("", str);
        }

        public string GetMiddleName(List<InfoClient> list)
        {
            var str = list.Select(x => x.MiddleName).ToList();
            return String.Join("", str);
        }

        public string GetDateBorn(List<InfoClient> list)
        {
            var str = list.Select(x => x.DateBorn).ToList();
            return String.Join("", str);
        }

        public string GetSnils(List<InfoClient> list)
        {
            var str = list.Select(x => x.Snils).ToList();
            return String.Join("", str);
        }

        public string GetPhone(List<InfoClient> list)
        {
            var str = list.Select(x => x.Phone).ToList();
            return String.Join("", str);
        }

        public bool GetIsDead(List<InfoClient> list)
        {
            var str = Int32.Parse(list.Select(x => x.IsDead).ToString()) == 0 ? false : true;
            return str;
        }

        public bool GetIsDisposal(List<InfoClient> list)
        {
            var str = Int32.Parse(list.Select(x => x.IsDisposal).ToString()) == 0 ? false : true;
            return str;
        }

        public bool GetIsSuspend(List<InfoClient> list)
        {
            var str = Int32.Parse(list.Select(x => x.IsSuspend).ToString()) == 0 ? false : true;
            return str;
        }
        #endregion

        #region Получение Id для заведующих
        public int GetDepartamentId(List<InfoHead> list)
        {
            var str = list.Select(x => x.DepartamentId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetSocialworkerId(List<InfoHead> list)
        {
            var str = list.Select(x => x.SocialWorkerId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetMedicalworkerId(List<InfoHead> list)
        {
            var str = list.Select(x => x.MedicalWorkerId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetHealthId(List<InfoHead> list)
        {
            var str = list.Select(x => x.HealthId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetLivingId(List<InfoHead> list)
        {
            var str = list.Select(x => x.ConditionLivingId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetStatusId(List<InfoHead> list)
        {
            var str = list.Select(x => x.StatusId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public string GetJob(List<InfoHead> list)
        {
            var str = list.Select(x => x.Job).ToList();
            return String.Join("", str);
        }
        #endregion

        #region Данные о жилищных условиях
        public int GetTypeHousingId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.TypeHousingId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetConditionHousingId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.ConditionHeatingId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetConditionGasId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.ConditionGasId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetConditionWaterId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.ConditionWaterId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetConditionHeatingId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.ConditionHeatingId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public int GetConditionSanitaryId(List<InfoHousing> list)
        {
            var str = list.Select(x => x.ConditionSanitaryId).ToList();
            return Int32.Parse(String.Join("", str));
        }

        public double GetHousingArea(List<InfoHousing> list)
        {
            var str = list.Select(x => x.HousingArea).ToList();
            return Double.Parse(String.Join("", str));
        }

        public int GetHousingRoom(List<InfoHousing> list)
        {
            var str = list.Select(x => x.HousingRoom).ToList();
            return Int32.Parse(String.Join("", str));
        }

        #endregion

        #region Данные о документах
        public string GetDocNumer(List<InfoDocumentsBasic> list)
        {
            var str = list.Select(x => x.Number).ToList();
            return String.Join("", str);
        }

        public string GetDocDate(List<InfoDocumentsBasic> list)
        {
            var str = list.Select(x => x.Date).ToList();
            return String.Join("", str);
        }

        public string GetPassportSerial(List<InfoDocumentsPassport> list)
        {
            var str = list.Select(x => x.Serial).ToList();
            return String.Join("", str);
        }

        public string GetPassportNumber(List<InfoDocumentsPassport> list)
        {
            var str = list.Select(x => x.Number).ToList();
            return String.Join("", str);
        }

        public string GetPassportCode(List<InfoDocumentsPassport> list)
        {
            var str = list.Select(x => x.Code).ToList();
            return String.Join("", str);
        }

        public string GetPassportDate(List<InfoDocumentsPassport> list)
        {
            var str = list.Select(x => x.Date).ToList();
            return String.Join("", str);
        }

        public string GetPassportIssued(List<InfoDocumentsPassport> list)
        {
            var str = list.Select(x => x.Issued).ToList();
            return String.Join("", str);
        }

        public string GetDisabilityGroup(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Group).ToList();
            return String.Join("", str);
        }

        public string GetDisabilityDegree(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Degree).ToList();
            return String.Join("", str);
        }

        public string GetDisabilitySerial(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Serial).ToList();
            return String.Join("", str);
        }

        public string GetDisabilityNumber(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Number).ToList();
            return String.Join("", str);
        }

        public string GetDisabilityDate(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Date).ToList();
            return String.Join("", str);
        }

        public string GetDisabilityAct(List<InfoDocumentsDisability> list)
        {
            var str = list.Select(x => x.Act).ToList();
            return String.Join("", str);
        }

        #endregion

        public List<DirectoryDefault> AddCategory(List<DirectoryDefault> list, int id, string name)
        {
            list.Add(new DirectoryDefault()
                {
                    Id = id,
                    Name = name
                });

            return list;
        }

        public List<DirectoryDefault> RemoveCategory(List<DirectoryDefault> list, int id)
        {
            list.RemoveAll(r => r.Id == id);

            return list;
        }

        #region Проверки

        public bool IsExistCategory(List<DirectoryDefault> list, int id)
        {
            var isExistCategory = list.FindAll(x => x.Id == id);
            if (isExistCategory.Count > 0)
                return true;
            else
                return false;
        }

        public bool IsNeedUpdate(string source, string update)
        {
            if (String.Equals(source, update))
                return false;
            else
                return true;
        }

        #endregion

        public void ActionsClient(int clientid, string commandtext, bool isOk)
        {
            int actions = isOk ? 1 : 0;
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$action", actions);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }
    }
}
