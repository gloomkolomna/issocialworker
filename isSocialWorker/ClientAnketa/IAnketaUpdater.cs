﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public interface IAnketaUpdater
    {
        void UpdateClient(int clientid, string lastname, string firstname, string middlename, string dateborn, string snils, string phone);

        void UpdateClientBasic(string commandtext, int clientid, int id);
        void UpdateClientBasic(string commandtext, int clientid, string job);
        void UpdateClientBasic(string commandtext, int clientid, double upd_id);

        void UpdateClientAddress(int clientid, string address, int isRefugee, int isHomeless);

        void UpdateClientDocumentsPassport(int clientid, string serial, string number, string code, string date, string issued);
        void UpdateClientDocumentsDisability(int clientid, string group, string degree, string serial, string number, string date, string act);
        void UpdateClientCertificate(int clientid, string commandtext, string number, string date);
    }
}
