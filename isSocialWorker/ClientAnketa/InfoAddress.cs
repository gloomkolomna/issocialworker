﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoAddress
    {
        public string Address { get; set; }
        public int IsRefugee { get; set; }
        public int IsHomeless { get; set; }
    }
}
