﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoHead
    {
        public int DepartamentId { get; set; }
        public int SocialWorkerId { get; set; }
        public int MedicalWorkerId { get; set; }
        public int StatusId { get; set; }
        public int HealthId { get; set; }
        public int ConditionLivingId { get; set; }
        public string Job { get; set; }
    }
}
