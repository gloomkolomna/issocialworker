﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class InfoDocumentsPassport
    {
        public string Serial { get; set; }
        public string Number { get; set; }
        public string Code { get; set; }
        public string Date { get; set; }
        public string Issued { get; set; }
    }
}
