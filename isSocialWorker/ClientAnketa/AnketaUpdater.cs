﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace isSocialWorker.ClientAnketa
{
    public class AnketaUpdater : IAnketaUpdater
    {
        public void UpdateClient(int clientid, string lastname, string firstname, string middlename, string dateborn, string snils, string phone)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_updateClient_Info";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$firstname", firstname);
            command.Parameters.AddWithValue("$lastname", lastname);
            command.Parameters.AddWithValue("$middlename", middlename);
            command.Parameters.Add("$dateborn", MySqlDbType.Date);
            command.Parameters["$dateborn"].Value = dateborn;
            command.Parameters.AddWithValue("$snils", snils);
            command.Parameters.AddWithValue("$phone", phone);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message);}
            finally { connection.Close(); }
        }

        public void UpdateClientAddress(int clientid, string address, int isRefugee, int isHomeless)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_updateClient_Address";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$address", address);
            command.Parameters.AddWithValue("$isRefugee", isRefugee);
            command.Parameters.AddWithValue("$isHomeless", isHomeless);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientDocumentsPassport(int clientid, string serial, string number, string code, string date, string issued)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_updateClient_DocPassport";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$serial", serial);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.AddWithValue("$code", code);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;
            command.Parameters.AddWithValue("$issued", issued);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientDocumentsDisability(int clientid, string group, string degree, string serial, string number, string date, string act)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = "p_updateClient_DocDisability";
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$serial", serial);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.AddWithValue("$groupinv", group);
            command.Parameters.AddWithValue("$degree", degree);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;
            command.Parameters.AddWithValue("$act", act);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientCertificate(int clientid, string commandtext, string number, string date)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$number", number);
            command.Parameters.Add("$date", MySqlDbType.Date);
            command.Parameters["$date"].Value = date;

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientBasic(string commandtext, int clientid, int id)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$upd_id", id);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientBasic(string commandtext, int clientid, string job)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$job", job);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }

        public void UpdateClientBasic(string commandtext, int clientid, double upd_id)
        {
            MySqlConnection connection = new MySqlConnection(Connect.MySqlConnectionString);
            MySqlCommand command = new MySqlCommand();
            command.Connection = connection;
            command.CommandType = CommandType.StoredProcedure;
            command.CommandText = commandtext;
            command.Parameters.AddWithValue("$clientid", clientid);
            command.Parameters.AddWithValue("$upd_id", upd_id);

            try
            {
                connection.Open();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex) { logs.logger.Warn(" -> " + ex.Message); }
            finally { connection.Close(); }
        }
    }
}
