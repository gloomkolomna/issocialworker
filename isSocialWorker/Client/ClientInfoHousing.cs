﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Client
{
    public class ClientInfoHousing
    {
        public static int TypeHousingId {get; set;}
        public static int ConditionHousingId {get; set;}
        public static int ConditionGasId {get; set;}
        public static int ConditionWaterId {get; set;}
        public static int ConditionHeatingId {get; set;}
        public static int ConditionSanitaryId { get; set; }

        public static double HousingArea { get; set; }
        public static int HousingRoom { get; set; }
        public static bool RepairCosmetic { get; set; }
        public static bool RepairMajor { get; set; }

        public static void ClearAllFields()
        {
            TypeHousingId = 0;
            ConditionHousingId = 0;
            ConditionGasId = 0;
            ConditionWaterId = 0;
            ConditionHeatingId = 0;
            ConditionSanitaryId = 0;

            HousingArea = 0;
            HousingRoom = 0;
            RepairCosmetic = false;
            RepairMajor = false;
        }
    }
}
