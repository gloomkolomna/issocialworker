﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Client
{
    public class ClientInfo
    {
        public static string FirstName { get; set; }
        public static string LastName { get; set; }
        public static string MiddleName { get; set; }
        public static string DateBorn { get; set; }
        public static string Snils { get; set; }
        public static string Phone { get; set; }

        public static bool IsDead { get; set; }
        public static bool IsDisposal { get; set; }
        public static bool IsSuspend { get; set; }

        public static void ClearAllFields()
        {
            FirstName = String.Empty;
            LastName = String.Empty;
            MiddleName = String.Empty;
            DateBorn = DateTime.Now.Date.ToShortDateString();
            Snils = String.Empty;
            Phone = String.Empty;
        }
    }
}
