﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Client
{
    public class ClientInfoAddress
    {
        public static int RegionId { get; set; }
        public static int CityId { get; set; }
        public static int StreetId { get; set; }
        public static int HouseId { get; set; }
        public static int AppartamentId { get; set; }

        public static bool IsRefugee { get; set; }
        public static bool IsHomeless { get; set; }

        public static void ClearAllFields()
        {
            RegionId = 0;
            CityId = 0;
            StreetId = 0;
            HouseId = 0;
            AppartamentId = 0;
            IsRefugee = false;
            IsHomeless = false;
        }
    }
}
