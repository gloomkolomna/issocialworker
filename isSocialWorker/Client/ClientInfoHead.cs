﻿using isSocialWorker.Directory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Client
{
    public class ClientInfoHead
    {
        public static int DepartamentId { get; set; }
        public static int SocialWorkerId { get; set; }
        public static int MedicalWorkerId { get; set; }
        public static int StatusId { get; set; }
        public static int HealthId { get; set; }
        public static int ConditionLivingId { get; set; }
        public static string Job { get; set; }

        public static List<DirectoryDefault> CategoryList = new List<DirectoryDefault>();

        public static void ClearAllFields()
        {
            DepartamentId = 0;
            SocialWorkerId = 0;
            DepartamentId = 0;
            MedicalWorkerId = 0;
            StatusId = 0;
            HealthId = 0;
            ConditionLivingId = 0;
            Job = String.Empty;
            CategoryList = null;
        }
    }
}
