﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Client
{
    public class ClientDocuments
    {
        public static string PassportSerial { get; set; }
        public static string PassportNumber { get; set; }
        public static string PassportCode { get; set; }
        public static string PassportDate { get; set; }
        public static string PassportIssued { get; set; }

        public static string DisabilityGroup { get; set; }
        public static string DisabilityDegree { get; set; }
        public static string DisabilitySerial { get; set; }
        public static string DisabilityNumber { get; set; }
        public static string DisabilityDate { get; set; }
        public static string DisabilityAct { get; set; }

        public static string CertificatePensionNumber { get; set; }
        public static string CertificatePensionDate { get; set; }
        public static string CertificateDisabledNumber { get; set; }
        public static string CertificateDisabledDate { get; set; }
        public static string CertificateMemberNumber { get; set; }
        public static string CertificateMemberDate { get; set; }
        public static string CertificateVeteranNumber { get; set; }
        public static string CertificateVeteranDate { get; set; }
        public static string CertificateMemberLaborNumber { get; set; }
        public static string CertificateMemberLaborDate { get; set; }

        public static void ClearAllFields()
        {
            PassportSerial = String.Empty;
            PassportNumber = String.Empty;
            PassportCode = String.Empty;
            PassportDate = String.Empty;
            PassportIssued = String.Empty;

            DisabilityGroup = String.Empty;
            DisabilityDegree = String.Empty;
            DisabilitySerial = String.Empty;
            DisabilityNumber = String.Empty;
            DisabilityDate = String.Empty;
            DisabilityAct = String.Empty;

            CertificatePensionNumber = String.Empty;
            CertificatePensionDate = String.Empty;
            CertificateDisabledNumber = String.Empty;
            CertificateDisabledDate = String.Empty;
            CertificateMemberNumber = String.Empty;
            CertificateMemberDate = String.Empty;
            CertificateVeteranNumber = String.Empty;
            CertificateVeteranDate = String.Empty;
            CertificateMemberLaborNumber = String.Empty;
            CertificateMemberLaborDate = String.Empty;
        }
    }
}
