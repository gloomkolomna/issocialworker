﻿using isSocialWorker.Model;
using isSocialWorker.View;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class NewClientInfoHousingPresenter
    {
        private readonly IFormNewClientInfoHousing _view;
        private readonly INewClientInfoHousingModel _model;
        private readonly IMessageService _message;

        public NewClientInfoHousingPresenter(IFormNewClientInfoHousing view, INewClientInfoHousingModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.LoadForm += _view_LoadForm;
            _view.NextButtonClick += _view_NextButtonClick;
            _view.BackButtonClick += _view_BackButtonClick;
        }

        void _view_BackButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientAddress form = new formNewClientAddress();
            NewClientAddressModel model = new NewClientAddressModel();
            MessageService message = new MessageService();

            NewClientAddressPresenter presenter = new NewClientAddressPresenter(form, model, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_NextButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientDocuments form = new formNewClientDocuments();
            NewClientDocumentsModel model = new NewClientDocumentsModel();
            NewClientSaveModel save = new NewClientSaveModel();
            MessageService message = new MessageService();
            NewClientDocumentsPresenter presenter = new NewClientDocumentsPresenter(form, model, save, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_LoadForm(object sender, EventArgs e)
        {
            _view.FillingList = _model.GetDirectory("p_getDirectory_typeHousing");
            _view.TypeHousingFilling();
            _view.FillingList = _model.GetDirectory("p_getDirectory_conditionHousing");
            _view.ConditionHousingFilling();
            _view.FillingList = _model.GetDirectory("p_getDirectory_conditionGas");
            _view.ConditionGasFilling();
            _view.FillingList = _model.GetDirectory("p_getDirectory_conditionWater");
            _view.ConditionWaterFilling();
            _view.FillingList = _model.GetDirectory("p_getDirectory_conditionHeating");
            _view.ConditionHeatingFilling();
            _view.FillingList = _model.GetDirectory("p_getDirectory_conditionSanitary");
            _view.ConditionSanitaryFilling();

            _view.TypeHousingId = _model.GetTypeHousingId();
            _view.HousingArea = _model.GetHousingArea();
            _view.HousingRoom = _model.GetHousingRoom();
            _view.RepairCosmetic = _model.GetRepairCosmetic();
            _view.RepairMajor = _model.GetRepairMajor();
            _view.ConditionGasId = _model.GetConditionGasId();
            _view.ConditionWaterId = _model.GetConditionWaterId();
            _view.ConditionHeatingId = _model.GetConditionHeatingId();
            _view.ConditionSanitaryId = _model.GetConditionSanitaryId();
            _view.ConditionHousingId = _model.GetConditionHousingId();
        }

        private void SetBuffer()
        {
            _model.SetInfoHousingBuffered(_view.TypeHousingId, _view.ConditionHousingId, _view.ConditionGasId, _view.ConditionWaterId, _view.ConditionHeatingId, _view.ConditionSanitaryId,
                _view.HousingArea, _view.HousingRoom, _view.RepairCosmetic, _view.RepairMajor);
        }
    }
}
