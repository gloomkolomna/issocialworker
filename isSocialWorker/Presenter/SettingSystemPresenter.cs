﻿using isSocialWorker.Model;
using isSocialWorker.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class SettingSystemPresenter
    {
        private readonly IFormSettingSystem _view;
        private readonly ISettingSystemModel _model;
        private readonly IMessageService _message;

        public SettingSystemPresenter(IFormSettingSystem view, ISettingSystemModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.SaveSettingsClick += _view_SaveSettingsClick;
            _view.OpenFormLoad += _view_OpenFormLoad;
        }

        void _view_OpenFormLoad(object sender, EventArgs e)
        {
            _view.PathToPhoto = _model.GetPath();
        }

        void _view_SaveSettingsClick(object sender, EventArgs e)
        {
            _model.SavePath(_view.PathToPhoto);
            _message.ShowMessage("Настройки сохранены");
        }
    }
}
