﻿using isSocialWorker.Client;
using isSocialWorker.Directory;
using isSocialWorker.Model;
using isSocialWorker.View;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class NewClientAddressPresenter
    {
        private readonly IFormNewClientAddress _view;
        private readonly INewClientAddressModel _model;
        private readonly IMessageService _message;

        public NewClientAddressPresenter(IFormNewClientAddress view, INewClientAddressModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.LoadForm += _view_LoadForm;
            _view.RegionSelect += _view_RegionSelect;
            _view.CitySelect += _view_CitySelect;
            _view.StreetSelect += _view_StreetSelect;
            _view.HouseSelect += _view_HouseSelect;
            _view.BackButtonClick += _view_BackButtonClick;
            _view.NextButtonClick += _view_NextButtonClick;
        }

        void _view_NextButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientInfoHousing form = new formNewClientInfoHousing();
            NewClientInfoHousingModel model = new NewClientInfoHousingModel();
            MessageService message = new MessageService();

            NewClientInfoHousingPresenter presenter = new NewClientInfoHousingPresenter(form, model, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_BackButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientInfoHead form = new formNewClientInfoHead();
            NewClientInfoHeadModel model = new NewClientInfoHeadModel();
            MessageService message = new MessageService();

            NewClientInfoHeadPresenter presenter = new NewClientInfoHeadPresenter(form, model, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        #region Combobox Selected
        void _view_RegionSelect(object sender, EventArgs e)
        {
            List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_City", _view.RegionId);
            _view.AddressList = list;
            _view.CityFilling();
        }

        void _view_CitySelect(object sender, EventArgs e)
        {
            List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_Street", _view.CityId);
            _view.AddressList = list;
            _view.StreetFilling();
        }

        void _view_StreetSelect(object sender, EventArgs e)
        {
            List<DirectoryDefault> list;
            if (_view.StreetId == 0)
                list = _model.GetDirectory("p_getAddress_House_city", _view.CityId);
            else
                list = _model.GetDirectory("p_getAddress_House_street", _view.StreetId);

            _view.AddressList = list;
            _view.HouseFilling();
        }

        void _view_HouseSelect(object sender, EventArgs e)
        {
            List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_Appartament", _view.HouseId);
            _view.AddressList = list;
            _view.AppartamentFilling();
        }
        #endregion

        void _view_LoadForm(object sender, EventArgs e)
        {
            _view.AddressList = _model.GetDirectory("p_getAddress_Region");
            _view.RegionFilling();

            if (_model.GetRegionId() != 0)
                _view.RegionId = _model.GetRegionId();

            if(_model.GetCityId() != 0)
            {
                List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_City", _view.RegionId);
                _view.AddressList = list;
                _view.CityFilling();
                _view.CityId = _model.GetCityId();
            }

            if(_model.GetStreetId() != 0)
            {
                List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_Street", _view.CityId);
                _view.AddressList = list;
                _view.StreetFilling();
                _view.StreetId = _model.GetStreetId();
            }
            
            if (_model.GetHouseId() != 0)
            {
                if (ClientInfoAddress.StreetId != 0)
                {
                    List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_House_street", _view.StreetId);
                    _view.AddressList = list;
                    _view.HouseFilling();
                    _view.HouseId = _model.GetHouseId();
                }
                else
                {
                    List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_House_city", _view.CityId);
                    _view.AddressList = list;
                    _view.HouseFilling();
                    _view.HouseId = _model.GetHouseId();
                }
            }
            

            if(_model.GetAppartamentId() != 0)
            {
                List<DirectoryDefault> list = _model.GetDirectory("p_getAddress_Appartament", _view.HouseId);
                _view.AddressList = list;
                _view.AppartamentFilling();
                _view.AppartamentId = _model.GetAppartamentId();
            }

            _view.IsRefugee = _model.GetIsRefugee();
            _view.IsHomeless = _model.GetIsHomeless();
        }

        private void SetBuffer()
        {
            _model.SetBufferFields(_view.RegionId, _view.CityId, _view.StreetId, _view.HouseId, _view.AppartamentId, _view.IsRefugee, _view.IsHomeless);
        }
    }
}
