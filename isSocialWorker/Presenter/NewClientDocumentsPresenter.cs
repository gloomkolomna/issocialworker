﻿using isSocialWorker.Client;
using isSocialWorker.Model;
using isSocialWorker.View;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class NewClientDocumentsPresenter
    {
        private readonly IFormNewClientDocuments _view;
        private readonly INewClientDocumentsModel _model;
        private readonly INewClientSaveModel _save;
        private readonly IMessageService _message;

        public NewClientDocumentsPresenter(IFormNewClientDocuments view, INewClientDocumentsModel model, INewClientSaveModel save, IMessageService message)
        {
            _view = view;
            _model = model;
            _save = save;
            _message = message;

            _view.LoadForm += _view_LoadForm;
            
            _view.BackButtonClick += _view_BackButtonClick;
            _view.SaveButtonClick += _view_SaveButtonClick;
        }

        void _view_SaveButtonClick(object sender, EventArgs e)
        {
            SetBuffer();

            int clientid = 0;
            try
            {
                _save.SaveClient(ClientInfo.LastName, ClientInfo.FirstName, ClientInfo.MiddleName, ClientInfo.DateBorn, ClientInfo.Snils, ClientInfo.Phone);
                clientid = _save.GetClientId();
            }
            catch { logs.logger.Warn("-> Ошибка добавления нового клиента"); return; }

            if (clientid > 0)
            {
                _save.SaveClientHead(clientid, ClientInfoHead.DepartamentId, ClientInfoHead.SocialWorkerId, ClientInfoHead.MedicalWorkerId, ClientInfoHead.StatusId,
                    ClientInfoHead.HealthId, ClientInfoHead.ConditionLivingId, ClientInfoHead.Job);
                _save.SaveClientCategory(clientid, ClientInfoHead.CategoryList);

                _save.SaveClientAddress(clientid, ClientInfoAddress.RegionId, ClientInfoAddress.CityId, ClientInfoAddress.StreetId, ClientInfoAddress.HouseId, ClientInfoAddress.AppartamentId, 
                    ClientInfoAddress.IsRefugee, ClientInfoAddress.IsHomeless);
                _save.SaveClientHousing(clientid, ClientInfoHousing.TypeHousingId, ClientInfoHousing.ConditionHousingId, ClientInfoHousing.ConditionGasId, ClientInfoHousing.ConditionWaterId,
                    ClientInfoHousing.ConditionHeatingId, ClientInfoHousing.ConditionSanitaryId, ClientInfoHousing.HousingArea, ClientInfoHousing.HousingRoom, ClientInfoHousing.RepairCosmetic, 
                    ClientInfoHousing.RepairMajor);

                if (ClientDocuments.PassportNumber != "______")
                    _save.SaveClientDocumentsPassport(clientid, ClientDocuments.PassportSerial, ClientDocuments.PassportNumber, ClientDocuments.PassportCode, ClientDocuments.PassportDate, 
                    ClientDocuments.PassportIssued);

                if (ClientDocuments.DisabilityNumber != "_______")
                    _save.SaveClientDocumentsDisability(clientid, ClientDocuments.DisabilityGroup, ClientDocuments.DisabilityDegree, ClientDocuments.DisabilitySerial, ClientDocuments.DisabilityNumber, 
                    ClientDocuments.DisabilityDate, ClientDocuments.DisabilityAct);

                if (ClientDocuments.CertificateDisabledNumber != "")
                    _save.SaveClientCertificateDisabled(clientid, ClientDocuments.CertificateDisabledNumber, ClientDocuments.CertificateDisabledDate);

                if (ClientDocuments.CertificateMemberNumber != "")
                    _save.SaveClientCertificateMember(clientid, ClientDocuments.CertificateMemberNumber, ClientDocuments.CertificateMemberDate);

                if (ClientDocuments.CertificateMemberLaborNumber != "")
                    _save.SaveClientCertificateMemberLabor(clientid, ClientDocuments.CertificateMemberLaborNumber, ClientDocuments.CertificateMemberLaborDate);

                if (ClientDocuments.CertificatePensionNumber != "")
                    _save.SaveClientCertificatePension(clientid, ClientDocuments.CertificatePensionNumber, ClientDocuments.CertificatePensionDate);

                if (ClientDocuments.CertificateVeteranNumber != "")
                    _save.SaveClientCertificateVeteran(clientid, ClientDocuments.CertificateVeteranNumber, ClientDocuments.CertificateVeteranDate);

                _message.ShowMessage("Данные сохранены.");
                _view.Parentform.Close();
            }
            else
            {
                _message.ShowError("Невозможно добавить данные по клиенту!");
            }
        }

        void _view_BackButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientInfoHousing form = new formNewClientInfoHousing();
            NewClientInfoHousingModel model = new NewClientInfoHousingModel();
            MessageService message = new MessageService();
            NewClientInfoHousingPresenter presenter = new NewClientInfoHousingPresenter(form, model, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_LoadForm(object sender, EventArgs e)
        {
            _view.CertificateDisabledDate = _model.GetCertificateDisabledDate();
            _view.CertificateDisabledNumber = _model.GetCertificateDisabledNumber();
            _view.CertificateMemberDate = _model.GetCertificateMemberDate();
            _view.CertificateMemberLaborDate = _model.GetCertificateMemberLaborDate();
            _view.CertificateMemberLaborNumber = _model.GetCertificateMemberLaborNumber();
            _view.CertificateMemberNumber = _model.GetCertificateMemberNumber();
            _view.CertificatePensionDate = _model.GetCertificatePensionDate();
            _view.CertificatePensionNumber = _model.GetCertificatePensionNumber();
            _view.CertificateVeteranDate = _model.GetCertificateVeteranDate();
            _view.CertificateVeteranNumber = _model.GetCertificateVeteranNumber();
            _view.DisabilityAct = _model.GetDisabilityAct();
            _view.DisabilityDate = _model.GetDisabilityDate();
            _view.DisabilityDegree = _model.GetDisabilityDegree();
            _view.DisabilityGroup = _model.GetDisabilityGroup();
            _view.DisabilityNumber = _model.GetDisabilityNumber();
            _view.DisabilitySerial = _model.GetDisabilitySerial();
            _view.PassportDate = _model.GetPassportDate();
            _view.PassportIssued = _model.GetPassportIssued();
            _view.PassportNumber = _model.GetPassportNumber();
            _view.PassportSerial = _model.GetPassportSerial();
        }

        private void SetBuffer()
        {
            _model.SetBufferPassport(_view.PassportSerial, _view.PassportNumber, _view.PassportCode, _view.PassportDate, _view.PassportIssued);
            _model.SetBufferDisability(_view.DisabilityGroup, _view.DisabilityDegree, _view.DisabilitySerial, _view.DisabilityNumber, _view.DisabilityDate, _view.DisabilityAct);
            _model.SetBufferCertificateDisabled(_view.CertificateDisabledNumber, _view.CertificateDisabledDate);
            _model.SetBufferCertificateMember(_view.CertificateMemberNumber, _view.CertificateMemberDate);
            _model.SetBufferCertificateMemberLabor(_view.CertificateMemberLaborNumber, _view.CertificateMemberLaborDate);
            _model.SetBufferCertificatePension(_view.CertificatePensionNumber, _view.CertificatePensionDate);
            _model.SetBufferCertificateVeteran(_view.CertificateVeteranNumber, _view.CertificateVeteranDate);
        }
    }
}
