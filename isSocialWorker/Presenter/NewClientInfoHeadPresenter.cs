﻿using isSocialWorker.Model;
using isSocialWorker.View;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class NewClientInfoHeadPresenter
    {
        private readonly IFormNewClientInfoHead _view;
        private readonly INewClientInfoHeadModel _model;
        private readonly IMessageService _message;

        public NewClientInfoHeadPresenter(IFormNewClientInfoHead view, INewClientInfoHeadModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.LoadInfoHeadFrom += _view_LoadFrom;
            _view.AddCategoryToList += _view_AddCategoryToList;
            _view.RemoveCategoryToList += _view_RemoveCategoryToList;

            _view.BackButtonClick += _view_BackButtonClick;
            _view.NextButtonClick += _view_NextButtonClick;
        }

        void _view_NextButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            formNewClientAddress form = new formNewClientAddress();
            NewClientAddressModel model = new NewClientAddressModel();
            MessageService message = new MessageService();

            NewClientAddressPresenter presenter = new NewClientAddressPresenter(form, model, message);

            _view.Parentform.Hide();
            form.MdiParent = formBasic.ActiveForm;
            form.Show();
        }

        void _view_BackButtonClick(object sender, EventArgs e)
        {
            SetBuffer();
            
        }

        private void SetBuffer()
        {
            _model.SetClientHeadInfoBuffer(_view.DepartamentId, _view.SocialWorkerId, _view.MedicalWorkerId, _view.StatusId, _view.HealthId, _view.ConditionLivingId, _view.Job);
        }
        
        void _view_RemoveCategoryToList(object sender, EventArgs e)
        {
            int index = _view.CategorySelectedId;
            _model.RemoveCategory(index);
            _view.Category = _model.GetCategoryList();
            _view.FillingCategory();
        }

        void _view_AddCategoryToList(object sender, EventArgs e)
        {
            string[] str = _view.CategoryIdName.Split(';');
            bool isCategory = _model.IsCategory(Convert.ToInt32(str[0]));
            if (!isCategory)
            {
                _model.AddCategoryToList(Convert.ToInt32(str[0]), str[1]);
                _view.Category = _model.GetCategoryList();
                _view.FillingCategory();
            }
            else
            {
                _message.ShowExclamation("Категория уже добавлена в список!");
            }
        }

        void _view_LoadFrom(object sender, EventArgs e)
        {
            _view.Lists = _model.GetDirectory("p_getDirectory_departaments");
            _view.DepartamentList();
            _view.Lists = _model.GetDirectory("p_getDirectory_socialworker");
            _view.SocialWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_medicalworker");
            _view.MedicalWorkerList();
            _view.Lists = _model.GetDirectory("p_getDirectory_status");
            _view.StatusList();
            _view.Lists = _model.GetDirectory("p_getDirectory_health");
            _view.HealthList();
            _view.Lists = _model.GetDirectory("p_getDirectory_conditionliving");
            _view.ConditionLivingList();
            _view.Lists = _model.GetDirectory("p_getDirectory_category");
            _view.CategoryList();

            _view.DepartamentId = _model.GetDepartamentId();
            _view.SocialWorkerId = _model.GetSocialWorkerId();
            _view.MedicalWorkerId = _model.GetMedicalWorkerId();
            _view.StatusId = _model.GetStatusId();
            _view.HealthId = _model.GetHealthId();
            _view.ConditionLivingId = _model.GetConditionLivingId();
            _view.Job = _model.GetJob();

            _view.Category = _model.GetCategoryList();
            _view.FillingCategory();
        }
    }
}
