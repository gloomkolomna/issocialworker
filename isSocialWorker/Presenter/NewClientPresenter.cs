﻿using isSocialWorker.Client;
using isSocialWorker.Model;
using isSocialWorker.View;
using isSocialWorker.View.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class NewClientPresenter
    {
        private readonly IFormNewClient _view;
        private readonly INewClientModel _model;
        private readonly IMessageService _message;

        public NewClientPresenter(IFormNewClient view, INewClientModel model, IMessageService message)
        {
            _view = view;
            _model = model;
            _message = message;

            _view.SetBufferedClient += _view_SetBufferedClient;
            _view.LoadClientForm += _view_LoadClientForm;
        }

        void _view_LoadClientForm(object sender, EventArgs e)
        {
            _view.LastName = _model.GetLastName();
            _view.FirstName = _model.GetFirstName();
            _view.MiddleName = _model.GetMiddleName();
            _view.DateBorn = _model.GetDateBorn();
            _view.Snils = _model.GetSnils();
            _view.Phone = _model.GetPhone();
        }

        void _view_SetBufferedClient(object sender, EventArgs e)
        {
            bool isExistClient = _model.IsExistClient(_view.FirstName, _view.LastName, _view.MiddleName, _view.DateBorn);
            bool isEmptyFields = _model.IsEmptyFields(_view.FirstName, _view.LastName, _view.MiddleName, _view.Snils);
            bool isExistSnils = _model.IsExistSnils(_view.Snils);

            if(isEmptyFields)
            {
                _message.ShowError("Заполните все поля!");
                return;
            }

            if (isExistSnils)
            {
                _message.ShowError("Данный СНИЛС уже есть в базе!");
                return;
            }

            if (!isExistClient) 
            {
                //var client = new NewClient(_view.FirstName, _view.LastName, _view.MiddleName, _view.DateBorn, _view.Snils, _view.Phone);
                //_model.AddClientToDatabase(_view.FirstName, _view.LastName, _view.MiddleName, _view.DateBorn, _view.Snils, _view.Phone, _view.Photolink);
                _model.SetClientBuffer(_view.FirstName, _view.LastName, _view.MiddleName, _view.DateBorn, _view.Snils, _view.Phone);
                formNewClientInfoHead form = new formNewClientInfoHead();
                NewClientInfoHeadModel model = new NewClientInfoHeadModel();
                MessageService message = new MessageService();

                NewClientInfoHeadPresenter presenter = new NewClientInfoHeadPresenter(form, model, message);
                
                _view.Parentform.Hide();
                form.MdiParent = formBasic.ActiveForm;
                form.Show();
                

            }
            else
            {
                _message.ShowExclamation("Клиент с такими данными уже существует!");
            }
        }
    }
}
