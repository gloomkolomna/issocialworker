﻿using isSocialWorker.Model;
using isSocialWorker.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace isSocialWorker.Presenter
{
    public class ConnectPresenter
    {
        private readonly ISettingsForm _view;
        private readonly ISettingConnectModel _model;
        private readonly IMessageService _messageService;

        public ConnectPresenter(ISettingsForm view, ISettingConnectModel model, IMessageService messageService)
        {
            _view = view;
            _model = model;
            _messageService = messageService;

            _view.SettingsOpenForm += _view_SettingsOpenForm;
            _view.SettingsSaveClick += _view_SettingsSaveClick;
        }

        void _view_SettingsOpenForm(object sender, EventArgs e)
        {
            string server = _model.GetServer();
            string user = _model.GetUser();
            string password = _model.GetPassword();
            string dbname = _model.GetDbname();

            _view.Server = server;
            _view.User = user;
            _view.Password = password;
            _view.Dbname = dbname;
        }

        void _view_SettingsSaveClick(object sender, EventArgs e)
        {
            string server = _view.Server;
            string user = _view.User;
            string password = _view.Password;
            string dbname = _view.Dbname;

            _model.SaveConnect(server, user, password, dbname);
        }
    }
}
