-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.6.19-log - MySQL Community Server (GPL)
-- ОС Сервера:                   Win64
-- HeidiSQL Версия:              9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры базы данных issocial
CREATE DATABASE IF NOT EXISTS `issocial` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `issocial`;


-- Дамп структуры для таблица issocial.tbl_address_appartaments
CREATE TABLE IF NOT EXISTS `tbl_address_appartaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `houseid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адреса: квартиры';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_address_city
CREATE TABLE IF NOT EXISTS `tbl_address_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regionid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адреса: населенные пункты';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_address_house
CREATE TABLE IF NOT EXISTS `tbl_address_house` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityid` int(11) DEFAULT NULL,
  `streetid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адреса: дома';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_address_region
CREATE TABLE IF NOT EXISTS `tbl_address_region` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адреса: регионы';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_address_street
CREATE TABLE IF NOT EXISTS `tbl_address_street` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cityid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адреса: улицы';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients
CREATE TABLE IF NOT EXISTS `tbl_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `middlename` varchar(50) DEFAULT NULL,
  `dateborn` date DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `snils` char(14) DEFAULT '000-000-000 00',
  `isDead` tinyint(4) DEFAULT '0',
  `isDisposal` tinyint(4) DEFAULT '0',
  `isSuspend` tinyint(4) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица с данными о клиентах';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_address
CREATE TABLE IF NOT EXISTS `tbl_clients_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `address` varchar(50) DEFAULT NULL,
  `isRefugee` tinyint(4) DEFAULT '0',
  `isHomeless` tinyint(4) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Адрес проживания клиентов';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_category
CREATE TABLE IF NOT EXISTS `tbl_clients_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица клиентов в категориях';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_departaments
CREATE TABLE IF NOT EXISTS `tbl_clients_departaments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `departamentId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Клиенты в отделениях';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_certificatedisabled
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_certificatedisabled` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: инвалиды ВОВ';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_certificatemember
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_certificatemember` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: участники ВОВ';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_certificatememberlabor
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_certificatememberlabor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: участники труд. фронта';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_certificatepension
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_certificatepension` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: пенсионные удостоверения';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_certificateveteran
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_certificateveteran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `number` varchar(50) DEFAULT NULL,
  `date` date DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: ветераны ВОВ';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_disability
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_disability` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `groupinv` char(1) DEFAULT NULL,
  `degree` char(1) DEFAULT NULL,
  `serial` varchar(10) DEFAULT NULL,
  `number` varchar(10) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `act` varchar(50) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Документы клиентов: инвалидность';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_documents_passport
CREATE TABLE IF NOT EXISTS `tbl_clients_documents_passport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `serial` varchar(5) DEFAULT NULL,
  `number` varchar(6) DEFAULT NULL,
  `code` varchar(7) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `issued` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Клиенты: паспорта';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_health
CREATE TABLE IF NOT EXISTS `tbl_clients_health` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `healthId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица соответствия клиентов и справочника состояния здоровья';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_housing
CREATE TABLE IF NOT EXISTS `tbl_clients_housing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT '0',
  `typehousingid` int(11) DEFAULT '0',
  `housingarea` double DEFAULT '0',
  `housingroom` int(11) DEFAULT '0',
  `conditionhousingid` int(11) DEFAULT '0',
  `conditiongasid` int(11) DEFAULT '0',
  `conditionwaterid` int(11) DEFAULT '0',
  `conditionheatingid` int(11) DEFAULT '0',
  `conditionsanitaryid` int(11) DEFAULT '0',
  `isrepaircosmetic` tinyint(4) DEFAULT '0',
  `isrepairmajor` tinyint(4) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица клиентов и их жилья';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_income
CREATE TABLE IF NOT EXISTS `tbl_clients_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `typeIncomeId` int(11) DEFAULT NULL,
  `summ` double DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица доходов клиентов, загружаемая из внешних источников и из программы';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_job
CREATE TABLE IF NOT EXISTS `tbl_clients_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Клиенты и работа';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_livingcondition
CREATE TABLE IF NOT EXISTS `tbl_clients_livingcondition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `livingId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Клиенты и оценка быт. условий';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_medicalworker
CREATE TABLE IF NOT EXISTS `tbl_clients_medicalworker` (
  `id` int(11) NOT NULL,
  `clientId` int(11) DEFAULT NULL,
  `medicalId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='Таблица соотношения клиентов с медработниками';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_report_guaranteed
CREATE TABLE IF NOT EXISTS `tbl_clients_report_guaranteed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientid` int(11) DEFAULT NULL,
  `departamentid` int(11) DEFAULT NULL,
  `socialworkerid` int(11) DEFAULT NULL,
  `report_month` int(11) DEFAULT NULL,
  `report_year` int(11) DEFAULT NULL,
  `summ_total` double DEFAULT NULL,
  `summ_pay` double DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица отчетности гарантированных услуг';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_report_guaranteed_details
CREATE TABLE IF NOT EXISTS `tbl_clients_report_guaranteed_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reportid` int(11) DEFAULT NULL,
  `serviceid` int(11) DEFAULT NULL,
  `count` int(11) DEFAULT NULL,
  `summ_service` double DEFAULT NULL,
  `summ_client` double DEFAULT NULL,
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица отчетности гарантированных услуг: детализация по каждой услуги';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_socialworker
CREATE TABLE IF NOT EXISTS `tbl_clients_socialworker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `socworkerId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица соотношения клиентов с соцработниками';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_status
CREATE TABLE IF NOT EXISTS `tbl_clients_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `statusId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица соотношения клиентов и статусов';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_clients_terms_payments
CREATE TABLE IF NOT EXISTS `tbl_clients_terms_payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `clientId` int(11) DEFAULT NULL,
  `paymentsId` int(11) DEFAULT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Условия оплаты для клиентов.';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_additional_services
CREATE TABLE IF NOT EXISTS `tbl_directory_additional_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` char(10) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: дополнительные услуги';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_address
CREATE TABLE IF NOT EXISTS `tbl_directory_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица адресов. name = район;населенный пункт;улица;дом;квартира (пример: 111;222;333;444;555)';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_category
CREATE TABLE IF NOT EXISTS `tbl_directory_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: Категории граждан';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_condition_gas
CREATE TABLE IF NOT EXISTS `tbl_directory_condition_gas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: Условия проживания: Газ';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_condition_heating
CREATE TABLE IF NOT EXISTS `tbl_directory_condition_heating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: условия проживания: отопление';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_condition_housing
CREATE TABLE IF NOT EXISTS `tbl_directory_condition_housing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: состояние жилья';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_condition_sanitary
CREATE TABLE IF NOT EXISTS `tbl_directory_condition_sanitary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: санитарно-гигиенические условия';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_condition_water
CREATE TABLE IF NOT EXISTS `tbl_directory_condition_water` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: условия проживания: вода';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_departament
CREATE TABLE IF NOT EXISTS `tbl_directory_departament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: Отделения';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_guaranteed_services
CREATE TABLE IF NOT EXISTS `tbl_directory_guaranteed_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `price` char(10) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: гарантированные услуги';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_living_conditions
CREATE TABLE IF NOT EXISTS `tbl_directory_living_conditions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: бытовые условия';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_medical_worker
CREATE TABLE IF NOT EXISTS `tbl_directory_medical_worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: таблица медицинских работников';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_residence_status
CREATE TABLE IF NOT EXISTS `tbl_directory_residence_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: статус проживания';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_social_worker
CREATE TABLE IF NOT EXISTS `tbl_directory_social_worker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: таблица соцработников';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_state_health
CREATE TABLE IF NOT EXISTS `tbl_directory_state_health` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: состояние здоровья';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_systems_cost_living
CREATE TABLE IF NOT EXISTS `tbl_directory_systems_cost_living` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: Таблица системных значений: прожиточный минимум';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_terms_payment
CREATE TABLE IF NOT EXISTS `tbl_directory_terms_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `percent` int(11) DEFAULT '0',
  `koef` varchar(5) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: условия оплаты с коэффициентами';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_type_housing
CREATE TABLE IF NOT EXISTS `tbl_directory_type_housing` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: вид жилья';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_directory_type_income
CREATE TABLE IF NOT EXISTS `tbl_directory_type_income` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Справочник: виды доходов';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_socialworker_departament
CREATE TABLE IF NOT EXISTS `tbl_socialworker_departament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socworkerId` int(11) DEFAULT NULL,
  `departamentId` int(11) DEFAULT NULL,
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица связей соцработников с отделениями';

-- Экспортируемые данные не выделены.


-- Дамп структуры для таблица issocial.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `access` int(11) DEFAULT '0',
  KEY `Индекс 1` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Экспортируемые данные не выделены.


-- Дамп структуры для процедура issocial.p_existClient
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_existClient`(IN `$firstname` VARCHAR(50), IN `$lastname` VARCHAR(50), IN `$middlename` VARCHAR(50), IN `$dateborn` VARCHAR(50))
    COMMENT 'Возвращает количество клиентов'
BEGIN
select count(tbl_clients.id) from tbl_clients where 
(tbl_clients.firstname = $firstname) AND
(tbl_clients.lastname = $lastname) AND 
(tbl_clients.middlename = $middlename) AND 
(tbl_clients.dateborn = $dateborn);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_existSnils
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_existSnils`(IN `$snils` VARCHAR(50))
    COMMENT 'Проверка на СНИЛС'
BEGIN
select COUNT(tbl_clients.id) from tbl_clients where tbl_clients.snils = $snils;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_Appartament
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_Appartament`(IN `$id` INT)
BEGIN
select id, name from tbl_address_appartaments where houseid = $id;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_City
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_City`(IN `$id` INT)
BEGIN
select id, name from tbl_address_city where regionid = $id;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_House_city
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_House_city`(IN `$id` INT)
BEGIN
select id, name from tbl_address_house where cityid = $id;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_House_street
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_House_street`(IN `$id` INT)
BEGIN
select id, name from tbl_address_house where streetid = $id;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_Region
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_Region`()
BEGIN
select id, name from tbl_address_region;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAddress_Street
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAddress_Street`(IN `$id` INT)
BEGIN
select id, name from tbl_address_street where cityid = $id;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAnketa_address
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAnketa_address`(IN `$clientid` INT)
BEGIN
select 
tbl_clients_address.address, 
tbl_clients_address.isRefugee, 
tbl_clients_address.isHomeless 
from tbl_clients_address 
WHERE tbl_clients_address.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAnketa_head
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAnketa_head`(IN `$clientid` INT)
BEGIN
SELECT 
ifnull(tbl_clients_departaments.departamentId,0) as departamentId, 
ifnull(tbl_clients_health.healthId,0) as healthId, 
ifnull(tbl_clients_job.job, "") as job, 
ifnull(tbl_clients_livingcondition.livingId,0) as livingId, 
ifnull(tbl_clients_medicalworker.medicalId,0) as medicalId, 
ifnull(tbl_clients_socialworker.socworkerId,0) as socworkerId, 
ifnull(tbl_clients_status.statusId,0) as statusId 
FROM ((((((tbl_clients LEFT JOIN tbl_clients_departaments ON tbl_clients.id = tbl_clients_departaments.clientId) LEFT JOIN tbl_clients_health ON tbl_clients.id = tbl_clients_health.clientId) LEFT JOIN tbl_clients_job ON tbl_clients.id = tbl_clients_job.clientId) LEFT JOIN tbl_clients_livingcondition ON tbl_clients.id = tbl_clients_livingcondition.clientId) LEFT JOIN tbl_clients_medicalworker ON tbl_clients.id = tbl_clients_medicalworker.clientId) LEFT JOIN tbl_clients_socialworker ON tbl_clients.id = tbl_clients_socialworker.clientId) LEFT JOIN tbl_clients_status ON tbl_clients.id = tbl_clients_status.clientId
WHERE tbl_clients.id = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAnketa_housing
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAnketa_housing`(IN `$clientid` INT)
BEGIN
select
ifnull(tbl_clients_housing.typehousingid,0) as typehousingid, 
tbl_clients_housing.housingarea,
tbl_clients_housing.housingroom, 
ifnull(tbl_clients_housing.conditionhousingid,0) as conditionhousingid,
ifnull(tbl_clients_housing.conditiongasid,0) as conditiongasid,
ifnull(tbl_clients_housing.conditionwaterid,0) as conditionwaterid,
ifnull(tbl_clients_housing.conditionheatingid,0) as conditionheatingid,
ifnull(tbl_clients_housing.conditionsanitaryid,0) as conditionsanitaryid,
tbl_clients_housing.isrepaircosmetic,
tbl_clients_housing.isrepairmajor
from tbl_clients_housing
where tbl_clients_housing.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getAnketa_info
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getAnketa_info`(IN `$clientid` INT)
BEGIN
select 
tbl_clients.lastname, 
tbl_clients.firstname,
tbl_clients.middlename,
tbl_clients.dateborn,
tbl_clients.snils,
tbl_clients.phone,
tbl_clients.isDead,
tbl_clients.isDisposal,
tbl_clients.isSuspend 
from tbl_clients 
where tbl_clients.id = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrentCategory
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrentCategory`(IN `$clientid` INT)
BEGIN
SELECT 
tbl_directory_category.id, 
tbl_directory_category.name 
FROM tbl_clients_category 
LEFT JOIN tbl_directory_category ON tbl_clients_category.categoryId = tbl_directory_category.id 
where tbl_clients_category.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocDisability
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocDisability`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_disability.groupinv,
tbl_clients_documents_disability.degree,
tbl_clients_documents_disability.serial,
tbl_clients_documents_disability.number,
tbl_clients_documents_disability.date,
tbl_clients_documents_disability.act
from tbl_clients_documents_disability
where
tbl_clients_documents_disability.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocDisabled
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocDisabled`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_certificatedisabled.number,
tbl_clients_documents_certificatedisabled.date
from tbl_clients_documents_certificatedisabled
WHERE tbl_clients_documents_certificatedisabled.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocMember
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocMember`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_certificatemember.number,
tbl_clients_documents_certificatemember.date
from tbl_clients_documents_certificatemember
WHERE tbl_clients_documents_certificatemember.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocMemberLabor
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocMemberLabor`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_certificatememberlabor.number,
tbl_clients_documents_certificatememberlabor.date
from tbl_clients_documents_certificatememberlabor
WHERE tbl_clients_documents_certificatememberlabor.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocPassport
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocPassport`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_passport.serial,
tbl_clients_documents_passport.number,
tbl_clients_documents_passport.code,
tbl_clients_documents_passport.date,
tbl_clients_documents_passport.issued
From tbl_clients_documents_passport
WHERE tbl_clients_documents_passport.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocPension
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocPension`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_certificatepension.number,
tbl_clients_documents_certificatepension.date
from tbl_clients_documents_certificatepension
WHERE tbl_clients_documents_certificatepension.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientCurrent_DocVeteran
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientCurrent_DocVeteran`(IN `$clientid` INT)
BEGIN
select
tbl_clients_documents_certificateveteran.number,
tbl_clients_documents_certificateveteran.date
from tbl_clients_documents_certificateveteran
WHERE tbl_clients_documents_certificateveteran.clientid = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientId
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientId`()
BEGIN
select MAX(id) from tbl_clients;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientList
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientList`()
BEGIN
SELECT 
tbl_clients.id, 
tbl_clients.lastname, 
tbl_clients.firstname, 
tbl_clients.middlename, 
tbl_clients.dateborn, 
tbl_clients.phone, 
tbl_clients.snils, 
tbl_clients.isDead, 
tbl_clients.isDisposal, 
tbl_clients.isSuspend, 
ifnull(tbl_clients_departaments.departamentId,0) as departamentId, 
ifnull(tbl_clients_medicalworker.medicalId,0) as medicalId, 
ifnull(tbl_clients_socialworker.socworkerId,0) as socworkerId, 
ifnull(tbl_clients_status.statusId,0) as statusId 
FROM 
(((tbl_clients LEFT JOIN tbl_clients_departaments ON tbl_clients.id = tbl_clients_departaments.clientId) LEFT JOIN tbl_clients_medicalworker ON tbl_clients.id = tbl_clients_medicalworker.clientId) LEFT JOIN tbl_clients_socialworker ON tbl_clients.id = tbl_clients_socialworker.clientId) LEFT JOIN tbl_clients_status ON tbl_clients.id = tbl_clients_status.clientId;

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getClientListCategory
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getClientListCategory`(IN `$clientid` INT)
BEGIN
SELECT 
ifnull(tbl_clients_category.categoryId,0) as category 
FROM tbl_clients_category WHERE tbl_clients_category.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_category
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_category`()
BEGIN
select id, name from tbl_directory_category;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionGas
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionGas`()
BEGIN
select id, name from tbl_directory_condition_gas;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionHeating
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionHeating`()
BEGIN
select id, name from tbl_directory_condition_heating;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionHousing
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionHousing`()
BEGIN
select id, name from tbl_directory_condition_housing;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionliving
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionliving`()
BEGIN
select id, name from tbl_directory_living_conditions;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionSanitary
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionSanitary`()
BEGIN
select id, name from tbl_directory_condition_sanitary;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_conditionWater
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_conditionWater`()
BEGIN
select id, name from tbl_directory_condition_water;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_departaments
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_departaments`()
    COMMENT 'Получение списка отделений'
BEGIN
select id, name from tbl_directory_departament;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_health
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_health`()
BEGIN
select id, name from tbl_directory_state_health;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_medicalworker
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_medicalworker`()
BEGIN
select id, name from tbl_directory_medical_worker;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_socialworker
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_socialworker`()
BEGIN
select id, name from tbl_directory_social_worker ORDER BY name ASC;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_status
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_status`()
BEGIN
select id, name from tbl_directory_residence_status;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getDirectory_typeHousing
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getDirectory_typeHousing`()
BEGIN
select id, name from tbl_directory_type_housing;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getPhotolink
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getPhotolink`(IN `$snils` CHAR(14))
    COMMENT 'Возвращает ссылку на местонахождения фото'
BEGIN
select tbl_clients.photolink from tbl_clients where tbl_clients.snils = $snils;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_departamentname
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_departamentname`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
SELECT tbl_directory_departament.name
FROM tbl_clients_report_guaranteed INNER JOIN tbl_directory_departament ON tbl_clients_report_guaranteed.departamentid = tbl_directory_departament.id
WHERE 
tbl_clients_report_guaranteed.clientid = $clientid
AND
tbl_clients_report_guaranteed.report_month = $report_month 
And
tbl_clients_report_guaranteed.report_year = $report_year;

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_guaranteed
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_guaranteed`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
SELECT 
tbl_clients_report_guaranteed_details.serviceid, 
tbl_clients_report_guaranteed_details.count,  
tbl_clients_report_guaranteed_details.summ_service, 
tbl_clients_report_guaranteed_details.summ_client, 
tbl_directory_guaranteed_services.name
FROM (tbl_clients_report_guaranteed RIGHT JOIN tbl_clients_report_guaranteed_details ON tbl_clients_report_guaranteed.id = tbl_clients_report_guaranteed_details.reportid) LEFT JOIN tbl_directory_guaranteed_services ON tbl_clients_report_guaranteed_details.serviceid = tbl_directory_guaranteed_services.id
WHERE 
tbl_clients_report_guaranteed.report_month = $report_month
AND 
tbl_clients_report_guaranteed.report_year = $report_year
AND 
tbl_clients_report_guaranteed.clientid = $clientid; 

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_paymentsterm
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_paymentsterm`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
SELECT tbl_directory_terms_payment.name
FROM tbl_clients_terms_payments INNER JOIN tbl_directory_terms_payment ON tbl_clients_terms_payments.paymentsId = tbl_directory_terms_payment.id
WHERE 
tbl_clients_terms_payments.clientId=$clientid
AND 
tbl_clients_terms_payments.month=$report_month
AND 
tbl_clients_terms_payments.year=$report_year;

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_summpay
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_summpay`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_report_guaranteed 
where
tbl_clients_report_guaranteed.clientid=$clientid
AND
tbl_clients_report_guaranteed.report_month=$report_month
AND
tbl_clients_report_guaranteed.report_year=$report_year;

if(counts > 0) then 
SELECT tbl_clients_report_guaranteed.summ_pay 
FROM tbl_clients_report_guaranteed
WHERE 
tbl_clients_report_guaranteed.clientid=$clientid
AND
tbl_clients_report_guaranteed.report_month=$report_month
AND
tbl_clients_report_guaranteed.report_year=$report_year;
else
select 0;
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_summtotal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_summtotal`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_report_guaranteed 
where
tbl_clients_report_guaranteed.clientid=$clientid
AND
tbl_clients_report_guaranteed.report_month=$report_month
AND
tbl_clients_report_guaranteed.report_year=$report_year;

if(counts > 0) then 
SELECT tbl_clients_report_guaranteed.summ_total 
FROM tbl_clients_report_guaranteed
WHERE 
tbl_clients_report_guaranteed.clientid=$clientid
AND
tbl_clients_report_guaranteed.report_month=$report_month
AND
tbl_clients_report_guaranteed.report_year=$report_year;
else
select 0;
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getReportClient_workername
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getReportClient_workername`(IN `$clientid` INT, IN `$report_month` INT, IN `$report_year` INT)
BEGIN
SELECT tbl_directory_social_worker.name
FROM tbl_clients_report_guaranteed INNER JOIN tbl_directory_social_worker ON tbl_clients_report_guaranteed.socialworkerid = tbl_directory_social_worker.id
WHERE 
tbl_clients_report_guaranteed.clientid = $clientid 
AND
tbl_clients_report_guaranteed.report_month = $report_month 
AND
tbl_clients_report_guaranteed.report_year = $report_year 
;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_getUsers
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_getUsers`()
    COMMENT 'Процедура возвращает список пользователей'
BEGIN
Select tbl_users.id, tbl_users.username, tbl_users.password, tbl_users.access from tbl_users;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClient
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClient`(IN `$lastname` VARCHAR(50), IN `$firstname` VARCHAR(50), IN `$middlename` VARCHAR(50), IN `$dateborn` DATE, IN `$snils` CHAR(14), IN `$phone` VARCHAR(50))
    COMMENT 'Добавление нового клиента'
BEGIN
insert into tbl_clients (
tbl_clients.lastname,
tbl_clients.firstname,
tbl_clients.middlename,
tbl_clients.dateborn,
tbl_clients.snils,
tbl_clients.phone) 
VALUES (
$lastname,
$firstname,
$middlename,
$dateborn,
$snils,
$phone);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientAddress
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientAddress`(IN `$clientid` INT, IN `$address` VARCHAR(50), IN `$isrefugee` BIT, IN `$ishomeless` BIT)
BEGIN
insert into tbl_clients_address (clientId, address, isRefugee, isHomeless) 
values ($clientid, $address, $isrefugee, $ishomeless);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientCategory
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientCategory`(IN `$clientid` INT, IN `$categoryid` INT)
BEGIN
insert into tbl_clients_category (clientId, categoryId) 
values ($clientid, $categoryid);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_certificatedisabled
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_certificatedisabled`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN

insert into tbl_clients_documents_certificatedisabled (clientid, number, date) 
values ($clientid, $number, $date);

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_certificatemember
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_certificatemember`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN

insert into tbl_clients_documents_certificatemember (clientid, number, date) 
values ($clientid, $number, $date);

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_certificatememberlabor
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_certificatememberlabor`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN

insert into tbl_clients_documents_certificatememberlabor (clientid, number, date) 
values ($clientid, $number, $date);

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_certificatepension
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_certificatepension`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN

insert into tbl_clients_documents_certificatepension (clientid, number, date) 
values ($clientid, $number, $date);

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_certificateveteran
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_certificateveteran`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN

insert into tbl_clients_documents_certificateveteran (clientid, number, date) 
values ($clientid, $number, $date);

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_disability
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_disability`(IN `$clientid` INT, IN `$groupinv` CHAR(1), IN `$degree` CHAR(1), IN `$serial` VARCHAR(10), IN `$number` VARCHAR(10), IN `$date` DATE, IN `$act` VARCHAR(50))
BEGIN
insert into tbl_clients_documents_disability (clientid, groupinv, degree, serial, number, date, act) 
values ($clientid, $groupinv, $degree, $serial, $number, $date, $act);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientDocuments_passport
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientDocuments_passport`(IN `$clientid` INT, IN `$serial` VARCHAR(5), IN `$number` VARCHAR(6), IN `$code` VARCHAR(7), IN `$date` DATE, IN `$issued` VARCHAR(255))
BEGIN
insert into tbl_clients_documents_passport (clientid, serial, number, code, date, issued) 
values ($clientid, $serial, $number, $code, $date, $issued);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientHead
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientHead`(IN `$clientid` INT, IN `$departamentid` INT, IN `$socworkerid` INT, IN `$medicalworkerid` INT, IN `$statusid` INT, IN `$healthid` INT, IN `$conditionlivingid` INT, IN `$job` VARCHAR(255))
BEGIN

# Добавление нового клиента в отделение
insert into tbl_clients_departaments (clientId, departamentId) 
values ($clientid, $departamentid);

# Добавление нового клиента к соцработнику
insert into tbl_clients_socialworker (clientId, socworkerId) 
values ($clientid, $socworkerid);

# Добавление нового клиента к медработнику
if($medicalworkerid > 0) then
	insert into tbl_clients_medicalworker (clientId, medicalId) 
	values ($clientid, $medicalworkerid);
end if;

# Добавление нового клиента и статуса
insert into tbl_clients_status (clientId, statusId) 
values ($clientid, $statusid);

# Добавление нового клиента и здоровья
insert into tbl_clients_health (clientId, healthId) 
values ($clientid, $healthid);

# Добавление нового клиента и быт.условий
insert into tbl_clients_livingcondition (clientId, livingId) 
values ($clientid, $conditionlivingid);

# Добавление нового клиента и работы
if($job != "") then
	insert into tbl_clients_job (clientId, job) 
	values ($clientid, $job);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_saveClientHousing
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_saveClientHousing`(IN `$clientid` INT, IN `$typehousingid` INT, IN `$conditionhousingid` INT, IN `$conditiongasid` INT, IN `$conditionwaterid` INT, IN `$conditionheatingid` INT, IN `$conditionsanitaryid` INT, IN `$housingarea` DOUBLE, IN `$housingroom` INT, IN `$repaircosmetic` BIT, IN `$repairmajor` BIT)
BEGIN
insert into tbl_clients_housing (clientid, typehousingid, conditionhousingid, conditiongasid, conditionwaterid, conditionheatingid, conditionsanitaryid, housingarea, housingroom, isrepaircosmetic, isrepairmajor) 
values ($clientid, $typehousingid, $conditionhousingid, $conditiongasid, $conditionwaterid, $conditionheatingid, $conditionsanitaryid, $housingarea, $housingroom, $repaircosmetic, $repairmajor);
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_Address
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_Address`(IN `$clientid` INT, IN `$address` VARCHAR(50), IN `$isRefugee` INT, IN `$isHomeless` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_address 
where tbl_clients_address.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_address set
	tbl_clients_address.address = $address,
	tbl_clients_address.isRefugee = $isRefugee,
	tbl_clients_address.isHomeless = $isHomeless
	where tbl_clients_address.clientId = $clientid;
else
	insert into tbl_clients_category(clientId, address, isRefugee, isHomeless) 
	values ($clientid, $address, $isRefugee, $isHomeless);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_Dead
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_Dead`(IN `$actions` INT, IN `$clientid` INT)
BEGIN
update tbl_clients set 
tbl_clients.isDead = $actions
where tbl_clients.id = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_Disposal
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_Disposal`(IN `$actions` INT, IN `$clientid` INT)
BEGIN
update tbl_clients set 
tbl_clients.isDisposal = $actions
where tbl_clients.id = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocDisability
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocDisability`(IN `$clientid` INT, IN `$serial` VARCHAR(10), IN `$groupinv` CHAR(1), IN `$degree` CHAR(1), IN `$number` VARCHAR(10), IN `$date` DATE, IN `$act` VARCHAR(50))
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_disability 
where tbl_clients_documents_disability.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_disability set
tbl_clients_documents_disability.serial = $serial,
tbl_clients_documents_disability.number = $number,
tbl_clients_documents_disability.groupinv = $groupinv,
tbl_clients_documents_disability.degree = $degree,
tbl_clients_documents_disability.date = $date,
tbl_clients_documents_disability.act = $act
where tbl_clients_documents_disability.clientid = $clientid;
else
insert into tbl_clients_documents_disability 
(clientid, serial, number, groupinv, degree, date, act) 
VALUES
($clientid, $serial, $number, $groupinv, $degree, $date, $act);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocDisabled
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocDisabled`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_certificatedisabled
where tbl_clients_documents_certificatedisabled.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_certificatedisabled set
tbl_clients_documents_certificatedisabled.number = $number,
tbl_clients_documents_certificatedisabled.date = $date
where tbl_clients_documents_certificatedisabled.clientid = $clientid;
else
insert into tbl_clients_documents_certificatedisabled 
(clientid, number, date) 
VALUES
($clientid, $number, $date);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocMember
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocMember`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_certificatemember
where tbl_clients_documents_certificatemember.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_certificatemember set
tbl_clients_documents_certificatemember.number = $number,
tbl_clients_documents_certificatemember.date = $date
where tbl_clients_documents_certificatemember.clientid = $clientid;
else
insert into tbl_clients_documents_certificatemember 
(clientid, number, date) 
VALUES
($clientid, $number, $date);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocMemberLabor
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocMemberLabor`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_certificatememberlabor
where tbl_clients_documents_certificatememberlabor.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_certificatememberlabor set
tbl_clients_documents_certificatememberlabor.number = $number,
tbl_clients_documents_certificatememberlabor.date = $date
where tbl_clients_documents_certificatememberlabor.clientid = $clientid;
else
insert into tbl_clients_documents_certificatememberlabor 
(clientid, number, date) 
VALUES
($clientid, $number, $date);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocPassport
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocPassport`(IN `$clientid` INT, IN `$serial` VARCHAR(5), IN `$number` VARCHAR(6), IN `$code` VARCHAR(7), IN `$date` DATE, IN `$issued` VARCHAR(255))
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_passport 
where tbl_clients_documents_passport.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_passport set
tbl_clients_documents_passport.serial = $serial,
tbl_clients_documents_passport.number = $number,
tbl_clients_documents_passport.code = $code,
tbl_clients_documents_passport.date = $date,
tbl_clients_documents_passport.issued = $issued
where tbl_clients_documents_passport.clientid = $clientid;
else
insert into tbl_clients_documents_passport 
(clientid, serial, number, code, date, issued) 
VALUES
($clientid, $serial, $number, $code, $date, $issued);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocPension
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocPension`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_certificatepension
where tbl_clients_documents_certificatepension.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_certificatepension set
tbl_clients_documents_certificatepension.number = $number,
tbl_clients_documents_certificatepension.date = $date
where tbl_clients_documents_certificatepension.clientid = $clientid;
else
insert into tbl_clients_documents_certificatepension 
(clientid, number, date) 
VALUES
($clientid, $number, $date);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_DocVeteran
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_DocVeteran`(IN `$clientid` INT, IN `$number` VARCHAR(50), IN `$date` DATE)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_documents_certificateveteran
where tbl_clients_documents_certificateveteran.clientid = $clientid;

if(counts > 0) then
update tbl_clients_documents_certificateveteran set
tbl_clients_documents_certificateveteran.number = $number,
tbl_clients_documents_certificateveteran.date = $date
where tbl_clients_documents_certificateveteran.clientid = $clientid;
else
insert into tbl_clients_documents_certificateveteran 
(clientid, number, date) 
VALUES
($clientid, $number, $date);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadCategory
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadCategory`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_category 
where tbl_clients_category.clientId = $clientid 
AND tbl_clients_category.categoryId = $upd_id;

if(counts > 0) then
	update tbl_clients_category set
	tbl_clients_category.categoryId = $upd_id
	where tbl_clients_category.clientId = $clientid;
else
	insert into tbl_clients_category(clientId, categoryId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadConditionLiving
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadConditionLiving`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_livingcondition 
where tbl_clients_livingcondition.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_livingcondition set
	tbl_clients_livingcondition.livingId = $upd_id
	where tbl_clients_livingcondition.clientId = $clientid;
else
	insert into tbl_clients_livingcondition(clientId, livingId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadDepartament
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadDepartament`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_departaments 
where tbl_clients_departaments.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_departaments set
	tbl_clients_departaments.departamentId = $upd_id
	where tbl_clients_departaments.clientId = $clientid;
else
	insert into tbl_clients_departaments(clientId, departamentId) 
	values ($clientid, $upd_id);
end if;

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadHealting
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadHealting`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_health 
where tbl_clients_health.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_health set
	tbl_clients_health.healthId = $upd_id
	where tbl_clients_health.clientId = $clientid;
else
	insert into tbl_clients_health(clientId, healthId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadJob
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadJob`(IN `$clientid` INT, IN `$job` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_job 
where tbl_clients_job.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_job set
	tbl_clients_job.job = $job
	where tbl_clients_job.clientId = $clientid;
else
	insert into tbl_clients_job(clientId, job) 
	values ($clientid, $job);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadMedicalworker
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadMedicalworker`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_medicalworker 
where tbl_clients_medicalworker.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_medicalworker set
	tbl_clients_medicalworker.medicalId = $upd_id
	where tbl_clients_medicalworker.clientId = $clientid;
else
	insert into tbl_clients_medicalworker(clientId, medicalId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadSocworker
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadSocworker`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_socialworker 
where tbl_clients_socialworker.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_socialworker set
	tbl_clients_socialworker.socworkerId = $upd_id
	where tbl_clients_socialworker.clientId = $clientid;
else
	insert into tbl_clients_socialworker(clientId, socworkerId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HeadStatus
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HeadStatus`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
DECLARE counts INT;

select count(id) into counts from tbl_clients_status 
where tbl_clients_status.clientId = $clientid;

if(counts > 0) then
	update tbl_clients_status set
	tbl_clients_status.statusId = $upd_id
	where tbl_clients_status.clientId = $clientid;
else
	insert into tbl_clients_status(clientId, statusId) 
	values ($clientid, $upd_id);
end if;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingArea
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingArea`(IN `$upd_id` DOUBLE, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.housingarea = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingCondition
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingCondition`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.conditionhousingid = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingGas
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingGas`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.conditiongasid = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingHeating
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingHeating`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.conditionheatingid = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingRepairCosmetic
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingRepairCosmetic`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.isrepaircosmetic = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingRepairMajor
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingRepairMajor`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.isrepairmajor = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingRoom
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingRoom`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.housingroom = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingSanitary
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingSanitary`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.conditionsanitaryid = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingType
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingType`(IN `$clientid` INT, IN `$upd_id` INT)
BEGIN

update tbl_clients_housing set
	tbl_clients_housing.typehousingid = $upd_id
where tbl_clients_housing.clientId = $clientid;

END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_HousingWater
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_HousingWater`(IN `$upd_id` INT, IN `$clientid` INT)
BEGIN
update tbl_clients_housing set
	tbl_clients_housing.conditionwaterid = $upd_id
where tbl_clients_housing.clientId = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_Info
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_Info`(IN `$lastname` VARCHAR(50), IN `$firstname` VARCHAR(50), IN `$middlename` VARCHAR(50), IN `$dateborn` DATE, IN `$snils` CHAR(14), IN `$clientid` INT, IN `$phone` VARCHAR(50))
BEGIN
update tbl_clients set
tbl_clients.lastname = $lastname,
tbl_clients.firstname = $firstname,
tbl_clients.middlename = $middlename,
tbl_clients.dateborn = $dateborn,
tbl_clients.snils = $snils,
tbl_clients.phone = $phone
where
tbl_clients.id = $clientid;
END//
DELIMITER ;


-- Дамп структуры для процедура issocial.p_updateClient_Suspend
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `p_updateClient_Suspend`(IN `$clientid` INT, IN `$actions` INT)
BEGIN
update tbl_clients set 
tbl_clients.isSuspend = $actions
where tbl_clients.id = $clientid;
END//
DELIMITER ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
